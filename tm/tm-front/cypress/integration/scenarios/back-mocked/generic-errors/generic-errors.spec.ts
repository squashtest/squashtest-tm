import {HomeWorkspacePage} from '../../../page-objects/pages/home-workspace/home-workspace.page';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {AlertDialogElement} from '../../../page-objects/elements/dialog/alert-dialog.element';

describe('Generic server errors display', function () {
  it('should show a generic error message', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel();
    homeWorkspacePage.assertExist();

    const errorMock = new HttpMockBuilder('information/version')
      .responseBody({trace: 'some stack-trace'})
      .status(500)
      .responseHeaders({ 'stack-trace': 'enable' })
      .build();

    const helpMenu = new NavBarElement().showSubMenu('help', 'help-menu');
    helpMenu.item('about').click();
    errorMock.wait();

    cy.get(selectByDataTestComponentId('generic-error-message')).should('be.visible');

    // We won't try clicking on the detail link as it would open in a popup...
  });

  it('should show an alert dialog when clicking detail with stack-trace disabled', () => {
    const homeWorkspacePage = HomeWorkspacePage.initTestAtPageWithModel();
    homeWorkspacePage.assertExist();

    const errorMock = new HttpMockBuilder('information/version')
      .status(500)
      .build();

    const helpMenu = new NavBarElement().showSubMenu('help', 'help-menu');
    helpMenu.item('about').click();
    errorMock.wait();

    cy.get(selectByDataTestComponentId('generic-error-message')).should('be.visible');
    cy.get(selectByDataTestComponentId('generic-error-message-details-link')).click();

    const detailsAlert = new AlertDialogElement('generic-error-message');
    detailsAlert.assertExist();
    detailsAlert.close();
  });
});
