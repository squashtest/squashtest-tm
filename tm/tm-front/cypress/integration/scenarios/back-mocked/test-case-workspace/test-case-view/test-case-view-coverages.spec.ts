import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {RequirementVersionCoverage} from '../../../../model/test-case/requirement-version-coverage-model';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {ChangeCoverageOperationReport} from '../../../../model/change-coverage-operation-report';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

const initialTestCaseRow: DataRow = {
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {
    'NAME': 'TestCase3', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'LOW'
  }
} as unknown as DataRow;

describe('Test Case View - Coverages', function () {
    it('should display test case coverages', () => {
      const page = navigateToTestCase();
      // folding tree and navbar to avoid nasty scroll bar in grids
      new NavBarElement().toggle();
      page.toggleTree();
      const coverageTables = page.coveragesTable;
      coverageTables.assertExist();
      coverageTables.assertRowExist(1);
      const row = coverageTables.getRow(1);
      row.cell('projectName').textRenderer().assertContainText('Project 1');
      row.cell('reference').textRenderer().assertContainText('');
      row.cell('name').linkRenderer().assertContainText('Requirement 1');
      row.cell('criticality').iconRenderer().assertContainIcon('anticon-sqtm-core-requirement:double_up');
      row.cell('status').iconRenderer().assertContainIcon('anticon-sqtm-core-requirement:status');
      // coverageTables.scrollPosition('right', 'mainViewport');
      const verifiedByCell = row.cell('verifiedBy');
      verifiedByCell.assertExist();
      verifiedByCell.textRenderer().assertContainText('Pas de test #1');

      const secondRow = coverageTables.getRow(2);
      // coverageTables.scrollPosition('left', 'mainViewport');
      secondRow.cell('projectName').textRenderer().assertContainText('Project 1');
      secondRow.cell('reference').textRenderer().assertContainText('');
      secondRow.cell('name').linkRenderer().assertContainText('Requirement 2');
      secondRow.cell('criticality').iconRenderer().assertContainIcon('anticon-sqtm-core-requirement:up');
      secondRow.cell('status').iconRenderer().assertContainIcon('anticon-sqtm-core-requirement:status');
      // coverageTables.scrollPosition('right', 'mainViewport');
      const secondRowVerifiedByCell = secondRow.cell('verifiedBy');
      secondRowVerifiedByCell.assertExist();
    });

    it('should add coverages', () => {
      const page = navigateToTestCase();
      new NavBarElement().toggle();
      page.toggleTree();
      const requirementResponse: GridResponse = {
        count: 2,
        dataRows: [
          {
            id: 'RequirementLibrary-1',
            children: ['Requirement-4', 'Requirement-5'],
            data: {'NAME': 'Project1'},
            projectId: 1,
            state: DataRowOpenState.open,
          } as unknown as DataRow,
          {
            id: 'Requirement-4',
            children: [],
            parentRowId: 'RequirementLibrary-1',
            projectId: 1,
            data: {
              'NAME': 'Requirement4',
              CRITICALITY: 'MAJOR',
              REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
              HAS_DESCRIPTION: true,
              REQ_CATEGORY_ICON: 'briefcase',
              REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
              REQ_CATEGORY_TYPE: 'SYS',
              COVERAGE_COUNT: 0,
              IS_SYNCHRONIZED: false
            }
          } as unknown as DataRow,
          {
            id: 'Requirement-5',
            children: [],
            parentRowId: 'RequirementLibrary-1',
            projectId: 1,
            data: {
              'NAME': 'Requirement5',
              CRITICALITY: 'MAJOR',
              REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
              HAS_DESCRIPTION: true,
              REQ_CATEGORY_ICON: 'briefcase',
              REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
              REQ_CATEGORY_TYPE: 'SYS',
              COVERAGE_COUNT: 0,
              IS_SYNCHRONIZED: false
            }
          } as unknown as DataRow
        ]
      } as unknown as GridResponse;
      const requirementDrawer = page.openRequirementDrawer(requirementResponse);
      requirementDrawer.beginDragAndDrop('Requirement-4');
      page.enterIntoTestCase();
      const coverageResponse = createCoverages([{
        criticality: 'CRITICAL',
        directlyVerified: true,
        name: 'Requirement4',
        projectName: 'Project 1',
        reference: '',
        requirementVersionId: 4,
        status: 'WORK_IN_PROGRESS',
        stepIndex: 0,
        unDirectlyVerified: false,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{id: 11, index: 0}],
        verifyingTestCaseId: 3
      }]);
      const coverageOperationReport: ChangeCoverageOperationReport = {
        coverages: coverageResponse,
        summary: {
          notLinkableRejections: false,
          noVerifiableVersionRejections: false,
          alreadyVerifiedRejections: false
        }
      };
      page.dropRequirementIntoTestCase(3, coverageOperationReport);
      page.closeRequirementDrawer();
      const coverageTables = page.coveragesTable;
      const newCoverage = coverageTables.getRow(4);
      newCoverage.cell('name').linkRenderer().assertContainText('Requirement4');
    });

    it('should navigate to search requirement for coverages', () => {
      const page = navigateToTestCase();
      new NavBarElement().toggle();
      page.toggleTree();
      const requirementForCoverageSearchPage = page.navigateToSearchRequirementForCoverage();
      requirementForCoverageSearchPage.assertExist();
      requirementForCoverageSearchPage.assertLinkSelectionButtonExist();
      requirementForCoverageSearchPage.assertLinkAllButtonExist();
    });

    it('should remove coverages', () => {
      const page = navigateToTestCase();
      new NavBarElement().toggle();
      page.toggleTree();
      const coverageTable = page.coveragesTable;
      let coveragesDialogElement = page.showDeleteConfirmCoveragesDialog(3, [1]);
      coveragesDialogElement.assertNotExist();
      coverageTable.selectRow(1, '#', 'leftViewport');
      coveragesDialogElement = page.showDeleteConfirmCoveragesDialog(3, [1]);
      coveragesDialogElement.assertExist();
      coveragesDialogElement.deleteForSuccess({
        coverages: [
          {
            criticality: 'MAJOR',
            directlyVerified: true,
            name: 'Requirement 2',
            projectName: 'Project 1',
            reference: '',
            requirementVersionId: 2,
            status: 'APPROVED',
            stepIndex: 2,
            unDirectlyVerified: true,
            verifiedBy: 'admin',
            verifyingCalledTestCaseIds: [12],
            verifyingTestCaseId: 3,
            coverageStepInfos: [{id: 11, index: 0}]
          }
        ]
      });
      coveragesDialogElement.assertNotExist();
      coverageTable.assertRowCount(1);
    });

    it('should remove coverage directly in table', () => {
      const page = navigateToTestCase();
      new NavBarElement().toggle();
      page.toggleTree();
      const coverageTable = page.coveragesTable;
      const coveragesDialogElement = page.showDeleteConfirmCoverageDialog(3, 2);
      coveragesDialogElement.assertExist();
      coveragesDialogElement.deleteForSuccess({
        coverages: [
          {
            criticality: 'CRITICAL',
            directlyVerified: true,
            name: 'Requirement 1',
            projectName: 'Project 1',
            reference: '',
            requirementVersionId: 1,
            status: 'WORK_IN_PROGRESS',
            stepIndex: 0,
            stepIndexes: [0],
            unDirectlyVerified: false,
            verifiedBy: 'admin',
            verifyingCalledTestCaseIds: [12],
            verifyingStepIds: [11],
            coverageStepInfos: [{id: 11, index: 0}],
            verifyingTestCaseId: 3
          }
        ]
      });
      coveragesDialogElement.assertNotExist();
      coverageTable.assertRowCount(1);
    });

    function navigateToTestCase(): TestCaseViewPage {
      const initialNodes: GridResponse = {
        count: 1,
        dataRows: [{
          id: 'TestCaseLibrary-1',
          children: [],
          data: {'NAME': 'Project1'}
        } as unknown as DataRow]
      };
      const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
      const libraryChildren = [
        {
          id: 'TestCaseLibrary-1',
          children: ['TestCase-3'],
          data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
          state: DataRowOpenState.open
        } as unknown as DataRow,
        initialTestCaseRow];

      testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryChildren);
      const model: TestCaseModel = mockTestCaseModel({
        id: 3,
        projectId: 1,
        name: 'TestCase3',
        customFieldValues: [],
        attachmentList: {
          id: 1,
          attachments: []
        },
        reference: '',
        description: '',
        uuid: '',
        type: 20,
        testSteps: [],
        status: 'WORK_IN_PROGRESS',
        prerequisite: '',
        parameters: [],
        nbIssues: 0,
        nature: 12,
        milestones: [],
        lastModifiedOn: new Date('2020-03-09 10:30'),
        lastModifiedBy: 'admin',
        kind: 'STANDARD',
        importanceAuto: false,
        importance: 'LOW',
        executions: [],
        datasets: [],
        datasetParamValues: [],
        createdOn: new Date('2020-03-09 10:30'),
        createdBy: 'admin',
        coverages: createCoverages(),
        automationRequest: null,
        automatable: 'M',
        calledTestCases: [],
        lastExecutionStatus: 'SUCCESS',
        script: ''
      });
      return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
    }
  }
)
;

function createCoverages(newCoverages?: RequirementVersionCoverage[]): RequirementVersionCoverage[] {
  const coverages = [...initialCoverages];
  if (newCoverages) {
    coverages.push(...newCoverages);
  }
  return coverages;
}

const initialCoverages: RequirementVersionCoverage[] = [
  {
    criticality: 'CRITICAL',
    directlyVerified: true,
    name: 'Requirement 1',
    projectName: 'Project 1',
    reference: '',
    requirementVersionId: 1,
    status: 'WORK_IN_PROGRESS',
    stepIndex: 0,
    unDirectlyVerified: false,
    verifiedBy: 'admin',
    verifyingCalledTestCaseIds: [12],
    verifyingTestCaseId: 3,
    coverageStepInfos: [{id: 11, index: 0}]
  },
  {
    criticality: 'MAJOR',
    directlyVerified: true,
    name: 'Requirement 2',
    projectName: 'Project 1',
    reference: '',
    requirementVersionId: 2,
    status: 'APPROVED',
    stepIndex: 2,
    unDirectlyVerified: true,
    verifiedBy: 'admin',
    verifyingCalledTestCaseIds: [12],
    verifyingTestCaseId: 3,
    coverageStepInfos: [{id: 11, index: 0}]
  }
];
