import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {mockGridResponse} from '../../../../data-mock/grid.data-mock';

describe('TestCase Workspace Tree Sort', function () {

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'TestCaseLibrary-1',
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'}
    } as unknown as DataRow]
  };

  it('should sort tree', () => {
    const childNodes = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'TestCaseFolder-1',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {'NAME': 'folder1'}
      } as unknown as DataRow,
      {
        id: 'TestCase-3',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {'NAME': 'a nice test', 'TC_KIND': 'STANDARD', 'TC_STATUS': 'APPROVED', 'IMPORTANCE': 'HIGH'}
      } as unknown as DataRow,
      {
        id: 'TestCaseFolder-2',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {'NAME': 'folder2'}
      } as unknown as DataRow
    ];
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    tree.assertNodeIsOpen(firstNode.id);
    tree.assertNodeOrderByName(['Project1', 'a nice test', 'folder1', 'folder2']);
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    tree.assertNodeOrderByName(['Project1', 'folder1', 'a nice test', 'folder2']);
    testCaseWorkspacePage.treeMenu.sortTreeAlphabetical();
    tree.assertNodeOrderByName(['Project1', 'a nice test', 'folder1', 'folder2']);
  });

  it('should persist sort', () => {
    const childNodes = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'TestCaseFolder-1',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {'NAME': 'folder1'}
      } as unknown as DataRow,
      {
        id: 'TestCase-3',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {'NAME': 'a nice test', 'TC_KIND': 'STANDARD', 'TC_STATUS': 'APPROVED', 'IMPORTANCE': 'HIGH'}
      } as unknown as DataRow,
      {
        id: 'TestCaseFolder-2',
        children: [],
        parentRowId: 'TestCaseLibrary-1',
        data: {'NAME': 'folder2'}
      } as unknown as DataRow
    ];
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    tree.assertNodeIsOpen(firstNode.id);
    tree.assertNodeOrderByName(['Project1', 'a nice test', 'folder1', 'folder2']);
    testCaseWorkspacePage.treeMenu.sortTreePositional();
    tree.assertNodeOrderByName(['Project1', 'folder1', 'a nice test', 'folder2']);
    NavBarElement.navigateToCampaignWorkspace(mockGridResponse('id', []));
    NavBarElement.navigateToTestCaseWorkspace(initialNodes);
    tree.openNode(firstNode.id, childNodes);
    tree.assertNodeOrderByName(['Project1', 'folder1', 'a nice test', 'folder2']);
  });
});

