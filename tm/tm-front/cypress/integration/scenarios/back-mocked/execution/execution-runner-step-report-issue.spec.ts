import {ExecutionRunnerProloguePage} from '../../../page-objects/pages/execution/execution-runner-prologue-page';
import {ExecutionModel} from '../../../model/execution/execution.model';
import {ReferentialDataMockBuilder} from '../../../utils/referential/referential-data-builder';
import {mockDataRow, mockGridResponse} from '../../../data-mock/grid.data-mock';
import {
  ReportIssueDialogElement,
  ReportIssueDialogMocks
} from '../../../page-objects/elements/issues/report-issue-dialog.element';
import {
  mockAdvancedIssueCreateModel,
  mockBTIssueCreateModel,
  mockExistingBTIssue
} from './execution-runner-step-report-issue.data';
import {mockExecutionModel} from '../../../data-mock/execution.data-mock';
import {ExecutionRunnerStepPage} from '../../../page-objects/pages/execution/execution-runner-step-page';
import {RemoteIssueSearchForm} from '../../../model/issue/remote-issue-search.model';

describe('Execution runner step - Report issue', function () {
  it('it should open with issues table visible', () => {
    const refData = new ReferentialDataMockBuilder().withBugTrackers({
      name: 'bt',
    }).withProjects({bugTrackerBinding: {id: 1, bugTrackerId: 1, projectId: 1}})
      .build();

    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel, refData);
    const firstStepPage = prologuePage.startExecutionWithKnownIssues(mockGridResponse('id', [mockDataRow({})]));
    firstStepPage.issuesPanelGrid.assertExist();
  });

  it('it should report a standard issue', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: false,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockBTIssueCreateModel(),
      issueType: 'standard',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: null,
    };

    const reportIssueDialog = navigateToReportIssueDialog(mocks);
    reportIssueDialog.assertExists();
    reportIssueDialog.fillTextField('summary', 'R');

    reportIssueDialog.confirmWithSuccess();
    reportIssueDialog.assertNotExists();
  });

  it('should show form validation errors for standard issue', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: false,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockBTIssueCreateModel(),
      issueType: 'standard',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: null,
    };

    const reportIssueDialog = navigateToReportIssueDialog(mocks);
    reportIssueDialog.assertExists();

    reportIssueDialog.confirmWithoutMock();
    reportIssueDialog.assertHasRequiredError('summary');
  });

  it('it should show server errors when submitting a standard issue', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: false,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockBTIssueCreateModel(),
      issueType: 'standard',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: null,
    };

    const reportIssueDialog = navigateToReportIssueDialog(mocks);
    reportIssueDialog.assertExists();
    reportIssueDialog.fillTextField('summary', 'R');

    reportIssueDialog.confirmWithErrorResponse({
      fieldValidationErrors: [
        {
          errorMessage: 'BOOM-KABOOM',
        }
      ],
    });
    reportIssueDialog.assertHasModalErrorMessage('BOOM-KABOOM');
  });

  it('it should attach to a standard issue', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: true,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockBTIssueCreateModel(),
      issueType: 'standard',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: getDefaultSearchForm(),
    };

    const reportIssueDialog = navigateToAttachIssueDialog(mocks);
    reportIssueDialog.assertExists();

    reportIssueDialog.searchForIssue({key: '1'}, mockExistingBTIssue());
    reportIssueDialog.confirmWithSuccess();
  });


  it('it should interact with widgets of an advanced issue', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: false,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockAdvancedIssueCreateModel(),
      issueType: 'advanced',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: null,
    };

    const reportIssueDialog = navigateToReportIssueDialog(mocks);
    reportIssueDialog.assertExists();

    // text_field, no auto complete
    reportIssueDialog.getAutocompleteTextField('summary').fill('R');

    // text_field, with auto complete
    const reporterAutocomplete = mockAssigneeAutoCompleteResponse();
    const reporterField = reportIssueDialog.getAutocompleteTextField('reporter');
    reporterField.fillWithAutocomplete('A', reporterAutocomplete);
    reporterField.assertHasAutocompleteChoices(reporterAutocomplete.composite.map(v => v.scalar));
    reporterField.clickAutocompleteChoice('Ali Baba');
    reporterField.checkContent('Ali Baba');

    // text_area
    reportIssueDialog.getTextAreaField('customfield_10091').fill(someMultiLineText);

    // dropdown_list
    reportIssueDialog.getDropdownList('customfield_10020').selectValue('Sample Sprint 5');

    // free_tag_list
    reportIssueDialog.getFreeTagList('labels').fillTagValue('free');

    // tag_list
    const tagList = reportIssueDialog.getTagList('fixVersions');
    tagList.focusTagInput();
    tagList.checkVisibleOptions(['v0.0', 'v0.1']);
    cy.clickVoid(); // dismiss overlay

    // date_picker
    reportIssueDialog.getDatePicker('customfield_10088').selectTodayDate();

    // date_time
    reportIssueDialog.getDatePicker('customfield_10179').selectNowDateTime();

    // multi_select
    const multiSelect = reportIssueDialog.getMultiSelect('customfield_10096');
    multiSelect.selectValue('Suzuki');
    cy.clickVoid(); // dismiss overlay
    multiSelect.checkSelectedOption('Suzuki');

    // cascading_select
    const cascadingSelect = reportIssueDialog.getCascadingSelect('customfield_10095');
    cascadingSelect.firstSelect.selectValue('Option 2');
    cascadingSelect.secondSelect.selectValue('B2');

    // checkbox_list
    const checkboxList = reportIssueDialog.getCheckboxList('customfield_10089');
    checkboxList.clickOption('Sedan');
    checkboxList.clickOption('Camion');
    checkboxList.checkSelectedOptions(['Sedan', 'Camion']);

    // file_upload
    reportIssueDialog.getRemoteAttachmentField('attachment').selector.should('be.visible');
    cy.get('sqtm-app-remote-attachment-field').should('be.visible');
    // this field relies on file drag and drop or native windows, which makes it difficult to test further

    // checkbox
    const checkbox = reportIssueDialog.getCheckbox('customfield_10090');
    checkbox.click();
    checkbox.checkState(true);
  });

  it('it should report an advanced issue', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: false,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockAdvancedIssueCreateModel(),
      issueType: 'advanced',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: null,
    };

    const reportIssueDialog = navigateToReportIssueDialog(mocks);
    reportIssueDialog.assertExists();

    reportIssueDialog.getAutocompleteTextField('summary').fill('R');

    reportIssueDialog.confirmWithSuccess();
    reportIssueDialog.assertNotExists();
  });

  it('it should attach to an advanced issue', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: true,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockAdvancedIssueCreateModel(),
      issueType: 'advanced',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: getDefaultSearchForm(),
    };

    const reportIssueDialog = navigateToAttachIssueDialog(mocks);
    reportIssueDialog.assertExists();

    reportIssueDialog.searchForIssue({key: '1'}, {...mockAdvancedIssueCreateModel(), id: '1'});
    reportIssueDialog.confirmWithSuccess();
  });

  it('it should perform multi-term search', () => {
    const mocks: ReportIssueDialogMocks = {
      attachMode: true,
      remoteProjectNames: ['a', 'b', 'c'],
      remoteIssue: mockAdvancedIssueCreateModel(),
      issueType: 'advanced',
      bindableEntity: 'EXECUTION_STEP_TYPE',
      searchForm: getMultiTermSearchForm(),
    };

    const reportIssueDialog = navigateToAttachIssueDialog(mocks);
    reportIssueDialog.assertExists();

    reportIssueDialog.searchForIssue({
      projectPath: 'path/to/project',
      issueId: '123',
    }, {...mockAdvancedIssueCreateModel(), id: '1'});
    reportIssueDialog.confirmWithSuccess();
  });
});

function navigateToExecutionRunnerStep(): ExecutionRunnerStepPage {
  const refData = new ReferentialDataMockBuilder()
    .withBugTrackers({name: 'bt'})
    .withProjects({bugTrackerBinding: {id: 1, bugTrackerId: 1, projectId: 1}})
    .build();

  const executionModel: ExecutionModel = getExecutionModel();
  const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel, refData);
  const firstStepPage = prologuePage.startExecutionWithKnownIssues(mockGridResponse('id', [mockDataRow({})]));
  firstStepPage.issuesPanelGrid.assertExist();
  return firstStepPage;
}

function navigateToReportIssueDialog(mocks: ReportIssueDialogMocks): ReportIssueDialogElement {
  const stepPage = navigateToExecutionRunnerStep();
  return stepPage.openReportIssueDialog(mocks);
}

function navigateToAttachIssueDialog(mocks: ReportIssueDialogMocks): ReportIssueDialogElement {
  const stepPage = navigateToExecutionRunnerStep();
  return stepPage.openAttachIssueDialog(mocks);
}

function getExecutionModel(): ExecutionModel {
  return mockExecutionModel({});
}

function mockAssigneeAutoCompleteResponse(): any {
  return {
    'id': null,
    'typename': null,
    'scalar': null,
    'composite': [
      {
        'id': '553b0a34c',
        'typename': 'user',
        'scalar': 'Ali Baba',
        'composite': [],
        'custom': null,
        'name': 'Ali Baba'
      },
      {
        'id': '58d40bd077',
        'typename': 'user',
        'scalar': 'Auto Complete',
        'composite': [],
        'custom': null,
        'name': 'Auto Complete'
      },
      {
        'id': '5e449e60a83d',
        'typename': 'user',
        'scalar': 'Ulrich Aaron',
        'composite': [],
        'custom': null,
        'name': 'Ulrich Aaron'
      },
    ],
    'custom': null,
    'name': 'Ali Baba, , Automcomplete, , Ulrich Aaron, '
  };
}

const someMultiLineText = `A
B
C`;

function getDefaultSearchForm(): RemoteIssueSearchForm {
  return {
    fields: [
      {
        id: 'key',
        label: 'key',
        possibleValues: [],
        rendering: {
          inputType: {
            name: 'text_field',
            configuration: {},
            dataType: 'text',
            fieldSchemeSelector: false,
            original: null
          },
          required: true,
          operations: [],
        }
      },
    ],
  };
}

function getMultiTermSearchForm(): RemoteIssueSearchForm {
  return {
    fields: [
      {
        id: 'projectPath',
        label: 'Chemin du projet',
        possibleValues: [],
        rendering: {
          inputType: {
            name: 'text_field',
            configuration: {},
            dataType: 'text',
            fieldSchemeSelector: false,
            original: null
          },
          required: true,
          operations: [],
        }
      },
      {
        id: 'issueId',
        label: 'Issue ID',
        possibleValues: [],
        rendering: {
          inputType: {
            name: 'text_field',
            configuration: {},
            dataType: 'text',
            fieldSchemeSelector: false,
            original: null
          },
          required: true,
          operations: [],
        }
      },
    ],
  };
}
