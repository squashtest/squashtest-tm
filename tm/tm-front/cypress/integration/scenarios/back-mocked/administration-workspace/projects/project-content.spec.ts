import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {AdminWorkspaceProjectsPage} from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {NavBarAdminElement} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {BugTrackerBinding} from '../../../../model/bugtracker/bug-tracker.binding';
import {ProjectView} from '../../../../model/project/project.model';
import {makeProjectViewData, mockBugTrackerReferentialDto} from '../../../../data-mock/administration-views.data-mock';
import {AuthenticationPolicy, AuthenticationProtocol} from '../../../../model/bugtracker/bug-tracker.model';
import {UnboundPartiesResponse} from '../../../../page-objects/pages/administration-workspace/project-view/dialogs/create-project-permissions.dialog';
import {AclGroup} from '../../../../model/permissions/permissions.model';
import {ProjectIsBoundToATemplateAlert} from '../../../../page-objects/pages/administration-workspace/dialogs/project-is-bound-to-a-template-alert.element';
import {ProjectAutomationPanelElement} from '../../../../page-objects/pages/administration-workspace/project-view/panels/project-automation-panel.element';
import {TestAutomationProject} from '../../../../model/test-automation/test-automation-project.model';
import {selectByDataTestDialogButtonId} from '../../../../utils/basic-selectors';
import {mockFieldValidationError} from '../../../../data-mock/http-errors.data-mock';
import {ScmServer} from '../../../../model/scm-server/scm-server.model';
import {TestAutomationServer} from '../../../../model/test-automation/test-automation-server.model';
import {AdminReferentialDataMockBuilder} from '../../../../utils/referential/admin-referential-data-builder';

describe('Administration Workspace - Projects - Bugtracker', function () {

  it('it should bind a bugtracker to the project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto(null, null)
    );

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.projectNamesInBugtracker.assertNotExist();

    projectViewPage.changeBugtracker('BT JIRA', true);

    projectViewPage.projectNamesInBugtracker.assertExist();
  });

  it('it should show special label when the selected bugtracker uses path to projects', () => {
    const refData = new AdminReferentialDataMockBuilder()
      .withBugTrackers([mockBugTrackerReferentialDto({
        id: -1, useProjectPaths: true})])
      .build();
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, refData);

    const projectViewPage = page.selectProjectByName(
      initialNodes.dataRows[0].data.name,
      getProjectDto({id: -1, projectId: 1, bugTrackerId: -1}, []),
    );

    projectViewPage.assertHasBugTrackerProjectPathLabel();
  });

  it('it should add a project name to tag field', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto({id: -1, projectId: 4, bugTrackerId: -1}, ['Project1'])
    );

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.projectNamesInBugtracker.addTag('@_@', {projectNames: ['Project1', '@_@']});
  });

  it('it should delete a project name from tag field', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto({id: -1, projectId: 4, bugTrackerId: -1}, ['Project1', 'Project2']),
    );

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.projectNamesInBugtracker.deleteTagWithValue('Project2', {projectNames: ['Project1']});
  });

  it('it should forbid to delete a unique tag field', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto({id: -1, projectId: 4, bugTrackerId: -1}, ['Project1'])
    );

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.projectNamesInBugtracker.checkIfTagIsClosable('Project1', false);
  });

  it('it should remove a bugtracker binding', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto({id: -1, projectId: 4, bugTrackerId: -1}, ['Project1'])
    );

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.selectNoBugtracker();
    projectViewPage.projectNamesInBugtracker.assertNotExist();
  });

  function getProjectDto(bugTrackerBinding: BugTrackerBinding, bugtrackerProjectNames: string[]): ProjectView {
    return makeProjectViewData({
      availableBugtrackers: [{
        id: -1,
        kind: 'jira.cloud',
        name: 'BT JIRA',
        url: 'https://',
        iframeFriendly: false,
        authPolicy: AuthenticationPolicy.APP_LEVEL,
        authProtocol: AuthenticationProtocol.BASIC_AUTH
      }],
      bugTrackerBinding: bugTrackerBinding,
      bugtrackerProjectNames: bugtrackerProjectNames,
    });
  }
});

describe('Administration Workspace - Projects - Permissions', function () {
  beforeEach(() => {
    // cy.server();
  });

  it('should show permissions count', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto(true));
    projectViewPage.assertPermissionCount(2);
  });

  it('should add project permissions to users', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto(false));

    projectViewPage.assertExist();
    projectViewPage.permissionsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const unboundParties: UnboundPartiesResponse = {
      teams: [],
      users: [
        {
          groupId: 'user',
          id: '-1',
          label: 'John Dane (jdane)',
        },
        {
          groupId: 'user',
          id: '-2',
          label: 'Jane Doe (jdoe)',
        }
      ]
    };

    const createPermissionDialog = projectViewPage.permissionsPanel.clickOnAddUserPermissionButton(unboundParties);

    createPermissionDialog.selectParties('Jane Doe (jdoe)', 'John Dane (jdane)');
    createPermissionDialog.selectProfile('Testeur référent');

    createPermissionDialog.confirm([]);
  });

  it('should add project permissions to teams', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto(false));

    projectViewPage.assertExist();
    projectViewPage.permissionsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const unboundParties: UnboundPartiesResponse = {
      teams: [
        {
          groupId: 'team',
          id: '-1',
          label: 'team1',
        },
        {
          groupId: 'team',
          id: '-2',
          label: 'team2',
        }
      ],
      users: []
    };

    const createPermissionDialog = projectViewPage.permissionsPanel.clickOnAddTeamPermissionButton(unboundParties);

    createPermissionDialog.selectParties('team1', 'team2');
    createPermissionDialog.selectProfile('Testeur référent');

    createPermissionDialog.confirm([]);
  });

  it('should remove project permission', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto(true));

    projectViewPage.assertExist();
    projectViewPage.permissionsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.permissionsPanel.deleteOne('Jane Doe (jdoe)', []);
  });

  it('should remove multiple project permissions', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto(true));

    projectViewPage.assertExist();
    projectViewPage.permissionsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.permissionsPanel.deleteMultiple(['Jane Doe (jdoe)', 'Team A']);
  });

  it('should prevent adding permissions with empty fields', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto(false));

    projectViewPage.assertExist();
    projectViewPage.permissionsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const unboundParties = {teams: [], users: []};
    const createPermissionDialog = projectViewPage.permissionsPanel.clickOnAddUserPermissionButton(unboundParties);

    createPermissionDialog.clickOnConfirmButton();
    cy.get('.sqtm-core-error-message').should('have.length', 2);
  });

  function getProjectDto(withPermissions: boolean): ProjectView {
    return makeProjectViewData({
      partyProjectPermissions: withPermissions ? [
        {
          partyId: -1,
          partyName: 'Jane Doe (jdoe)',
          permissionGroup: {
            id: 1,
            simpleName: 'PROJECT_MANAGER',
            qualifiedName: AclGroup.PROJECT_MANAGER
          },
          team: false,
          projectId: 4,
        },
        {
          partyId: -2,
          partyName: 'Team A',
          permissionGroup: {
            id: 2,
            simpleName: 'TEST_RUNNER',
            qualifiedName: AclGroup.TEST_RUNNER
          },
          team: true,
          projectId: 4,
        }
      ] : [],
    });
  }
});

describe('Administration Workspace - Projects - Execution', function () {
  beforeEach(() => {
    // cy.server();
  });

  it('should forbid to switch status availability if project is bound to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto(true, false, false));

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.clickOnSwitchAllowTcModifDuringExec();

    const alert = new ProjectIsBoundToATemplateAlert();
    alert.assertMessage();
    alert.close();

  });

  it('should alert that UNTESTABLE status is used within project when trying to switch it off', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto(false, false, true));

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const dialog = projectViewPage.openDialogForStatusesInUseWithinProject('UNTESTABLE');
    dialog.editStatusInUse('UNTESTABLE', 'Succès');

  });

  it('should alert that SETTLED status is used within project when trying to switch it off', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto(false, true, false));

    projectViewPage.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const dialog = projectViewPage.openDialogForStatusesInUseWithinProject('SETTLED');
    dialog.editStatusInUse('SETTLED', 'Succès');
  });

  function getProjectDto(
    isLinkedToTemplate: boolean,
    settledStatusIsUsedWithinProject: boolean,
    untestableStatusIsUsedWithinProject: boolean): ProjectView {
    return makeProjectViewData({
      linkedTemplateId: (isLinkedToTemplate ? 2 : null),
      linkedTemplate: (isLinkedToTemplate ? 'Template1' : null),
      allowTcModifDuringExec: false,
      allowedStatuses: {SETTLED: true, UNTESTABLE: true},
      statusesInUse: {SETTLED: settledStatusIsUsedWithinProject, UNTESTABLE: untestableStatusIsUsedWithinProject},
    });
  }
});

describe('Administration Workspace - Projects - Automation', function () {
  beforeEach(() => {
    // cy.server();
  });

  it('should set a SCM repository', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());

    projectViewPage.assertExist();
    const automationPanel = projectViewPage.clickAnchorLink('automation') as ProjectAutomationPanelElement;

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    automationPanel.checkScmServerBlockVisibility(false);
    automationPanel.selectAutomationWorkflowType('Squash');

    automationPanel.checkScmServerBlockVisibility(true);
    automationPanel.useTreeStructureField.checkValue(false);
    automationPanel.useTreeStructureField.toggle();
    automationPanel.useTreeStructureField.checkValue(true);

    automationPanel.checkScmRepositoryFieldVisibility(false);
    automationPanel.selectScmServer('server1');
    automationPanel.checkScmRepositoryFieldVisibility(true);

    automationPanel.scmRepositorySelectField.setAndConfirmValue('repo1');

    automationPanel.selectAutomationWorkflowType('Aucun');
    automationPanel.checkScmServerBlockVisibility(false);
    automationPanel.checkScmRepositoryFieldVisibility(false);
  });

  it('should set an execution server', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.automationPanel.checkTaJobsBlockVisibility(false);
    projectViewPage.automationPanel.executionServerField.setAndConfirmValue('taServer2');
    projectViewPage.automationPanel.checkTaJobsBlockVisibility(true);
  });

  it('should add a job', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name,
      getProjectDto(-1), {});

    const addJobDialog = projectViewPage.automationPanel.openAddJobDialog([
      {remoteName: 'Job1', label: 'Job1'} as TestAutomationProject,
      {remoteName: 'Job2', label: 'Job2'} as TestAutomationProject,
    ]);

    addJobDialog.checkRowIsEditable('Job2', false);
    addJobDialog.toggleJobSelection('Job2');
    addJobDialog.setJobLabelInTM('Job2', 'blabla');
    addJobDialog.toggleJobCanRunBdd('Job2');
    addJobDialog.confirm();
  });

  it('should modify a job inside the grid', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const dto = getProjectDto(-1);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, dto);

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.automationPanel.changeJobLabel('rem1', 'new label', dto.boundTestAutomationProjects);
    projectViewPage.automationPanel.toggleJobCanRunBdd('rem1', dto.boundTestAutomationProjects);
    projectViewPage.automationPanel.toggleJobCanRunBdd('rem2', dto.boundTestAutomationProjects);

    // Only one job per TM project should be able to run BDD. We don't test this in ITs because
    // this is happening server-side and the front won't do anything fancy (only refreshing the
    // grid with fresh data from the server)
  });

  it('should show error messages', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto(-1));
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();
    const automationPanel = projectViewPage.clickAnchorLink('automation') as ProjectAutomationPanelElement;

    // should show an error message if the execution server is unreachable when showing job binding dialog
    const alert = automationPanel.openAddJobDialogWithError();
    cy.get(alert.baseSelector).find(selectByDataTestDialogButtonId('cancel')).click();

    // should show error message in job binding dialog when label is blank
    const addJobDialog = automationPanel.openAddJobDialog([
      {remoteName: 'Job1', label: 'Job1'} as TestAutomationProject,
      {remoteName: 'Job2', label: 'Job2'} as TestAutomationProject,
    ]);

    addJobDialog.toggleJobSelection('Job1');
    addJobDialog.clearJobLabelInTM('Job1');
    addJobDialog.confirmWithClientSideErrors();
    addJobDialog.checkRequiredErrorMessage(true);

    // should show error message in job binding dialog when label are duplicates
    addJobDialog.toggleJobSelection('Job2');
    addJobDialog.setJobLabelInTM('Job1', 'Job1');
    addJobDialog.setJobLabelInTM('Job2', 'Job1');
    addJobDialog.confirmWithClientSideErrors();
    addJobDialog.checkDuplicateLabelError(true);

    // should show server-side error messages
    addJobDialog.toggleJobSelection('Job2');
    const error = mockFieldValidationError('label', 'sqtm-core.exception.duplicate.tmlabel');
    addJobDialog.confirmWithServerSideErrors(error);
    addJobDialog.checkDuplicateLabelError(true);
  });

  it('should delete a job', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectDto = getProjectDto(-1);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, projectDto);

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const automationPanel = projectViewPage.clickAnchorLink('automation') as ProjectAutomationPanelElement;
    automationPanel.taServerJobsGrid.assertRowCount(2);
    automationPanel.deleteSingleJob('job1', true, [projectDto.boundTestAutomationProjects[0]]);
    automationPanel.deleteSingleJob('job2', false, []);
  });

  it('should update automated suites lifetime', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());

    projectViewPage.assertExist();
    const automationPanel = projectViewPage.clickAnchorLink('automation') as ProjectAutomationPanelElement;

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    automationPanel.automatedSuitesLifetimeField.setAndConfirmValue(300);
  });

  it('should update implementation technology', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());

    projectViewPage.assertExist();
    const automationPanel = projectViewPage.clickAnchorLink('automation') as ProjectAutomationPanelElement;

    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    automationPanel.bddTechnologyField.setAndConfirmValue('Robot framework');
    automationPanel.bddScriptLanguageField.assertIsNotEditable();

    automationPanel.bddTechnologyField.setAndConfirmValue('Cucumber');
    automationPanel.bddScriptLanguageField.assertIsEditable();
    automationPanel.bddScriptLanguageField.setAndConfirmValue('Espagnol');
  });

  function getProjectDto(taServerId?: number): ProjectView {
    return makeProjectViewData({
      scmRepositoryId: taServerId ? 1 : null,
      automationWorkflowType: 'NONE',
      taServerId,
      availableScmServers: [
        {
          serverId: -1,
          name: 'server1',
          repositories: [
            {
              scmRepositoryId: 1,
              serverId: 1,
              name: 'repo1',
            }
          ]
        } as ScmServer,
        {
          serverId: -2,
          name: 'server2',
          repositories: [
            {
              scmRepositoryId: 2,
              serverId: 2,
              name: 'repo1',
            }
          ]
        } as ScmServer
      ],
      availableTestAutomationServers: [
        {id: -1, name: 'taServer1', baseUrl: 'https://url1'} as TestAutomationServer,
        {id: -2, name: 'taServer2', baseUrl: 'https://url2'} as TestAutomationServer,
      ],
      boundMilestonesInformation: [],
      boundTestAutomationProjects: [
        {
          taProjectId: -1,
          label: 'job1',
          remoteName: 'rem1',
          canRunBdd: false,
          executionEnvironments: '',
          serverId: -1,
          tmProjectId: 4,
        },
        {
          taProjectId: -2,
          label: 'job2',
          remoteName: 'rem2',
          canRunBdd: false,
          executionEnvironments: '',
          serverId: -1,
          tmProjectId: 4,
        },
      ],
      bddScriptLanguage: 'ENGLISH',
      bddImplementationTechnology: 'CUCUMBER'
    });
  }
});

const todayDate = new Date();

const initialNodes: GridResponse = {
  count: 1,
  dataRows: [
    {
      id: '1',
      children: [],
      data: {
        projectId: 1,
        name: 'Project1',
        label: 'label',
        isTemplate: false,
        createdOn: todayDate,
        createdBy: 'JP01',
        lastModifiedOn: todayDate,
        lastModifiedBy: 'JP01',
        hasPermissions: true,
        bugtrackerName: 'BT JIRA',
        executionServer: 'TA server',
      }
    } as unknown as DataRow,
  ],
};
