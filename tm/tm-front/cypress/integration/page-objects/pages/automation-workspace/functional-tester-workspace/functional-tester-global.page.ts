import {FunctionalTesterWorkspacePage} from './functional-tester-workspace.page';
import {NavBarElement} from '../../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../../elements/grid/grid.element';
import {PageFactory} from '../../page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {ToolbarElement} from '../../../elements/workspace-common/toolbar.element';
import {
  AutomationFunctionalTesterWorkspaceDataModel,
  defaultFunctionalTesterWorkspaceData
} from './utils/functional-tester-utils';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {selectByDataTestButtonId, selectByDataTestMenuItemId} from '../../../../utils/basic-selectors';

export class FunctionalTesterGlobalPage extends FunctionalTesterWorkspacePage {

  public readonly navBar = new NavBarElement();
  gridToolBarElement: ToolbarElement;

  protected constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-global-functional-tester-view');
    this.gridToolBarElement = new ToolbarElement('global-view-toolbar');
  }

  public static initTestAtPage: PageFactory<FunctionalTesterGlobalPage> =
    (initialNodes: GridResponse = {dataRows: []}, initialWorkspaceModel: AutomationFunctionalTesterWorkspaceDataModel
       = defaultFunctionalTesterWorkspaceData, referentialData?: ReferentialData) => {

      const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
      const mockBuilder = new HttpMockBuilder('automation-tester-workspace/data').responseBody(initialWorkspaceModel).build();

      const gridElement = GridElement.createGridElement('functional-tester-global-view',
        'automation-tester-workspace/global-view', initialNodes);
      const page = new FunctionalTesterGlobalPage(gridElement);

      // visit page
      cy.visit(`automation-workspace/functional-tester-workspace/global`);

      // wait for ref data request to fire
      referentialDataProvider.wait();
      mockBuilder.wait();

      // wait for initial grid data and additional requests to fire
      page.waitInitialDataFetch();
      // Check page initialisation
      page.assertExist();

      return page;
    }

  protected getPageUrl(): string {
    return 'global';
  }

  transmitButton() {
    return cy.get(selectByDataTestButtonId('transmit'));
  }

  suspendedMenuItem() {
    return cy.get(selectByDataTestMenuItemId('suspended'));
  }

  readyToTransmitMenuItem() {
    return cy.get(selectByDataTestMenuItemId('ready-for-transmission'));
  }

  workInProgressMenuItem() {
    return cy.get(selectByDataTestMenuItemId('work-in-progress'));
  }
}
