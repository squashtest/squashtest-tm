import {User} from '../../../../../model/user/user.model';

export class AutomationWorkspaceDataModel {
  nbTotal: number;
  nbAutomReqToTreat: number;
  nbAssignedAutomReq: number;
  usersWhoModifiedTestCasesAssignView: User[];
  usersWhoModifiedTestCasesTreatmentView: User[];
  usersWhoModifiedTestCasesGlobalView: User[];
  usersAssignedTo: User[];
  loaded?: boolean;
}

export const defaultAutomationWorkspaceData: AutomationWorkspaceDataModel = {
  nbTotal: 5,
  nbAssignedAutomReq: 2,
  nbAutomReqToTreat: 3,
  usersWhoModifiedTestCasesAssignView: [{
    id: 1,
    login: 'admin'
  } as User],
  usersWhoModifiedTestCasesGlobalView: [{
    id: 1,
    login: 'admin'
  } as User],
  usersWhoModifiedTestCasesTreatmentView: [{
    id: 1,
    login: 'admin'
  } as User],
  usersAssignedTo: [{
    id: 1,
    login: 'admin'
  } as User],
};
