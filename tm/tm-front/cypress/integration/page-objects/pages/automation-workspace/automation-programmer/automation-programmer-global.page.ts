import {NavBarElement} from '../../../elements/nav-bar/nav-bar.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridElement} from '../../../elements/grid/grid.element';
import {AutomationProgrammerWorkspacePage} from './automation-programmer-workspace.page';
import {PageFactory} from '../../page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {AutomationWorkspaceDataModel, defaultAutomationWorkspaceData} from './utils/automation-programmer-utils';
import {ReferentialData} from '../../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {ToolbarElement} from '../../../elements/workspace-common/toolbar.element';


export class AutomationProgrammerGlobalPage extends AutomationProgrammerWorkspacePage {

  public readonly navBar = new NavBarElement();
  gridToolBarElement: ToolbarElement;

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-automation-workspace-global');
    this.gridToolBarElement = new ToolbarElement('global-toolbar');
  }


  public static initTestAtPage: PageFactory<AutomationProgrammerGlobalPage> =
    (initialNodes: GridResponse = {dataRows: []}, initialWorkspaceModel: AutomationWorkspaceDataModel = defaultAutomationWorkspaceData,
     referentialData?: ReferentialData) => {

      const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
      const mockBuilder = new HttpMockBuilder('automation-workspace/data').responseBody(initialWorkspaceModel).build();
      const gridElement = GridElement.createGridElement('automation-programmer-global',
        'automation-workspace/global-autom-req', initialNodes);
      const page = new AutomationProgrammerGlobalPage(gridElement);

      // visit page
      cy.visit(`automation-workspace/automation-programmer-workspace/global`);

      // wait for ref data request to fire
      referentialDataProvider.wait();

      mockBuilder.wait();

      // wait for initial grid data and additional requests to fire
      page.waitInitialDataFetch();
      // Check page initialisation
      page.assertExist();

      return page;
    }

  protected getPageUrl(): string {
    return 'treatment';
  }

}
