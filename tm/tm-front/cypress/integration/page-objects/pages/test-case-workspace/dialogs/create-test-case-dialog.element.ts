import {CreateEntityDialog} from '../../create-entity-dialog.element';
import {ProjectData} from '../../../../model/project/project-data.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class CreateTestCaseDialog extends CreateEntityDialog {

  scriptLanguageField: SelectFieldElement;

  constructor(project?: ProjectData, domain?: BindableEntity) {
    super({
      treePath: 'test-case-tree',
      viewPath: 'test-case-view',
      newEntityPath: 'new-test-case'
    }, project, domain);

    this.scriptLanguageField = new SelectFieldElement(CommonSelectors.fieldName('scriptLanguage'));
  }

  checkCreateTestCaseDialogButtons() {
    cy.get('button[data-test-dialog-button-id="add-another"]').should('exist');
    cy.get('button[data-test-dialog-button-id="add"]').should('exist');
    cy.get('button[data-test-dialog-button-id="cancel"]').should('exist');
  }

  fillFieldsWithoutCuf(name: string, reference: string, description: string) {
    this.fillName(name);
    this.fillReference(reference);
    this.fillDescription(description);
  }

  changeScriptLanguage(kind: string) {
    this.scriptLanguageField.selectValue(kind);
  }

}
