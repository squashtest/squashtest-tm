import {TestCaseViewPage} from './test-case-view.page';
import {Page} from '../../page';
import {GridElement} from '../../../elements/grid/grid.element';
import {CreateDatasetDialogElement} from '../dialogs/create-dataset-dialog.element';
import {CreateParameterDialogElement} from '../dialogs/create-parameter-dialog.element';
import {RemoveDatasetDialog} from '../dialogs/remove-dataset-dialog.element';
import {MenuElement} from '../../../../utils/menu.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {RemoveParameterDialog} from '../dialogs/remove-parameter-dialog.element';
import {ParameterInformationDialog} from '../dialogs/parameter-information-dialog.element';
import {GridCellSelectorBuilder, GridHeaderRowElement} from '../../../../utils/grid-selectors.builder';

export class TestCaseViewParametersPage extends Page {
  constructor(public parentPage: TestCaseViewPage) {
    super('sqtm-app-datasets-table');
  }

  get testCaseId(): number | string {
    return this.parentPage.testCaseId;
  }

  get parametersTable(): GridElement {
    return new GridElement('test-case-view-datasets');
  }

  openAddDataSetDialog(): CreateDatasetDialogElement {
    const addDataSet = cy.get('[data-test-button-id="add-dataset"]');
    addDataSet.should('exist');
    addDataSet.click();
    return new CreateDatasetDialogElement();
  }

  openAddParameterDialog(): CreateParameterDialogElement {
    const addParam = cy.get('[data-test-button-id="add-param"]');
    addParam.should('exist');
    addParam.click();
    return new CreateParameterDialogElement();
  }

  openDeleteDataSetDialog(rowId: number): RemoveDatasetDialog {
    cy.get(`[data-test-row-id=${rowId}] [data-test-cell-id="delete"] i`).click();
    return new RemoveDatasetDialog();
  }

  openDeleteParamDialog(headerId: string, isUsed?: ParameterUsed) {
    cy.get(`[data-test-cell-id="${headerId}"] i[data-test-icon-id="param-action"]`).trigger('mouseenter');
    const menuElement = new MenuElement('param-menu');
    menuElement.assertExist();
    const removeParamItem = menuElement.item('remove-param');
    removeParamItem.assertExist();
    const httpMock = new HttpMockBuilder('parameters/*/used').responseBody(isUsed).build();
    removeParamItem.click();
    httpMock.wait();
  }

  openInformationParamDialog(headerId: string): ParameterInformationDialog {
    cy.get(`[data-test-cell-id="${headerId}"] i[data-test-icon-id="param-action"]`).trigger('mouseenter');
    const menuElement = new MenuElement('param-menu');
    menuElement.assertExist();
    const removeParamItem = menuElement.item('information-param');
    removeParamItem.assertExist();
    removeParamItem.click();
    return new ParameterInformationDialog();
  }

  renameParam(paramName: string, newValue: string) {
    const gridHeaderRowElement = this.parametersTable.getHeaderRow() as GridHeaderRowElement;
    gridHeaderRowElement.findCellId('name', paramName).then(paramId => {
      const gridCellSelectorBuilder = gridHeaderRowElement.cell(paramId) as GridCellSelectorBuilder;
      gridCellSelectorBuilder.findCellTextSpan().should('contain', paramName);
      gridCellSelectorBuilder.textRenderer().editText(newValue, 'parameters/*/rename');
      gridCellSelectorBuilder.findCellTextSpan().should('contain', newValue);
    });
  }

  checkExistingParam(paramName: string) {
    const gridHeaderRowElement = this.parametersTable.getHeaderRow() as GridHeaderRowElement;
    gridHeaderRowElement.findCellId('name', paramName).then(paramId => {
      const gridCellSelectorBuilder = gridHeaderRowElement.cell(paramId) as GridCellSelectorBuilder;
      gridCellSelectorBuilder.findCellTextSpan().contains(new RegExp('\\s' + paramName + '\\s'));
    });
  }

  checkExistingDataSet(datasetName: string) {
    const gridElement = this.parametersTable as GridElement;
    gridElement.findRowId('name', datasetName, 'leftViewport').then(dataSetId => {
      gridElement.selectRow(dataSetId, 'name', 'leftViewport' );
    });
  }

  renameDataSet(datasetName: string, newName: string) {
    const gridElement = this.parametersTable as GridElement;
    gridElement.findRowId('name', datasetName, 'leftViewport').then(dataSetId => {
      const gridCellSelectorBuilder = gridElement.getCell(dataSetId, 'name', 'leftViewport') as GridCellSelectorBuilder;
      gridCellSelectorBuilder.findCellTextSpan().should('contain', datasetName);
      gridCellSelectorBuilder.textRenderer().editText(newName, 'datasets/rename');
      gridCellSelectorBuilder.findCellTextSpan().should('contain', newName);
    });
  }

  changeParamValue(datasetName: string, paramName: string, newValue: string) {
    const gridElement = this.parametersTable as GridElement;
    gridElement.findRowId('name', datasetName, 'leftViewport').then(dataSetId => {
      const gridHeaderRowElement = this.parametersTable.getHeaderRow() as GridHeaderRowElement;
      gridHeaderRowElement.findCellId('name', paramName).then(paramId => {
        const gridCellSelectorBuilder = gridElement.getCell(dataSetId, paramId) as GridCellSelectorBuilder;
        gridCellSelectorBuilder.textRenderer().editText(newValue, 'dataset-param-values');
        gridCellSelectorBuilder.findCellTextSpan().should('contain', newValue);
      });
    });
  }

  get paramRemoveDialog() {
    return new RemoveParameterDialog();
  }
}

export interface ParameterUsed {
  PARAMETER_USED: boolean;
}
