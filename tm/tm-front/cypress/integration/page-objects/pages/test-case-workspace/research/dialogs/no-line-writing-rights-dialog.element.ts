import {AlertDialogElement} from '../../../../elements/dialog/alert-dialog.element';

export class NoLineWritingRightsDialogElement extends AlertDialogElement {

  constructor() {
    super('alert');
  }

  assertMessage() {
    this.assertHasMessage('Aucun des éléments séléctionnés ne peut être édité car : soit le statut de l\'un de' +
      ' leurs jalons associés ne le permet pas, soit vous ne disposez pas de droits suffisants.');
  }
}
