import {EntityViewPage} from '../../page';
import {FolderInformationPanelElement} from '../../../elements/panels/folder-information-panel.element';
import {TestCaseStatisticPanelElement} from '../panels/test-case-statistic-panel.element';
import {apiBaseUrl} from '../../../../utils/mocks/request-mock';

export class TestCaseFolderViewPage extends EntityViewPage {

  constructor(private folderId: number|'*') {
    super('sqtm-app-test-case-folder-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-case-folder-view/${this.folderId}?**`;
    cy.wait(`@${url}`);
  }

  checkNumberOfTestCases(number: number) {
    cy.get('div[data-test-component-id="footer"]> a')
      .should('have.text', number);
  }

  clickNumberOfTestCases() {
    cy.get('div[data-test-component-id="footer"]> a')
      .click();
  }

  clickAnchorLink<T>(linkId: TestCaseFolderAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'dashboard':
        element = this.showDashboard(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showInformationPanel<T>(linkId: 'information'): FolderInformationPanelElement {
    this.clickOnAnchorLink(linkId);
    return new FolderInformationPanelElement(this.folderId);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  private showDashboard(linkId: 'dashboard') {
    this.clickOnAnchorLink(linkId);
    return new TestCaseStatisticPanelElement();
  }
}

export type TestCaseFolderAnchorLinks = 'dashboard' | 'information';
