import {UserView} from '../../../../model/user/user-view';

export class RequirementSearchModel {
  usersWhoCreatedRequirements: UserView[];
  usersWhoModifiedRequirements: UserView[];
}
