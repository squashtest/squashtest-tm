import {CreateEntityDialog} from '../../create-entity-dialog.element';
import {ProjectData} from '../../../../model/project/project-data.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class CreateHighLevelRequirementDialogElement extends CreateEntityDialog {

  criticalitySelectBox = new SelectFieldElement(CommonSelectors.fieldName('criticality'));
  categorySelectBox = new SelectFieldElement(CommonSelectors.fieldName('category'));

  constructor(project?: ProjectData, domain?: BindableEntity) {
    super({
      treePath: 'requirement-tree',
      viewPath: 'requirement-view/high-level/current-version',
      newEntityPath: 'new-high-level-requirement'
    }, project, domain);
  }
}
