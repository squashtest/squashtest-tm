import {RequirementVersionViewPage} from './requirement-version-view.page';
import {GridElement} from '../../../elements/grid/grid.element';

export class HighLevelRequirementVersionPage extends RequirementVersionViewPage {
  constructor(public requirementVersionId: number | '*') {
    super(requirementVersionId, 'sqtm-app-high-level-requirement-version-view');
  }

  get lowLevelRequirementTable() {
    return new GridElement('requirement-version-linked-low-level');
  }
}
