import {CampaignMenuItemIds} from './campaign-workspace.page';
import {ToolbarMenuButtonElement} from '../../elements/workspace-common/toolbar.element';
import {CreateCampaignDialog} from './dialogs/create-campaign-dialog.element';
import {ProjectData} from '../../../model/project/project-data.model';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {CreateIterationDialog} from './dialogs/create-iteration-dialog.element';
import {CreateSuiteDialog} from './dialogs/create-suite-dialog.element';
import {TreeToolbarElement} from '../../elements/workspace-common/tree-toolbar.element';
import {CreateCampaignFolderDialog} from './dialogs/create-campaign-folder-dialog.element';
import {selectByDataTestMenuItemId, selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';


export class CampaignWorkspaceTreeMenu extends TreeToolbarElement {
  constructor() {
    super('campaign-toolbar');
  }

  openCreateCampaign(projectData?: ProjectData): CreateCampaignDialog {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    menuItem.click();
    return new CreateCampaignDialog(projectData, BindableEntity.CAMPAIGN);
  }

  openAddIteration(projectData?: ProjectData): CreateIterationDialog {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    menuItem.click();
    return new CreateIterationDialog(projectData, BindableEntity.ITERATION);
  }

  openAddTestSuite(projectData?: ProjectData): CreateSuiteDialog {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);
    menuItem.click();
    return new CreateSuiteDialog(projectData, BindableEntity.TEST_SUITE);
  }

  assertCreateCampaignDisabled() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    menuItem.assertDisabled();
    createButtonElement.hideMenu();
  }

  assertCreateFolderDisabled() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    menuItem.assertDisabled();
    createButtonElement.hideMenu();
  }

  assertAddIterationDisabled() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    menuItem.assertDisabled();
    createButtonElement.hideMenu();
  }

  assertAddSuiteDisabled() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const menuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);
    menuItem.assertDisabled();
    createButtonElement.hideMenu();
  }

  createButton(): ToolbarMenuButtonElement {
    const createButtonElement = this.buttonMenu('create-button', 'create-menu');
    createButtonElement.assertExist();
    return createButtonElement;
  }

  openCreateCampaignFolder(): CreateCampaignFolderDialog {
    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-folder'))
      .click();
    return new CreateCampaignFolderDialog();
  }

  performAllChecksForCampaignLibrary() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);

    folderMenuItem.assertEnabled();
    campaignMenuItem.assertEnabled();
    iterationMenuItem.assertDisabled();
    testSuiteMenuItem.assertDisabled();
    createButtonElement.hideMenu();
    this.assertCopyButtonIsDisabled();
    this.assertDeleteButtonIsDisabled();
  }

  performAllChecksForCampaign() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);

    folderMenuItem.assertDisabled();
    campaignMenuItem.assertEnabled();
    iterationMenuItem.assertEnabled();
    testSuiteMenuItem.assertDisabled();
    createButtonElement.hideMenu();
    this.assertCopyButtonIsActive();
    this.assertDeleteButtonIsActive();
  }

  performAllChecksForIteration() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);

    folderMenuItem.assertDisabled();
    campaignMenuItem.assertDisabled();
    iterationMenuItem.assertDisabled();
    testSuiteMenuItem.assertEnabled();
    createButtonElement.hideMenu();
    this.assertCopyButtonIsActive();
    this.assertDeleteButtonIsActive();
  }

  assertAllMenuItemsDisabled() {
    const createButtonElement = this.createButton();
    const createMenuElement = createButtonElement.showMenu();
    const folderMenuItem = createMenuElement.item(CampaignMenuItemIds.newFolder);
    const campaignMenuItem = createMenuElement.item(CampaignMenuItemIds.newCampaign);
    const iterationMenuItem = createMenuElement.item(CampaignMenuItemIds.newIteration);
    const testSuiteMenuItem = createMenuElement.item(CampaignMenuItemIds.newTestSuite);

    folderMenuItem.assertDisabled();
    campaignMenuItem.assertDisabled();
    iterationMenuItem.assertDisabled();
    testSuiteMenuItem.assertDisabled();
    createButtonElement.hideMenu();
  }

}
