import {EntityViewPage, Page} from '../../page';

export class IterationLibraryViewPage extends EntityViewPage {

  constructor(private iterationLibraryId: number | string) {
    super('sqtm-app-iteration-view');
  }

  public checkDataFetched() {
    const url = `backend/iteration-library-view/${this.iterationLibraryId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T extends Page>(linkId: string, data?: any): T {
    throw Error('Implement me when needed !');
  }

}
