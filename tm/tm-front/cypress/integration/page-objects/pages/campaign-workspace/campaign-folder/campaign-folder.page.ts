import {EntityViewPage, Page} from '../../page';
import {apiBaseUrl, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignFolderViewIssuesPage} from './campaign-folder-view-issues.page';

export class CampaignFolderViewPage extends EntityViewPage {

  constructor(private campaignFolderId: number) {
    super('sqtm-app-campaign-folder-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/campaign-folder-view/${this.campaignFolderId}?**`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T extends Page>(linkId: string, data?: any): T {
    throw Error('Implement me when needed !');
  }

  showIssuesWithoutBindingToBugTracker(response?: any): CampaignFolderViewIssuesPage {
    const httpMock = new HttpMockBuilder('issues/campaign-folder/*?frontEndErrorIsHandled=true').responseBody(response).build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    httpMock.wait();
    cy.removeNzTooltip();
    return new CampaignFolderViewIssuesPage();
  }

  showIssuesIfBindedToBugTracker(modelResponse?: any, gridResponse?: GridResponse): CampaignFolderViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/campaign-folder/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse).build();
    const issues = new HttpMockBuilder('issues/campaign-folder/*/known-issues').responseBody(gridResponse).post().build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new CampaignFolderViewIssuesPage();
  }

}
