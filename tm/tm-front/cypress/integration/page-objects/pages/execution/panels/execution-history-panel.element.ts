import {Page} from '../../page';

export class ExecutionHistoryPanelElement extends Page {

  constructor() {
    super('sqtm-app-execution-page-history');
  }

}
