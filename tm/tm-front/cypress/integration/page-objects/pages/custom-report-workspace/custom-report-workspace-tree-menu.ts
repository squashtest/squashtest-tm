import {TreeToolbarElement} from '../../elements/workspace-common/tree-toolbar.element';
import {CreateEntityDialog} from '../create-entity-dialog.element';

export enum CustomReportCreateMenuItemIds {
  NEW_CHART = 'new-chart-definition',
  NEW_DASHBOARD = 'new-dashboard',
}

export class CustomReportWorkspaceTreeMenu extends TreeToolbarElement {
  constructor() {
    super('custom-report-toolbar');
  }

  // Reactivate when doing folders
  // assertCreateFolderButtonIsEnabled() {
  //   const menuItem = this.findCreateFolderButton();
  //   menuItem.assertExist();
  //   menuItem.assertEnabled();
  // }
  //
  // assertCreateFolderButtonIsDisabled() {
  //   const menuItem = this.findCreateFolderButton();
  //   menuItem.assertExist();
  //   menuItem.assertDisabled();
  // }

  // private findCreateFolderButton() {
  //   return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_FOLDER);
  // }

  private findCreateMenuButton(itemId: CustomReportCreateMenuItemIds) {
    const createButton = this.createButton();
    const menuElement = createButton.showMenu();
    return menuElement.item(itemId);
  }

  assertCreateChartButtonIsEnabled() {
    const menuButton = this.findCreateChartButton();
    menuButton.assertExist();
    menuButton.assertEnabled();
  }

  assertCreateChartButtonIsDisabled() {
    const menuButton = this.findCreateChartButton();
    menuButton.assertExist();
    menuButton.assertDisabled();
  }

  hideCreateMenu() {
    this.createButton().hideMenu();
  }

  assertCreateMenuIsDisabled() {
    this.createButton().assertExist();
    this.createButton().assertIsDisabled();
  }

  private findCreateChartButton() {
    return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_CHART);
  }

  assertCreateDashboardButtonIsEnabled() {
    const menuButton = this.findCreateDashboardButton();
    menuButton.assertExist();
    menuButton.assertEnabled();
  }

  assertCreateDashboardButtonIsDisabled() {
    const menuButton = this.findCreateDashboardButton();
    menuButton.assertExist();
    menuButton.assertDisabled();
  }

  private findCreateDashboardButton() {
    return this.findCreateMenuButton(CustomReportCreateMenuItemIds.NEW_DASHBOARD);
  }

  public openCreateDashboard(): CreateEntityDialog {
    const menuItemElement = this.findCreateDashboardButton();
    menuItemElement.click();
    return new CreateEntityDialog({
      viewPath: 'dashboard-view',
      newEntityPath: 'new-dashboard',
      treePath: 'custom-report-tree'
    });
  }

}
