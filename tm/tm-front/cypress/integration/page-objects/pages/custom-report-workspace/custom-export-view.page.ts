import {EntityViewPage, Page} from '../page';

export class CustomExportViewPage extends EntityViewPage {

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-custom-export-view');
  }

  clickAnchorLink(linkId: string): Page {
    // There's only one available anchor for this page...
    return undefined;
  }
}
