import {ConfirmDialogElement} from '../../../../elements/dialog/confirm-dialog.element';

export class ConfirmDisableOptionalDropdown extends ConfirmDialogElement {
  constructor() {
    super('cuf-disable-optional-dialog');
  }

  assertMessage(currentDefaultOptionLabel: string) {
    const message = 'Ce champ personnalisé va passer de facultatif à obligatoire.' +
      '\nPour respecter cette nouvelle contrainte, les valeurs non renseignées pour ce champ seront valorisées avec ' +
      `la valeur par défaut: "${currentDefaultOptionLabel}".` +
      '\nCette valorisation est automatique et irréversible.';

    this.assertHasMessage(message);
  }
}
