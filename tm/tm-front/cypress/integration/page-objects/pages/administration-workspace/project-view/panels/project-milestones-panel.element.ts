import {Page} from '../../../page';
import {GridElement} from '../../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {BindMilestoneToProjectDialog, MilestonesResponse} from '../dialogs/bind-milestone-to-project.dialog';
import {CreateBindMilestoneToProjectDialog} from '../dialogs/create-bind-milestone-to-project.dialog';
import {MilestoneAdminView} from '../../../../../model/milestone/milestone.model';
import {selectByDataTestToolbarButtonId} from '../../../../../utils/basic-selectors';


export class MilestonesPanelElement extends Page {
  public readonly grid: GridElement;

  constructor() {
    super('sqtm-app-project-milestones-panel');
    this.grid = GridElement.createGridElement('project-milestones');
  }

  waitInitialDataFetch() {
  }

  unbindOne(label: string) {
    this.grid.findRowId('label', label).then((id) => {
      this.grid.scrollPosition('right', 'mainViewport');
      this.grid
        .getRow(id)
        .cell('delete')
        .iconRenderer()
        .click();


      const deleteMock = new HttpMockBuilder('milestone-binding/project/*/unbind-milestones/*')
        .delete()
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();

    });
  }

  unbindMultiple(labels: string[]) {
    this.grid.selectRowsWithMatchingCellContent('label', labels);

    const deleteMock = new HttpMockBuilder('milestone-binding/project/*/unbind-milestones/*')
      .delete()
      .build();

    this.clickOnDeleteButton();


    this.clickConfirmDeleteButton();
    deleteMock.wait();

  }

  openMilestoneBindingDialog(availableMilestones?: MilestonesResponse): BindMilestoneToProjectDialog {
    const dialog = new BindMilestoneToProjectDialog(availableMilestones);

    cy.get(selectByDataTestToolbarButtonId('bind-milestones-button'))
      .should('exist')
      .click();

    dialog.waitInitialDataFetch();
    dialog.assertExist();

    return dialog;
  }

  openMilestoneCreateAndBindDialog(): CreateBindMilestoneToProjectDialog {
    const dialog = new CreateBindMilestoneToProjectDialog();


    cy.get(selectByDataTestToolbarButtonId('create-bind-milestone-button'))
      .should('exist')
      .click();

    dialog.assertExist();

    return dialog;
  }

  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('unbind-milestones-button'))
      .should('exist')
      .click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  showMilestoneDetail(milestoneName: string, response: MilestoneAdminView): void {
    const mock = new HttpMockBuilder('milestone-view/*').responseBody(response).build();
    const possibleOwnersMock = new HttpMockBuilder('milestones/possible-owners')
      .responseBody([]).build();

    this.grid.findRowId('label', milestoneName)
      .then((id) => {
        this.grid
          .getRow(id)
          .cell('label')
          .linkRenderer().findCellLink().click();
      });

    mock.wait();
    possibleOwnersMock.wait();
  }
}

