import {Page} from '../../../page';

export class ProjectPluginsPanelElement extends Page {
  constructor() {
    super('sqtm-app-project-plugins');
  }
}
