import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';

export class CannotRemoveBugtrackersAlert extends AlertDialogElement {
  constructor() {
    super('alert');
  }

  assertMessage() {
    this.assertHasMessage('Les serveurs sélectionnés ne peuvent pas être supprimés car au moins ' +
      'l\'un d\'eux est associé à au moins une synchronisation.\n\nVeuillez supprimer toutes les synchronisations avant ' +
      'de supprimer les serveurs.');
  }
}
