import {RemoveAdministrationEntityDialog} from '../remove-administration-entity-dialog';

export class RemoveUserDialogElement extends RemoveAdministrationEntityDialog {
  constructor(public readonly userIds: number[]) {
    super('users', 'users', userIds);
  }
}
