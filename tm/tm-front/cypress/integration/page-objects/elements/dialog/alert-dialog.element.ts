import Chainable = Cypress.Chainable;

export class AlertDialogElement {
  public constructor(protected dialogId = 'alert') {
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  close() {
    this.clickOnCloseButton();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  assertHasMessage(expected: string) {
    return this.getDialog().should('contain.text', expected);
  }

  private getDialog(): Chainable<any> {
    return cy.get(this.buildMessageSelector());
  }

  clickOnCloseButton() {
    const buttonSelector = this.buildButtonSelector('close');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
    cy.get(this.buildSelector()).should('not.exist');
  }

  buildMessageSelector(): string {
    return `${this.buildSelector()} [data-test-dialog-message]`;
  }

  protected buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
