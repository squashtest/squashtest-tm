import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {ReferentialData} from '../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../utils/referential/referential-data.provider';
import {GridResponse} from '../../../model/grids/data-row.type';
import {TestCaseSearchModel} from '../../pages/test-case-workspace/research/test-case-search-model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {GridElement} from '../grid/grid.element';
import {TestCaseSearchPage} from '../../pages/test-case-workspace/research/test-case-search-page';

// for now we have only pie chart
// please refactor this to an abstract class and subclass for other types when needed
export class ChartElement {
  constructor(private chartId: string) {
  }

  assertChartExist() {
    const selector = `${selectByDataTestComponentId(this.chartId)} .plotly`;
    cy.get(selector).should('exist');
  }

  assertHasTitle(expectedTitle: string) {
    const selector = `${selectByDataTestComponentId(this.chartId)} .infolayer .gtitle`;
    cy.get(selector).should('contain.text', expectedTitle);
  }

  private clickOnLegend(chartZoneIndex: number) {
    const selector = `${selectByDataTestComponentId(this.chartId)}`;
    cy.get(selector)
      .find('.legend')
      .find('.traces')
      .eq(chartZoneIndex)
      .should('be.visible')
      .click();
  }

  goToResearchPage (
    chartZoneIndex: number,
    referentialData: ReferentialData = basicReferentialData,
    initialNodes: GridResponse = {dataRows: []},
    testCaseResearchModel: TestCaseSearchModel = {usersWhoCreatedTestCases: [], usersWhoModifiedTestCases: []}
  ): TestCaseSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case').responseBody(testCaseResearchModel).build();
    const grid = GridElement.createGridElement('test-case-search', 'search/test-case', initialNodes);
    // visit page
    this.clickOnLegend(chartZoneIndex);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new TestCaseSearchPage(grid);
  }

}
