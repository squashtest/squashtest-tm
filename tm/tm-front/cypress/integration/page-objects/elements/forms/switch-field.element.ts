import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class SwitchFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string,
              private readonly url?: string) {
    super(selectorOrFieldId);
  }

  toggle() {
    if (Boolean(this.url)) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.selector.find('button').click();
      mock.wait();
    } else {
      this.selector.find('button').click();
    }
  }

  checkValue(shouldBeChecked: boolean) {
    const chainer = shouldBeChecked ? 'have.class' : 'not.have.class';
    this.selector.find('button').should(chainer, 'ant-switch-checked');
  }
}
