import Chainable = Cypress.Chainable;
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class SelectFieldElement extends AbstractFormFieldElement {

  constructor(selectorFactory: ElementSelectorFactory) {
    super(selectorFactory);
  }

  get tagInputSelector(): Chainable {
    return this.selector.find('input');
  }

  // Shows the dropdown and click on an option
  selectValue(newValue: string) {
    this.selector.click();
    this.getOption(newValue).click();
  }

  fillTagValue(newValue: string) {
    this.tagInputSelector
      .type(newValue)
      .type('{enter}');
  }

  focusTagInput() {
    this.tagInputSelector.click();
  }

  typeTag(newValue: string): void {
    this.tagInputSelector.type(newValue);
  }

  // Assumes the overlay is already shown!
  checkVisibleOptions(options: string[]) {
    options.forEach(option => cy.contains('nz-option-item', option).should('exist'));
  }

  // Checks that the selected option matches the specified text
  checkSelectedOption(expected: string) {
    this.selector.should('contain.text', expected);
  }

  /** @deprecated because of ambiguous semantic.
   * Use 'selectValue' if you deal with a dropdown list
   * Use 'fillTagValue' and 'type' if you're dealing with tags
   */
  setValue(newValue: string) {
    this.selectValue(newValue);
  }

  /** Show dropdown and check that the available options names match the specified string array. */
  checkAllOptions(options: string[]) {
    this.selector.click();
    this.checkVisibleOptions(options);
    this.selector.click();
  }

  // Returns the Chainable for an option of the dropdown menu
  // This won't work if the dropdown isn't shown.
  private getOption(optionName: string): Chainable<any> {
    return cy.contains(`nz-option-item`, optionName);
  }
}
