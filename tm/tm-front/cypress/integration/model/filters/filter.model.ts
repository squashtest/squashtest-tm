import {Type} from '@angular/core';
import {Identifier} from '../grids/data-row.type';

export interface FilterData {
  scope: Scope;
  filter: Filter;
}

export interface EntityScope {
  id: string;
  label: string;
  projectId: number;
}

export type SimpleScope = Pick<Scope, 'kind' | 'value'>;

export type SimpleFilter = Pick<Filter, 'id' | 'value' | 'operation'>;

export interface Scope {
  value: EntityScope[];
  initialValue: EntityScope[];
  kind: 'project' | 'custom';
  active: boolean;
}

export interface Filter {
  id: Identifier;
  // each filter should have either a label (typically for custom fields)
  // or an i18nKey (all system fields)
  label?: string;
  i18nLabelKey?: string;
  // Widget for rendering
  widget?: Type<any>;
  // Widget for rendering
  valueRenderer?: Type<any>;
  // Value can be null
  value?: FilterValue;
  // Column prototype unique key for filters backed by custom report engine server side.
  columnPrototype?: ResearchColumnPrototype;
  // Available operations. The model include all operation of custom report engine.
  availableOperations?: FilterOperation[];
  // Current selected operation.
  operation?: FilterOperation;
  // For filters groups. Use in filter manager.
  groupId?: string;
  // Id of the custom field.
  cufId?: number;
}

// Model used server side mainly for the research controllers.
// Other backend controllers will have to use the same data shape.
export interface FilterValueModel {
  operation?: string;
  columnPrototype?: string;
  values: string[];
  id: string;
  cufId?: number;
}

export interface FilterGroup {
  id: Identifier;
  i18nLabelKey: string;
}

export enum FilterOperation {
  EQUALS = 'EQUALS',
  LIKE = 'LIKE',
  IN = 'IN',
  AND = 'AND',
  GREATER = 'GREATER',
  GREATER_EQUAL = 'GREATER_EQUAL',
  LOWER = 'LOWER',
  LOWER_EQUAL = 'LOWER_EQUAL',
  BETWEEN = 'BETWEEN',
  FULLTEXT = 'FULLTEXT'
}

export enum ResearchColumnPrototype {
  TEST_CASE_PROJECT_NAME = 'TEST_CASE_PROJECT_NAME',
  TEST_CASE_ID = 'TEST_CASE_ID',
  TEST_CASE_NAME = 'TEST_CASE_NAME',
  TEST_CASE_REFERENCE = 'TEST_CASE_REFERENCE',
  TEST_CASE_STATUS = 'TEST_CASE_STATUS',
  TEST_CASE_KIND = 'TEST_CASE_KIND',
  TEST_CASE_IMPORTANCE = 'TEST_CASE_IMPORTANCE',
  TEST_CASE_NATURE = 'TEST_CASE_NATURE',
  TEST_CASE_NATURE_ID = 'TEST_CASE_NATURE_ID',
  TEST_CASE_TYPE = 'TEST_CASE_TYPE',
  TEST_CASE_PROJECT_ID = 'TEST_CASE_PROJECT_ID',
  TEST_CASE_AUTOMATABLE = 'TEST_CASE_AUTOMATABLE',
  TEST_CASE_CREATED_BY = 'TEST_CASE_CREATED_BY',
  TEST_CASE_CREATED_ON = 'TEST_CASE_CREATED_ON',
  TEST_CASE_MODIFIED_BY = 'TEST_CASE_MODIFIED_BY',
  TEST_CASE_MODIFIED_ON = 'TEST_CASE_MODIFIED_ON',
  TEST_CASE_MILCOUNT = 'TEST_CASE_MILCOUNT',
  TEST_CASE_ATTCOUNT = 'TEST_CASE_ATTCOUNT',
  TEST_CASE_VERSCOUNT = 'TEST_CASE_VERSCOUNT',
  TEST_CASE_STEPCOUNT = 'TEST_CASE_STEPCOUNT',
  TEST_CASE_ITERCOUNT = 'TEST_CASE_ITERCOUNT',
  TEST_CASE_CUF_TAG = 'TEST_CASE_CUF_TAG',
  TEST_CASE_CUF_TEXT = 'TEST_CASE_CUF_TEXT',
  TEST_CASE_CUF_NUMERIC = 'TEST_CASE_CUF_NUMERIC',
  TEST_CASE_DESCRIPTION = 'TEST_CASE_DESCRIPTION',
  TEST_CASE_PREQUISITE = 'TEST_CASE_PREQUISITE',
  TEST_CASE_TYPE_ID = 'TEST_CASE_TYPE_ID',
  AUTOMATION_REQUEST_STATUS = 'AUTOMATION_REQUEST_STATUS'
}

export interface DiscreteFilterValue {
  id: string | number;
  label?: string;
  i18nLabelKey?: string;
}

export abstract class FilterValue {
  readonly kind: FilterValueKind;
  value: string | string[] | DiscreteFilterValue[];
}

export class StringFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'single-string-value';
  value: string;
}

export class DateFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'single-date-value';
  value: string;
}


export class MultiDateFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-date-value';
  value: string[];
}

export class MultipleStringFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-string-value';
  value: string[];
}

export class MultiDiscreteFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-discrete-value';
  value: DiscreteFilterValue[];
}

export function isStringValue(value: FilterValue): value is StringFilterValue {
  return value.kind === 'single-string-value';
}

export function isMultiStringValue(value: FilterValue): value is MultipleStringFilterValue {
  return value.kind === 'multiple-string-value';
}


export function isDiscreteValue(value: FilterValue): value is MultiDiscreteFilterValue {
  return value.kind === 'multiple-discrete-value';
}

export function isDateValue(value: FilterValue): value is DateFilterValue {
  return value.kind === 'single-date-value';
}

export function isMultiDateValue(value: FilterValue): value is MultiDateFilterValue {
  return value.kind === 'multiple-date-value';
}

export type FilterValueKind = 'single-string-value' | 'single-date-value' | 'multiple-string-value' |
  'multiple-date-value' | 'multiple-discrete-value';

export interface FilteringChange {
  filterValue?: FilterValue;
  operation?: FilterOperation;
}
