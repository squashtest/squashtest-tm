import {PermissionGroup} from '../project/project.model';
import {AuthenticatedUser} from './authenticated-user.model';

export interface User {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
  email: string;
  active: boolean;
  createdOn: Date;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  lastConnectedOn: Date;
  usersGroupBinding: number;
  usersGroups: UsersGroup[];
  projectPermissions: ProjectPermission[];
  teams: AssociatedTeam[];
  canManageLocalPassword: boolean;
}

export interface ProjectPermission {
  projectId: number;
  projectName: string;
  permissionGroup: PermissionGroup;
}

export interface AssociatedTeam {
  partyId: number;
  name: string;
}

export interface UsersGroup {
  id: number;
  qualifiedName: string;
}

export interface SimpleUser {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
}

export function formatFullUserName(user: User | SimpleUser | AuthenticatedUser): string {
  const login = isAuthenticatedUser(user) ? user.username : user.login;
  const fullName = Boolean(user.firstName) ? `${user.firstName} ${user.lastName}` : user.lastName;
  return `${fullName} (${login})`;
}

function isAuthenticatedUser(user: any): user is AuthenticatedUser {
  return typeof user.username === 'string';
}
