import {ChartDefinitionModel} from './chart-definition.model';
import {WorkspaceKeys} from '../level-enums/level-enum';
import {ReportDefinitionModel} from './report-definition.model';

export interface CustomDashboardBinding {
  id: number;
  dashboardId: number;
  row: number;
  col: number;
  sizeX: number;
  sizeY: number;
}

export interface ChartBinding extends CustomDashboardBinding {
  chartDefinitionId: number;
  chartInstance: ChartDefinitionModel;
}

export interface ReportBinding extends CustomDashboardBinding {
  reportDefinitionId: number;
  reportInstance: ReportDefinitionModel;
}

export interface CustomDashboardModel {
  id: number;
  projectId: number;
  customReportLibraryNodeId: number;
  name: string;
  createdBy: string;
  chartBindings: ChartBinding[];
  reportBindings: ReportBinding[];
  favoriteWorkspaces: WorkspaceKeys[];
}
