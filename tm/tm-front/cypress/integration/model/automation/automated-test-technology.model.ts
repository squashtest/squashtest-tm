export class AutomatedTestTechnology {
  id: number;
  name: string;
  actionProviderKey: string;
  premium: boolean;
}
