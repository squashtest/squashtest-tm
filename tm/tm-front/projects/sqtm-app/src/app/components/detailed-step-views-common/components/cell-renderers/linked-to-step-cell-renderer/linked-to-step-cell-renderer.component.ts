import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, GridService} from 'sqtm-core';
import {concatMap, filter, finalize, take, tap} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {STEP_VIEW_REQUIREMENT_LINK_SERVICE} from '../../../detailed-step-view.constant.ts.constant';
import {StepViewRequirementLinkService} from '../../../step-view-requirement-link.service';

@Component({
  selector: 'sqtm-app-linked-to-step-cell-renderer',
  templateUrl: './linked-to-step-cell-renderer.component.html',
  styleUrls: ['./linked-to-step-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinkedToStepCellRendererComponent extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  get linkedToStep() {
    return this.row.data.linkedToStep;
  }

  private _async = new BehaviorSubject<boolean>(false);

  serverOperationRunning$ = this._async.asObservable();

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef, @Inject(STEP_VIEW_REQUIREMENT_LINK_SERVICE) public viewService: StepViewRequirementLinkService) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this._async.complete();
  }

  changeLinkState() {
    this.viewService.componentData$.pipe(
      take(1),
      filter(data => data.permissions.canWrite && data.milestonesAllowModification),
      tap(() => this.startAsyncOperation()),
      concatMap(() => {
        if (this.row.data.linkedToStep) {
          return this.viewService.deleteStepCoverages([this.row.id as number]);
        } else {
          return this.viewService.linkRequirementToCurrentStep(this.row.id as number);
        }
      }),
      finalize(() => this.stopAsyncOperation())
    ).subscribe();
  }


  private startAsyncOperation() {
    this._async.next(true);
  }

  private stopAsyncOperation() {
    this._async.next(false);
  }
}
