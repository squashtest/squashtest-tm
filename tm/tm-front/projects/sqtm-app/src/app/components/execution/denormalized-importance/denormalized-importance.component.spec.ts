import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DenormalizedImportanceComponent } from './denormalized-importance.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

describe('DenormalizedImportanceComponent', () => {
  let component: DenormalizedImportanceComponent;
  let fixture: ComponentFixture<DenormalizedImportanceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ DenormalizedImportanceComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenormalizedImportanceComponent);
    component = fixture.componentInstance;
    component.importanceKey = 'VERY_HIGH';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
