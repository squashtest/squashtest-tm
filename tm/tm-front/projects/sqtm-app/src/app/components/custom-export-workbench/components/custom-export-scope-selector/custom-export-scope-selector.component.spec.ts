import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomExportScopeSelectorComponent} from './custom-export-scope-selector.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';

describe('CustomExportScopeSelectorComponent', () => {
  let component: CustomExportScopeSelectorComponent;
  let fixture: ComponentFixture<CustomExportScopeSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, OverlayModule],
      declarations: [CustomExportScopeSelectorComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportScopeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
