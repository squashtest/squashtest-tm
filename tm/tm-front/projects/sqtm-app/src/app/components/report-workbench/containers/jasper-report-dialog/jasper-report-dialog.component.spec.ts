import {ComponentFixture, TestBed} from '@angular/core/testing';

import {JasperReportDialogComponent} from './jasper-report-dialog.component';
import {JasperReportService} from '../../services/jasper-report.service';
import {DialogReference} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('JasperReportDialogComponent', () => {
  let component: JasperReportDialogComponent;
  let fixture: ComponentFixture<JasperReportDialogComponent>;
  const dialogReferenceMock = {data: {report: {views: [{formats: ['pdf']}]}}};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      declarations: [JasperReportDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReferenceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JasperReportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
