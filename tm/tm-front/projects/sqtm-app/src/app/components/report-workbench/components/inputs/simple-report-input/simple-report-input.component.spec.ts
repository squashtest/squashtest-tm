import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SimpleReportInputComponent} from './simple-report-input.component';
import {ReportInputType} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';

describe('SimpleReportInputComponent', () => {
  let component: SimpleReportInputComponent;
  let fixture: ComponentFixture<SimpleReportInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), ReactiveFormsModule, NzCheckboxModule],
      declarations: [SimpleReportInputComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleReportInputComponent);
    component = fixture.componentInstance;
    component.reportInput = {
      name: 'report',
      type: ReportInputType.RADIO_BUTTONS_GROUP,
      required: false,
      label: 'report'
    };
    component.errors = [];
    component.subFormGroup = new FormGroup({report: new FormControl('')});
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
