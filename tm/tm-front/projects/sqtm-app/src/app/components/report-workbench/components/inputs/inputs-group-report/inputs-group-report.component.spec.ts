import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputsGroupReportComponent } from './inputs-group-report.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ReportInputType} from 'sqtm-core';

describe('InputsGroupReportComponent', () => {
  let component: InputsGroupReportComponent;
  let fixture: ComponentFixture<InputsGroupReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ InputsGroupReportComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputsGroupReportComponent);
    component = fixture.componentInstance;
    component.reportInput = {
      name: 'report',
      inputs: [],
      type: ReportInputType.INPUTS_GROUP,
      required: false,
      label: 'report',
    };
    component.errors = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
