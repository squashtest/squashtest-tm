import {ComponentFixture, TestBed} from '@angular/core/testing';

import {FormRequirementTreePickerComponent} from './form-requirement-tree-picker.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('FormRequirementTreePickerComponent', () => {
  let component: FormRequirementTreePickerComponent;
  let fixture: ComponentFixture<FormRequirementTreePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormRequirementTreePickerComponent],
      imports: [OverlayModule, HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRequirementTreePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
