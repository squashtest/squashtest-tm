import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EMPTY } from 'rxjs';
import { DialogReference } from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../utils/testing-utils/app-testing-utils.module';
import { DuplicateActionWordDialogConfiguration } from './duplicate-action-word-dialog-configuration';

import { DuplicateActionWordsDialogComponent } from './duplicate-action-words-dialog.component';

describe('DuplicateActionWordsDialogComponent', () => {
  let component: DuplicateActionWordsDialogComponent;
  let fixture: ComponentFixture<DuplicateActionWordsDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);
  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference = new DialogReference<DuplicateActionWordDialogConfiguration>(
    'duplicate-action-word',
    null,
    overlayReference,
    { duplicateActionWords: [{ projectName: 'Project1', actionWordId: 45 }] }
  );

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DuplicateActionWordsDialogComponent ],
      imports: [ AppTestingUtilsModule ],
      providers: [{
        provide: DialogReference, useValue: dialogReference
      }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DuplicateActionWordsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
