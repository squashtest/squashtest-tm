import {ChangeDetectionStrategy, Component, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'sqtm-app-remote-attachment-field',
  templateUrl: './remote-attachment-field.component.html',
  styleUrls: ['./remote-attachment-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteAttachmentFieldComponent {

  @ViewChild('fileInput')
  fileInput: ElementRef;

  hoverCount = 0;

  addedFiles: File[] = [];

  constructor() {
  }

  get isHovered(): boolean {
    return this.hoverCount > 0;
  }

  onAreaDragEnter() {
    this.hoverCount++;
  }

  onAreaDragExit(): void {
    this.hoverCount--;
  }

  addAttachmentClicked(): void {
    this.fileInput.nativeElement.click();
  }

  onFileChange($event: Event): void {
    const input = $event.target as HTMLInputElement;
    this.addFiles(input.files);
    input.value = '';
  }

  onFileDrop($event: DragEvent): void {
    $event.stopPropagation();
    $event.preventDefault();
    this.hoverCount = 0;
    this.addFiles($event.dataTransfer.files);
  }

  private addFiles(fileList: FileList): void {
    const files: File[] = Array.prototype.slice.call(fileList);

    if (files.length) {
      this.addedFiles.push(...files);
    }
  }

  removeAttachmentClicked(file: File): void {
    this.addedFiles = this.addedFiles.filter(f => f !== file);
  }
}
