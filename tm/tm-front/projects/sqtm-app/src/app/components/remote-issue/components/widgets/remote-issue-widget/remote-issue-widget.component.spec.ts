import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteIssueWidgetComponent } from './remote-issue-widget.component';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {DialogService} from 'sqtm-core';
import {mockRemoteIssueService} from '../../../../../utils/testing-utils/mocks.service';

describe('RemoteIssueWidgetComponent', () => {
  let component: RemoteIssueWidgetComponent;
  let fixture: ComponentFixture<RemoteIssueWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoteIssueWidgetComponent ],
      providers: [
        { provide: RemoteIssueService, useValue: mockRemoteIssueService() },
        { provide: DialogService, useValue: {} },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteIssueWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
