import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {AbstractFormField, DisplayOption, Field, FieldValidationError} from 'sqtm-core';
import {FormGroup} from '@angular/forms';
import {AutocompleteDataSource} from 'ng-zorro-antd/auto-complete';

@Component({
  selector: 'sqtm-app-remote-combo-box-field',
  templateUrl: './remote-combo-box-field.component.html',
  styleUrls: ['./remote-combo-box-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteComboBoxFieldComponent extends AbstractFormField {

  @Input() field: Field;
  @Input() formGroup: FormGroup;
  @Input() options: DisplayOption[];

  get fieldName(): string {
    return this.field.id;
  }

  // Inherited but not used for now
  serverSideFieldValidationError: FieldValidationError[];

  public constructor(cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  private get currentValue(): string {
    return this.formGroup.controls[this.field.id].value;
  }

  getAutocompleteOptions(): AutocompleteDataSource {
    if (! Array.isArray(this.options)) {
      return [];
    }

    return this.options.map(option => option.label).filter(label => label.includes(this.currentValue));
  }
}
