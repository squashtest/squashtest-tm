import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NewRemoteIssueFormComponent} from './new-remote-issue-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BTIssue} from 'sqtm-core';
import {RemoteIssueDialogState} from '../../../states/remote-issue-dialog.state';
import {mockRemoteIssueService} from '../../../../../utils/testing-utils/mocks.service';

describe('NewRemoteIssueFormComponent', () => {
  let component: NewRemoteIssueFormComponent;
  let fixture: ComponentFixture<NewRemoteIssueFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [NewRemoteIssueFormComponent],
      providers: [
        {provide: RemoteIssueService, useValue: mockRemoteIssueService()}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRemoteIssueFormComponent);
    component = fixture.componentInstance;
    component.remoteIssueDialogState = mockDialogState();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


function makeBTIssue(): BTIssue {
  return {
    assignee: undefined,
    bugtracker: '',
    category: undefined,
    comment: '',
    createdOn: undefined,
    description: '',
    hasBlankId: false,
    id: '',
    priority: undefined,
    project: {
      categories: [],
      versions: [],
      users: [],
      priorities: [],
      defaultIssuePriority: {
        id: 'low', name: 'low',
      },
      id: 1,
      name: 'prj',
    },
    reporter: undefined,
    state: undefined,
    status: undefined,
    summary: '',
    version: undefined,
    getNewKey(): string {
      return '';
    }
  };
}

function mockDialogState(): RemoteIssueDialogState {
  return {
    bugTrackerId: 1,
    attachMode: false,
    reportForm: makeBTIssue(),
    remoteProjectName: 'P12',
    boundEntity: {
      boundEntityId: 12,
      bindableEntity: 'EXECUTION_TYPE',
    },
    squashProjectId: 1,
    bugTrackerInfo: {
      kind: 'BTKIND',
      projectNames: ['P12', 'P13'],
    },
    searchForm: {
      fields: [],
    }
  };
}
