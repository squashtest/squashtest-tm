import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteComboBoxFieldComponent } from './remote-combo-box-field.component';

describe('RemoteComboBoxFieldComponent', () => {
  let component: RemoteComboBoxFieldComponent;
  let fixture: ComponentFixture<RemoteComboBoxFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoteComboBoxFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteComboBoxFieldComponent);
    component = fixture.componentInstance;
    component.field = {
      possibleValues: [],
      id: '',
      rendering: null,
      label: '',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
