import {CascadingSelectOption, DisplayOption, Field, ListItem} from 'sqtm-core';
import {
  DATE_FORMAT,
  DATE_TIME_FORMAT,
  getPossibleValuesForField
} from './new-advanced-issue-form/new-advanced-issue-form.model';
import {parse} from 'date-fns';

/**
 * Utility class for advanced issue forms (report issue and search issue).
 */
export class FieldToWidgetHelper {

  public static getOptionsForField(field: Field): DisplayOption[] {
    return getPossibleValuesForField(field)?.map(value => ({id: value.id, label: value.name})) ?? [];
  }

  static getFirstAvailableOption(field: Field): DisplayOption | undefined {
    return this.getOptionsForField(field)[0];
  }

  // We need a different method for checkbox_list type because we're expecting a scalar with a
  // comma-separated list of labels...
  public static getCheckboxOptionsForField(field: Field): DisplayOption[] {
    return getPossibleValuesForField(field)?.map(value => ({id: value.name, label: value.name})) ?? [];
  }

  // Here again, server is expecting labels (not ids)
  public static getCascadeOptionsForField(field: Field): CascadingSelectOption[] {
    // We know we won't have more than 2 levels so we don't need to make it recursive
    return getPossibleValuesForField(field)?.map(value => ({
      id: value.name,
      label: value.name,
      children: value.composite
        .map(child => ({id: child.name, label: child.name, children: []}))
    }));
  }

  public static getListItemsForField(field: Field): ListItem[] {
    return getPossibleValuesForField(field)?.map(value => ({
      id: value.id,
      label: value.name,
    })) ?? [];
  }

  public static parseDate(candidate: any): Date {
    if (candidate instanceof Date) {
      return candidate;
    }

    const date = parse(candidate, DATE_FORMAT, new Date());
    return isValidDate(date) ? date : null;
  }

  public static parseDateTime(candidate: any): Date {
    if (candidate instanceof Date) {
      return candidate;
    }

    const date = parse(candidate, DATE_TIME_FORMAT, new Date());
    return isValidDate(date) ? date : null;
  }
}

function isValidDate(date: any): boolean {
  return date instanceof Date
    && !isNaN(date.getTime());
}
