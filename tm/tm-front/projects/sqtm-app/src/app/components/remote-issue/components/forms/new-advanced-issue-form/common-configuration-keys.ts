export enum CommonConfigurationKey {
  FORMAT = 'format',
  TIME_FORMAT = 'time-format',
  ONCHANGE = 'onchange',
  MAX_LENGTH = 'max-length',
  CONFIRM_MESSAGE = 'confirmMessage',
  HELP_MESSAGE = 'help-message',
  FILTER_POSSIBLE_VALUES = 'filter-possible-values',
  RENDER_AS_HTML = 'render-as-html',
}
