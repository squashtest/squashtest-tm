import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteCommandTagFieldComponent } from './remote-command-tag-field.component';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {EMPTY} from 'rxjs';
import {mockRemoteIssueService} from '../../../../../utils/testing-utils/mocks.service';

describe('RemoteCommandTagFieldComponent', () => {
  let component: RemoteCommandTagFieldComponent;
  let fixture: ComponentFixture<RemoteCommandTagFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoteCommandTagFieldComponent ],
      providers: [
        { provide: RemoteIssueService, useValue: mockRemoteIssueService() }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteCommandTagFieldComponent);
    component = fixture.componentInstance;
    component.fieldName = 'test';
    component.formGroup = {
      controls: {
        [component.fieldName]: { valueChanges: EMPTY }
      },
    } as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
