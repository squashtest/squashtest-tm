import {
  GridResponse,
} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {emptyTableSection, SectionKind, TableRow, TableSection} from '../state/PrintData.state';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class PrintSectionService {

  constructor(public translateService: TranslateService) {
  }

  public buildIssuesSection(issuesGrid: GridResponse, isRequirementVersionPrint: boolean): TableSection {
    if (issuesGrid.dataRows.length === 0) {
      return emptyTableSection('sqtm-core.test-case-workspace.title.issues');
    }

    const header = [
      'sqtm-core.entity.issue.key.label',
      'sqtm-core.entity.issue.project.label',
      'sqtm-core.entity.issue.summary.label',
      'sqtm-core.entity.issue.priority.label',
      'sqtm-core.entity.issue.status.label',
      'sqtm-core.entity.issue.assignee.label',
      'sqtm-core.entity.issue.reported-in.label',
    ];

    if (isRequirementVersionPrint) {
      header.push('sqtm-core.entity.issue.attached-to-requirement-version');
    }

    const rows: TableRow[] = issuesGrid.dataRows.map((dataRow) => {
      const issueRow = dataRow.data;
      const executionOrder = issueRow.executionOrder + 1;
      const suiteNames = issueRow.suiteNames;
      const executionName = issueRow.executionName;
      const reportedIn = Boolean(issueRow.suiteNames) ?
        `${executionName} (${this.translateService.instant('sqtm-core.entity.test-suite.label.singular').toLowerCase()} '${suiteNames}', exe. #${executionOrder})` :
        `${executionName} (exe. #${executionOrder})`;
      const values = [
        issueRow.remoteKey,
        issueRow.btProject,
        issueRow.summary,
        issueRow.priority,
        issueRow.status,
        issueRow.assignee,
        reportedIn,
      ];
      if (isRequirementVersionPrint) {
        values.push(issueRow.reqVersionName);
      }
      return {values};
    });

    return {
      kind: SectionKind.table,
      title: 'sqtm-core.test-case-workspace.title.issues',
      rows,
      header
    };
  }

}
