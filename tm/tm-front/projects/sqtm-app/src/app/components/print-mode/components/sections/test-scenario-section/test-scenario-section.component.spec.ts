import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestScenarioSectionComponent } from './test-scenario-section.component';

describe('TestScenarioSectionComponent', () => {
  let component: TestScenarioSectionComponent;
  let fixture: ComponentFixture<TestScenarioSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestScenarioSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestScenarioSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
