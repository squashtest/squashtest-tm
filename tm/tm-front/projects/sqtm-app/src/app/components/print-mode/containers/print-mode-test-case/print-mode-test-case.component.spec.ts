import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PrintModeTestCaseComponent} from './print-mode-test-case.component';
import {ActivatedRoute} from '@angular/router';
import {mockPassThroughTranslateService, mockRestService} from '../../../../utils/testing-utils/mocks.service';
import {ReferentialDataService, RestService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import createSpyObj = jasmine.createSpyObj;
import {DatePipe} from '@angular/common';
import {ConvertFileSizePipe} from 'sqtm-core';

describe('PrintModeTestCaseComponent', () => {
  let component: PrintModeTestCaseComponent;
  let fixture: ComponentFixture<PrintModeTestCaseComponent>;
  const restServiceMock = {} as RestService;
  restServiceMock.get = jasmine.createSpy().and.returnValue(EMPTY);
  restServiceMock.post = jasmine.createSpy().and.returnValue(EMPTY);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: {
            refresh: () => EMPTY,
            referentialData$: EMPTY,
            connectToProjectData: () => EMPTY,
          }
        },
        {provide: DatePipe, useValue: {}},
        {provide: ConvertFileSizePipe, useClass: ConvertFileSizePipe},
        {provide: ActivatedRoute, useValue: {snapshot: {paramMap: createSpyObj<Map<any, any>>(['get'])}}},
        {provide: RestService, useValue: mockRestService()},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
      ],
      declarations: [PrintModeTestCaseComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintModeTestCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
