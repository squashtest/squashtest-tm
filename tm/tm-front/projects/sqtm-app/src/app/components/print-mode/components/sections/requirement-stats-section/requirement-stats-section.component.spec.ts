import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementStatsSectionComponent } from './requirement-stats-section.component';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceCommonModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('InformationSectionComponent', () => {
  let component: RequirementStatsSectionComponent;
  let fixture: ComponentFixture<RequirementStatsSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), WorkspaceCommonModule],
      declarations: [ RequirementStatsSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementStatsSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
