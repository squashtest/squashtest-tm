import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintModePageComponent } from './print-mode-page.component';

describe('PrintModePageComponent', () => {
  let component: PrintModePageComponent;
  let fixture: ComponentFixture<PrintModePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintModePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintModePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
