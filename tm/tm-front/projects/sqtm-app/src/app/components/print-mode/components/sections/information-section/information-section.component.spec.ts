import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationSectionComponent } from './information-section.component';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceCommonModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('InformationSectionComponent', () => {
  let component: InformationSectionComponent;
  let fixture: ComponentFixture<InformationSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), WorkspaceCommonModule],
      declarations: [ InformationSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
