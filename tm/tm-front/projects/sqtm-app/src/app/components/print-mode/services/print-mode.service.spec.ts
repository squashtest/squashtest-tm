import { TestBed } from '@angular/core/testing';

import { PrintModeService } from './print-mode.service';

describe('PrintModeService', () => {
  let service: PrintModeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrintModeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
