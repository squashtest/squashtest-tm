import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintFieldValueComponent } from './print-field-value.component';

describe('PrintFieldValueComponent', () => {
  let component: PrintFieldValueComponent;
  let fixture: ComponentFixture<PrintFieldValueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintFieldValueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintFieldValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
