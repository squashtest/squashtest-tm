import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PrintModeRequirementComponent} from './print-mode-requirement.component';
import {ActivatedRoute} from '@angular/router';
import {mockPassThroughTranslateService} from '../../../../utils/testing-utils/mocks.service';
import {ConvertFileSizePipe,  ReferentialDataService, RestService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import createSpyObj = jasmine.createSpyObj;
import {DatePipe} from '@angular/common';

describe('PrintModeRequirementComponent', () => {
  let component: PrintModeRequirementComponent;
  let fixture: ComponentFixture<PrintModeRequirementComponent>;
  const restServiceMock = {} as RestService;
  restServiceMock.get = jasmine.createSpy().and.returnValue(EMPTY);
  restServiceMock.post = jasmine.createSpy().and.returnValue(EMPTY);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: {
            refresh: () => EMPTY,
            referentialData$: EMPTY,
            connectToProjectData: () => EMPTY,
          }
        },
        {provide: ConvertFileSizePipe, useValue: {}},
        {provide: DatePipe, useValue: {}},
        {provide: ActivatedRoute, useValue: {snapshot: {paramMap: createSpyObj<Map<any, any>>(['get'])}}},
        {provide: RestService, useValue: restServiceMock},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
      ],
      declarations: [PrintModeRequirementComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintModeRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
