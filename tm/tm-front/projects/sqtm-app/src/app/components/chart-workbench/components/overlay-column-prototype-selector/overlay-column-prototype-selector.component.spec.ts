import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OverlayColumnPrototypeSelectorComponent } from './overlay-column-prototype-selector.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('OverlayColumnPrototypeSelectorComponent', () => {
  let component: OverlayColumnPrototypeSelectorComponent;
  let fixture: ComponentFixture<OverlayColumnPrototypeSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [ OverlayColumnPrototypeSelectorComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayColumnPrototypeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
