import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ChartFilterValueRendererComponent, ChartWorkbenchFilter} from './chart-filter-value-renderer.component';
import {TranslateModule} from '@ngx-translate/core';
import {EntityType} from 'sqtm-core';
import {DatePipe} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ChartFilterValueRendererComponent', () => {
  let component: ChartFilterValueRendererComponent;
  let fixture: ComponentFixture<ChartFilterValueRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ChartFilterValueRendererComponent],
      providers: [DatePipe],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartFilterValueRendererComponent);
    component = fixture.componentInstance;
    const filter: ChartWorkbenchFilter = {
      id: '1',
      entityType: EntityType.REQUIREMENT,
      preventOpening: true
    };
    component.filter = filter;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
