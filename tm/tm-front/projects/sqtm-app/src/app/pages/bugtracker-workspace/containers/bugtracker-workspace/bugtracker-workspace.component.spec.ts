import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BugtrackerWorkspaceComponent } from './bugtracker-workspace.component';
import {ReferentialDataService} from 'sqtm-core';
import {mockReferentialDataService} from '../../../../utils/testing-utils/mocks.service';
import {ActivatedRoute, convertToParamMap} from '@angular/router';

describe('BugtrackerWorkspaceComponent', () => {
  let component: BugtrackerWorkspaceComponent;
  let fixture: ComponentFixture<BugtrackerWorkspaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BugtrackerWorkspaceComponent],
      providers: [
        {provide: ReferentialDataService, useValue: mockReferentialDataService()},
        {provide: ActivatedRoute, useValue: {snapshot: {paramMap: convertToParamMap({bugtrackerId: '1'})}}},
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BugtrackerWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
