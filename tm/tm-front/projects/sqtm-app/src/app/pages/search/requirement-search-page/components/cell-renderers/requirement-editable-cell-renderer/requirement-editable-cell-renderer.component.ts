import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  GridService,
  InlineEditableTextFieldComponent,
  RestService,
  TableValueChange
} from 'sqtm-core';
import {catchError, finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-requirement-editable-cell-renderer',
  templateUrl: './requirement-editable-cell-renderer.component.html',
  styleUrls: ['./requirement-editable-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementEditableCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  @ViewChild('editableTextField')
  editableTextField: InlineEditableTextFieldComponent;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public restService: RestService,
              public errorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  canEdit() {
    const editable = this.columnDisplay.editable;
    if (typeof editable === 'function') {
      return editable(this.columnDisplay, this.row);
    } else {
      return editable;
    }
  }

  updateValue(value: string) {
    this.editableTextField.beginAsync();
    this.grid.updateCellValue(this.row, this.columnDisplay, value).pipe(
      catchError(err => this.errorDisplayService.handleActionError(err)),
      finalize(() => this.editableTextField.endAsync())
    ).subscribe(() => {
      const tableValueChange: TableValueChange = {columnId: this.columnDisplay.id, value};
      this.grid.editRows([this.row.id], [tableValueChange]);
    });
  }
}

export function requirementSearchEditableText(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(RequirementEditableCellRendererComponent);
}
