export class TestCaseMultiEditConfiguration {
  id: string;
  titleKey: string;
  projectIds: number[];
  testCaseIds: number[];
  writingRightOnLine: boolean;
}
