import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-app-test-case-by-req-for-campaign-search-page',
  template: `
    <sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page [workspaceName]="'requirement-workspace'"
                                                                           [titleKey]="'sqtm-core.search.requirement-coverage.title'"
                                                                           [containerType]="'campaignId'"
                                                                           [url]="'campaign'">
    </sqtm-app-test-case-by-req-for-campaign-workspace-entities-search-page>
  `,
  styleUrls: ['./test-case-by-req-for-campaign-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseByReqForCampaignSearchPageComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
