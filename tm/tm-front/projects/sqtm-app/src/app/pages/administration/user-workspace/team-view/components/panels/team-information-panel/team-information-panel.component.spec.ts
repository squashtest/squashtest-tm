import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TeamInformationPanelComponent} from './team-information-panel.component';
import {DatePipe} from '@angular/common';
import {DialogService, GridService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TeamViewService} from '../../../services/team-view.service';
import {EMPTY} from 'rxjs';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('TeamInformationPanelComponent', () => {
  let component: TeamInformationPanelComponent;
  let fixture: ComponentFixture<TeamInformationPanelComponent>;

  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: DatePipe,
          useValue: jasmine.createSpyObj(['transform']),
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TeamViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: GridService,
          useClass: GridService,
        },
      ],
      declarations: [TeamInformationPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
