import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ADMIN_WS_MILESTONE_TABLE, ADMIN_WS_MILESTONE_TABLE_CONFIG} from '../../../milestone-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GRID_PERSISTENCE_KEY,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  isAdminOrProjectManager,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {adminMilestonesTableDefinition} from '../milestone-workspace-grid/milestone-workspace-grid.component';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {filter, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-milestone-workspace',
  templateUrl: './milestone-workspace.component.html',
  styleUrls: ['./milestone-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: ADMIN_WS_MILESTONE_TABLE_CONFIG,
      useFactory: adminMilestonesTableDefinition,
      deps: []
    },
    {
      provide: ADMIN_WS_MILESTONE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_MILESTONE_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_MILESTONE_TABLE
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'milestone-workspace-main-grid'
    },
    GridWithStatePersistence
  ],
})
export class MilestoneWorkspaceComponent implements OnInit, OnDestroy  {

  authenticatedUser$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private router: Router,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.authenticatedUser$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => isAdminOrProjectManager(authUser))
    );

    this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => !isAdminOrProjectManager(authUser)),
    ).subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.gridService.complete();
  }

}
