import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProjectBoundToMilestoneCellComponent } from './project-bound-to-milestone-cell.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';
import {grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MilestoneViewService} from '../../../services/milestone-view.service';

describe('ProjectBoundToMilestoneCellComponent', () => {
  let component: ProjectBoundToMilestoneCellComponent;
  let fixture: ComponentFixture<ProjectBoundToMilestoneCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectBoundToMilestoneCellComponent ],
      imports: [AppTestingUtilsModule, OverlayModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: MilestoneViewService,
          useValue: jasmine.createSpyObj(['load', 'complete'])
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectBoundToMilestoneCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
