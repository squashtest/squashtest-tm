import {DisplayOption} from 'sqtm-core';

export interface ScmServerDialogConfiguration {
  titleKey: string;
  scmServerKinds: DisplayOption[];
}
