import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DataRow,
  DialogService,
  DisplayOption,
  Extendable, FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  selectableTextColumn,
  textColumn,
  WorkspaceWithGridComponent,
  basicExternalLinkColumn
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {deleteScmServerColumn} from '../../components/cell-renderers/delete-scm-server/delete-scm-server.component';
import {ScmServerCreationDialogComponent} from '../../components/dialogs/scm-server-creation-dialog/scm-server-creation-dialog.component';

export function adminScmServersTableDefinition(): GridDefinition {
  return grid('scm-servers')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(120, 0.2)),
      textColumn('kind')
        .withI18nKey('sqtm-core.entity.scm-server.kind.label')
        .changeWidthCalculationStrategy(new Extendable(120, 0.2)),
      basicExternalLinkColumn('url')
        .withI18nKey('sqtm-core.entity.generic.url.label')
        .changeWidthCalculationStrategy(new Extendable(300, 0.1)),
      deleteScmServerColumn('delete')
    ]).server().withServerUrl(['scm-servers'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['name'])
    .build();
}

@Component({
  selector: 'sqtm-app-scm-server-grid',
  templateUrl: './scm-server-grid.component.html',
  styleUrls: ['./scm-server-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScmServerGridComponent implements AfterViewInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  scmServerKinds: DisplayOption[];

  protected readonly entityIdPositionInUrl = 3;

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              public workspaceWithGrid: WorkspaceWithGridComponent) {
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;

    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.getScmServerKinds();
    this.addFilters();
    this.gridService.refreshData();
  }

  private addFilters() {
    this.gridService.addFilters([{
      id: 'name',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openScmServerDialog() {
    if (this.scmServerKinds.length === 0) {
      this.showNoneScmPluginDetectedAlert();
    } else {

      const dialogReference = this.dialogService.openDialog({
        component: ScmServerCreationDialogComponent,
        viewContainerReference: this.viewContainerRef,
        data: {
          titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.new-scm-server',
          scmServerKinds: this.scmServerKinds
        },
        id: 'scm-server-dialog',
        width: 600
      });

      dialogReference.dialogResultChanged$.pipe(
        takeUntil(dialogReference.dialogClosed$),
        filter(result => result != null)
      ).subscribe(() => {
        this.gridService.refreshData();
      });
    }
  }

  private showNoneScmPluginDetectedAlert() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.entity.generic.information.label',
      messageKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.none-scm-plugin-detected',
      level: 'INFO'
    });
  }

  getScmServerKinds() {
    this.restService.get<{ scmServerKinds: string[] }>(['scm-servers/get-scm-server-kinds'])
      .subscribe(response => {
          this.scmServerKinds = response.scmServerKinds.map(scmServerKind => {
            return {id: scmServerKind, label: scmServerKind};
          });
        }
      );
  }

  deleteScmServers($event: MouseEvent) {
    $event.stopPropagation();

    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      concatMap((rows: DataRow[]) => this.showConfirmDeleteScmServerDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({rows}) => this.deleteScmServersServerSide(rows)),
      tap(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteScmServerDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.delete-many',
      messageKey: this.isUsedByProject(rows) ?
        'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-many-with-project' :
        'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-many-without-project',
      level: 'DANGER'
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  private deleteScmServersServerSide(rows): Observable<void> {
    const pathVariable = rows.map(row => row.data['serverId']).join(',');
    return this.restService.delete([`scm-servers`, pathVariable]);
  }


  private isUsedByProject(rows: DataRow[]) {
    return rows.map(row => row.data['projectCount'] > 0).includes(true);
  }

  filterCodeSourceServer($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}
