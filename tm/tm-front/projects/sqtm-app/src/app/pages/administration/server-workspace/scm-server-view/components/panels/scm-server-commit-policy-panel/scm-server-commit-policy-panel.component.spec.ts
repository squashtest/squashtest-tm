import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerCommitPolicyPanelComponent } from './scm-server-commit-policy-panel.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ScmServerCommitPolicyPanelComponent', () => {
  let component: ScmServerCommitPolicyPanelComponent;
  let fixture: ComponentFixture<ScmServerCommitPolicyPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ScmServerCommitPolicyPanelComponent ],
      imports: [AppTestingUtilsModule, FormsModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerCommitPolicyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
