import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {MainCustomWorkspaceComponent} from './main-custom-workspace.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('MainCustomWorkspaceComponent', () => {
  let component: MainCustomWorkspaceComponent;
  let fixture: ComponentFixture<MainCustomWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MainCustomWorkspaceComponent],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCustomWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
