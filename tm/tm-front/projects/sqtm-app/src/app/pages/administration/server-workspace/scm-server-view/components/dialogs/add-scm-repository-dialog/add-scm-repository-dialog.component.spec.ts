import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddScmRepositoryDialogComponent } from './add-scm-repository-dialog.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {
  DialogReference,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import createSpyObj = jasmine.createSpyObj;
import {ScmServerViewService} from '../../../services/scm-server-view.service';
import {OverlayModule} from '@angular/cdk/overlay';

describe('AddScmRepositoryDialogComponent', () => {
  let component: AddScmRepositoryDialogComponent;
  let fixture: ComponentFixture<AddScmRepositoryDialogComponent>;

  const dialogReference = createSpyObj(['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddScmRepositoryDialogComponent ],
      imports: [AppTestingUtilsModule, ReactiveFormsModule, OverlayModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        },
        {
          provide: RestService,
          useValue: jasmine.createSpyObj(['get', 'post'])
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant'])
        },
        {
          provide: GridDefinition,
          useValue: grid('grid-test').build(),
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: ScmServerViewService,
          useValue: {},
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddScmRepositoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
