import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProjectViewWithGridComponent } from './project-view-with-grid.component';
import {mockGridService} from '../../../../../../utils/testing-utils/mocks.service';
import {GridService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ProjectViewWithGridComponent', () => {
  let component: ProjectViewWithGridComponent;
  let fixture: ComponentFixture<ProjectViewWithGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectViewWithGridComponent ],
      providers: [
        {
          provide: GridService,
          useValue: mockGridService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectViewWithGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
