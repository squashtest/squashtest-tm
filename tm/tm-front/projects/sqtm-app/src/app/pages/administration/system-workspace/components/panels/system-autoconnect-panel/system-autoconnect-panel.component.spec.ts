import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemAutoconnectPanelComponent } from './system-autoconnect-panel.component';
import {SystemViewService} from '../../../services/system-view.service';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SystemAutoconnectPanelComponent', () => {
  let component: SystemAutoconnectPanelComponent;
  let fixture: ComponentFixture<SystemAutoconnectPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemAutoconnectPanelComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: {
            componentData$: of({autoconnectOnConnection: false}),
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemAutoconnectPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
