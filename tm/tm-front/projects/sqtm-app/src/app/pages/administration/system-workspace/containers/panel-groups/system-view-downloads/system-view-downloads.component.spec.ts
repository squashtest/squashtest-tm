import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemViewDownloadsComponent } from './system-view-downloads.component';
import {EMPTY} from 'rxjs';
import {SystemViewService} from '../../../services/system-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SystemViewDownloadsComponent', () => {
  let component: SystemViewDownloadsComponent;
  let fixture: ComponentFixture<SystemViewDownloadsComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemViewDownloadsComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemViewDownloadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
