import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {BugTrackerContentComponent} from './bug-tracker-content.component';
import {GridService, RestService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {BugTrackerViewService} from '../../../services/bug-tracker-view.service';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('BugTrackerContentComponent', () => {
  let component: BugTrackerContentComponent;
  let fixture: ComponentFixture<BugTrackerContentComponent>;
  const restService = mockRestService();
  const viewService = jasmine.createSpyObj(['load']);
  viewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forChild(), HttpClientTestingModule],
      declarations: [BugTrackerContentComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
        {
          provide: BugTrackerViewService,
          useValue: viewService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(BugTrackerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});
