import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MilestoneOwnerCellRendererComponent} from './milestone-owner-cell-renderer.component';
import {grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService,} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('MilestoneOwnerCellRendererComponent', () => {
  let component: MilestoneOwnerCellRendererComponent;
  let fixture: ComponentFixture<MilestoneOwnerCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MilestoneOwnerCellRendererComponent ],
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneOwnerCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
