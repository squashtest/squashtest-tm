import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemWorkspaceComponent } from './system-workspace.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SystemViewService} from '../../services/system-view.service';
import {AdminReferentialDataService, RestService} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {mockAdminReferentialDataService, mockRestService} from '../../../../../utils/testing-utils/mocks.service';

describe('SystemWorkspaceComponent', () => {
  let component: SystemWorkspaceComponent;
  let fixture: ComponentFixture<SystemWorkspaceComponent>;

  const adminReferentialDataService = mockAdminReferentialDataService();

  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemWorkspaceComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: jasmine.createSpyObj(['load']),
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemWorkspaceComponent);
    component = fixture.componentInstance;
    component.authenticatedUser$ = EMPTY;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
