import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {AddInfoListItemDialogComponent} from './add-info-list-item-dialog.component';
import {DialogReference, grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {InfoListViewService} from '../../../services/info-list-view.service';

describe('AddInfoListItemDialogComponent', () => {
  let component: AddInfoListItemDialogComponent;
  let fixture: ComponentFixture<AddInfoListItemDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule],
      declarations: [AddInfoListItemDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close']),
        },
        {
          provide: RestService,
          useValue: jasmine.createSpyObj(['get', 'post'])
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant'])
        },
        {
          provide: GridDefinition,
          useValue: grid('grid-test').build(),
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: InfoListViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInfoListItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
