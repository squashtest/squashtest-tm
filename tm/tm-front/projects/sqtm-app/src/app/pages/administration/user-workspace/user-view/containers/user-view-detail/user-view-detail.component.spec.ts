import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {UserViewDetailComponent} from './user-view-detail.component';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';
import {GenericEntityViewService, RestService} from 'sqtm-core';
import {mockRestService} from '../../../../../../utils/testing-utils/mocks.service';
import {TranslateModule} from '@ngx-translate/core';
import {UserViewService} from '../../services/user-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('UserViewDetailComponent', () => {
  let component: UserViewDetailComponent;
  let fixture: ComponentFixture<UserViewDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [UserViewDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({
              userId: '123',
            })),
          }
        },
        {
          provide: GenericEntityViewService,
          useValue: {},
        },
        {
          provide: UserViewService,
          useValue: {},
        },
        {
          provide: RestService,
          useValue: mockRestService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
