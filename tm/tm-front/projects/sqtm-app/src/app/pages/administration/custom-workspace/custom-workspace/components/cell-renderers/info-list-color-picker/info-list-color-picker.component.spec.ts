import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoListColorPickerComponent } from './info-list-color-picker.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {InfoListOptionService} from '../../../services/info-list-option.service';

describe('InfoListOptionColorPickerCellComponent', () => {
  let component: InfoListColorPickerComponent;
  let fixture: ComponentFixture<InfoListColorPickerComponent>;
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoListColorPickerComponent ],
      imports: [AppTestingUtilsModule,  HttpClientTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: InfoListOptionService,
          useValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
