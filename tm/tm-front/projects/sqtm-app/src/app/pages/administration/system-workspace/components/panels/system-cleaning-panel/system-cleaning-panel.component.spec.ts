import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemCleaningPanelComponent } from './system-cleaning-panel.component';
import {Overlay} from '@angular/cdk/overlay';
import {SystemViewService} from '../../../services/system-view.service';
import {EMPTY} from 'rxjs';

describe('SystemCleaningPanelComponent', () => {
  let component: SystemCleaningPanelComponent;
  let fixture: ComponentFixture<SystemCleaningPanelComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemCleaningPanelComponent ],
      providers : [
        {provide: Overlay, useClass: Overlay},
        {provide: SystemViewService, useValue: systemViewService}
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemCleaningPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
