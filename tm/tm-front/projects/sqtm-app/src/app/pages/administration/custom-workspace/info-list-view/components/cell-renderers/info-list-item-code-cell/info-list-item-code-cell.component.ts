import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  GridService
} from 'sqtm-core';
import {InfoListViewService} from '../../../services/info-list-view.service';
import {catchError, finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-info-list-item-code-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <sqtm-core-editable-text-field #editableTextField style="margin: auto 5px;"
                                       class="sqtm-grid-cell-txt-renderer"
                                       [showPlaceHolder]="false"
                                       [value]="row.data[columnDisplay.id]" [layout]="'no-buttons'"
                                       [size]="'small'"
                                       (confirmEvent)="updateValue($event)"
        ></sqtm-core-editable-text-field>
      </div>
    </ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoListItemCodeCellComponent extends AbstractCellRendererComponent {

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private dialogService: DialogService,
              private infoListViewService: InfoListViewService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef);
  }

  updateValue(newCode: string) {
    this.infoListViewService.changeItemCode(this.row.data['id'], newCode).pipe(
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.editableTextField.endAsync())
    ).subscribe();
  }
}

export function infoListItemCodeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(InfoListItemCodeCellComponent);
}
