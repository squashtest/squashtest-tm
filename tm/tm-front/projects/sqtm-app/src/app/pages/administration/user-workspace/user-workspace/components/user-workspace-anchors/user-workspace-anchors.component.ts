import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {WorkspaceWithGridComponent} from 'sqtm-core';


@Component({
  selector: 'sqtm-app-user-workspace-anchors',
  templateUrl: './user-workspace-anchors.component.html',
  styleUrls: ['./user-workspace-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserWorkspaceAnchorsComponent implements OnInit {

  constructor(private workspaceWithGrid: WorkspaceWithGridComponent) {
  }

  ngOnInit(): void {
  }

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }
}
