import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserViewComponent } from './user-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {DialogService, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {UserViewService} from '../../services/user-view.service';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';

describe('UserViewComponent', () => {
  let component: UserViewComponent;
  let fixture: ComponentFixture<UserViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule, NzDropDownModule],
      declarations: [ UserViewComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj(['openDeletionConfirm', 'openAlert'])
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
        {
          provide: UserViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
