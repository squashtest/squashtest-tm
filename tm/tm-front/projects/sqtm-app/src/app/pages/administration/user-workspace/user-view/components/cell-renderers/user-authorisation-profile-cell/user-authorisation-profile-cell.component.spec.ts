import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserAuthorisationProfileCellComponent } from './user-authorisation-profile-cell.component';
import {
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {UserViewService} from '../../../services/user-view.service';
import {EMPTY} from 'rxjs';

describe('UserAuhtorisationProfileCellComponent', () => {
  let component: UserAuthorisationProfileCellComponent;
  let fixture: ComponentFixture<UserAuthorisationProfileCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAuthorisationProfileCellComponent ],
      imports: [AppTestingUtilsModule, OverlayModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: UserViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAuthorisationProfileCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
