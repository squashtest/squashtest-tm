import {TestBed} from '@angular/core/testing';

import {BugTrackerViewService} from './bug-tracker-view.service';
import {
  AdminBugTrackerState,
  AttachmentService,
  AuthenticationPolicy,
  AuthenticationProtocol,
  BasicAuthCredentials,
  EntityViewAttachmentHelperService,
  isBasicAuthCredentials,
  isOAuth1aCredentials,
  OAuth1aConfiguration,
  OAuthCredentials,
  RestService,
  SignatureMethod
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {take, withLatestFrom} from 'rxjs/operators';
import {of} from 'rxjs';
import {AdminBugTrackerViewState} from '../states/admin-bug-tracker-view-state';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('BugTrackerViewService', () => {
  let service: BugTrackerViewService;
  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: BugTrackerViewService,
          useClass: BugTrackerViewService,
          deps: [
            RestService,
            AttachmentService,
            TranslateService,
            EntityViewAttachmentHelperService,
          ]
        },
      ]
    });
    service = TestBed.inject(BugTrackerViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load a bugtracker', async (done) => {
    const model = getInitialBugTracker();
    restService.getWithoutErrorHandling.and.returnValue(of(model));
    service.load(1);

    service.componentData$.pipe(
      take(1)
    ).subscribe((componentData) => {
      expect(componentData.bugTracker).toEqual(model);
      done();
    });
  });

  it('should change auth policy', async (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1);

    service.setAuthPolicy(AuthenticationPolicy.APP_LEVEL).pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
      expect(componentData.bugTracker.authPolicy).toEqual(AuthenticationPolicy.APP_LEVEL);
      done();
    });
  });

  it('should change auth configuration', async (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1);

    const conf = makeOAuthConfiguration();
    service.setAuthenticationConfiguration(conf).pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
      expect(componentData.bugTracker.authConfiguration).toEqual(conf);
      done();
    });
  });

  it('should change basic auth credentials', async (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1);

    service.setBasicAuthCredentials('login', 'password').pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
      expect(componentData.bugTracker.credentials).toEqual({
        type: AuthenticationProtocol.BASIC_AUTH,
        implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
        password: 'password',
        username: 'login',
        registered: true
      });
      done();
    });
  });

  it('should change OAuth1a credentials', async (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1);

    service.setOAuthCredentials('token', 'secret').pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
      expect(componentData.bugTracker.credentials).toEqual({
        type: AuthenticationProtocol.OAUTH_1A,
        implementedProtocol: AuthenticationProtocol.OAUTH_1A,
        tokenSecret: 'secret',
        token: 'token',
        registered: true
      });
      done();
    });
  });

  it('should discriminate OAuth1a credentials', () => {
    const validOAuthCredentials: OAuthCredentials = {
      type: AuthenticationProtocol.OAUTH_1A,
      implementedProtocol: AuthenticationProtocol.OAUTH_1A,
      tokenSecret: 'secret',
      token: 'token',
    };

    expect(isOAuth1aCredentials(validOAuthCredentials)).toBeTruthy();
    expect(isOAuth1aCredentials(null)).toBeFalsy();
    expect(isOAuth1aCredentials({type: 'titi'})).toBeFalsy();
  });

  it('should discriminate basic auth credentials', () => {
    const validBasicAuthCredentials: BasicAuthCredentials = {
      type: AuthenticationProtocol.BASIC_AUTH,
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    };

    expect(isBasicAuthCredentials(validBasicAuthCredentials)).toBeTruthy();
    expect(isBasicAuthCredentials(null)).toBeFalsy();
    expect(isBasicAuthCredentials({type: 'titi'})).toBeFalsy();
  });

  it('should change credentials based on deduced type (basic)', async (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1);

    const credentials: BasicAuthCredentials = {
      type: AuthenticationProtocol.BASIC_AUTH,
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
      registered : true,
    };

    service.setCredentials(credentials).pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
      expect(componentData.bugTracker.credentials).toEqual(credentials);
      done();
    });
  });

  it('should change credentials based on deduced type (OAuth)', async (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialBugTracker()));
    service.load(1);

    const credentials: OAuthCredentials = {
      type: AuthenticationProtocol.OAUTH_1A,
      implementedProtocol: AuthenticationProtocol.OAUTH_1A,
      tokenSecret: 'secret',
      token: 'token',
      registered : true,
    };

    service.setCredentials(credentials).pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminBugTrackerViewState]) => {
      expect(componentData.bugTracker.credentials).toEqual(credentials);
      done();
    });
  });
});

function makeOAuthConfiguration(): OAuth1aConfiguration {
  return {
    consumerKey: '',
    implementedProtocol: 'OAUTH_1A',
    signatureMethod: SignatureMethod.RSA_SHA1,
    clientSecret: '',
    userAuthorizationUrl: '',
    requestTokenUrl: '',
    requestTokenHttpMethod: 'GET',
    accessTokenUrl: '',
    accessTokenHttpMethod: 'GET',
    type: 'OAUTH_1A',
  };
}

function getInitialBugTracker(): AdminBugTrackerState {
  return {
    id: 1,
    kind: 'bugTracker',
    name: 'name',
    url: 'http://url.com',
    authPolicy: AuthenticationPolicy.USER,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    iframeFriendly: false,
    bugTrackerKinds: [],
    authConfiguration: null,
    credentials: null,
    supportedAuthenticationProtocols: [],
    attachmentList: {id: null, attachments: null},
  };
}
