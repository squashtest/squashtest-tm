import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectPermissionsProfileCellComponent} from './project-permissions-profile-cell.component';
import {grid, GridDefinition, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('ProjectPermissionsProfileCellComponent', () => {
  let component: ProjectPermissionsProfileCellComponent;
  let fixture: ComponentFixture<ProjectPermissionsProfileCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectPermissionsProfileCellComponent],
      imports: [AppTestingUtilsModule, OverlayModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: ProjectViewService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPermissionsProfileCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
