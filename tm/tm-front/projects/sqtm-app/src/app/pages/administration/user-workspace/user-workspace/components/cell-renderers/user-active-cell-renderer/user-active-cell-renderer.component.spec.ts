import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserActiveCellRendererComponent } from './user-active-cell-renderer.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockDialogService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('UserActiveCellRendererComponent', () => {
  let component: UserActiveCellRendererComponent;
  let fixture: ComponentFixture<UserActiveCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};
  const dialogService = mockDialogService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActiveCellRendererComponent ],
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActiveCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
