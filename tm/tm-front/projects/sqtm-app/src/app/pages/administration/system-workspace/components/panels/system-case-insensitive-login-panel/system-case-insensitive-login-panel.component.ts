import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {SystemViewState} from '../../../states/system-view.state';
import {SystemViewService} from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-system-case-insensitive-login-panel',
  templateUrl: './system-case-insensitive-login-panel.component.html',
  styleUrls: [
    './system-case-insensitive-login-panel.component.less',
    '../../../styles/system-workspace.common.less'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemCaseInsensitiveLoginPanelComponent implements OnInit {

  @Input()
  componentData: SystemViewState;

  constructor(private systemViewService: SystemViewService) { }

  ngOnInit(): void {
  }

  changeCaseInsensitiveLoginEnabled() {
    const isActive = this.componentData.caseInsensitiveLogin;
    const newStatus = !isActive;
    this.systemViewService.changeCaseInsensitiveLoginEnabled(newStatus);
  }

  checkIdDuplicateLogins() {
    return this.componentData.duplicateLogins.length > 0;
  }
}
