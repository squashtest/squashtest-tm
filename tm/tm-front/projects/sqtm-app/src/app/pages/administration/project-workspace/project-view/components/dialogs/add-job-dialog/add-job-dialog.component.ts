import {ChangeDetectionStrategy, ChangeDetectorRef, Component, InjectionToken, OnDestroy, OnInit} from '@angular/core';
import {
  DataRow,
  DialogReference,
  Extendable,
  Fixed,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  selectRowColumn,
  smallGrid,
  StyleDefinitionBuilder,
  textColumn,
  ToggleSelectionHeaderRendererComponent
} from 'sqtm-core';
import {ProjectViewService} from '../../../services/project-view.service';
import {filter, take} from 'rxjs/operators';
import {addJobNameColumn} from '../../cell-renderers/add-job-label-cell/add-job-label-cell.component';
import {addJobBddColumn} from '../../cell-renderers/add-job-bdd-cell/add-job-bdd-cell.component';
import {AddJobDialogDataState, AddJobDialogService} from '../../../services/add-job-dialog.service';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';

export const PROJECT_ADD_JOB_TABLE_CONF = new InjectionToken('PROJECT_ADD_JOB_TABLE_CONF');
export const PROJECT_ADD_JOB_TABLE = new InjectionToken('PROJECT_ADD_JOB_TABLE');

export function projectAddJobsTableDefinition(): GridDefinition {
  return smallGrid('project-add-jobs')
    .withColumns([
      selectRowColumn()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withHeaderRenderer(ToggleSelectionHeaderRendererComponent),
      textColumn('remoteName')
        .withI18nKey('sqtm-core.administration-workspace.views.project.automation.jobs.remote-name')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      addJobNameColumn('label')
        .withI18nKey('sqtm-core.administration-workspace.views.project.automation.jobs.tm-label')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      addJobBddColumn('canRunBdd'),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-add-job-dialog',
  templateUrl: './add-job-dialog.component.html',
  styleUrls: ['./add-job-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROJECT_ADD_JOB_TABLE_CONF,
      useFactory: projectAddJobsTableDefinition
    },
    {
      provide: PROJECT_ADD_JOB_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_ADD_JOB_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: PROJECT_ADD_JOB_TABLE
    }
  ]
})
export class AddJobDialogComponent implements OnInit, OnDestroy {
  errors: string[] = [];

  dialogDataState$: Observable<AddJobDialogDataState>;

  constructor(private gridService: GridService,
              private restService: RestService,
              private projectViewService: ProjectViewService,
              private addJobDialogService: AddJobDialogService,
              private dialogRef: DialogReference,
              private translateService: TranslateService,
              private cdref: ChangeDetectorRef) {
    this.dialogDataState$ = this.addJobDialogService.dataState$;

    this.dialogRef.dialogClosed$.pipe(take(1)).subscribe(() => {
      this.addJobDialogService.unload();
    });
  }

  ngOnInit(): void {
    this.initializeTable();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  confirm() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => rows.length > 0),
      filter((rows) => this.checkValidity(rows))
    ).subscribe((rows) => {
      this.doAddProjects(rows);
    });
  }

  private doAddProjects(rows: DataRow[]): void {
    const projectsToAdd = rows.map(row => ({
      remoteName: row.data.remoteName,
      label: row.data.label,
      canRunBdd: row.data.canRunBdd,
    }));

    this.projectViewService.addTestAutomationProjects(projectsToAdd).subscribe(
      () => this.handleSuccess(),
      (error) => this.handleServerSideError(error));
  }

  getCloseButtonLabel(dataState: AddJobDialogDataState): string {
    const key = dataState === 'ready' ? 'sqtm-core.generic.label.cancel' : 'sqtm-core.generic.label.close';
    return this.translateService.instant(key);
  }

  private initializeTable() {
    this.projectViewService.componentData$.pipe(
      filter(componentData => Boolean(componentData.project?.id)),
      take(1),
    ).subscribe((componentData) => {
      this.addJobDialogService.load(componentData.project.id);
      this.gridService.connectToDatasource(this.addJobDialogService.taProjects$, 'remoteName');
    });
  }

  private checkValidity(selectedRows: DataRow[]): boolean {
    this.errors = [];

    for (const row of selectedRows) {
      const label = row.data?.label?.trim();

      if (label == null || label === '') {
        this.errors.push(this.makeErrorMessage('label', 'sqtm-core.validation.errors.required'));
        return false;
      }
    }

    if (hasDuplicates(selectedRows.map(row => row.data?.label?.trim()))) {
      this.errors.push(this.makeErrorMessage('label', 'sqtm-core.exception.duplicate.tmlabel'));
      return false;
    }

    return true;
  }

  private handleServerSideError(error: any) {
    const squashError = error?.error?.squashTMError;
    if (error.status === 412 && squashError?.kind === 'FIELD_VALIDATION_ERROR') {
      const messages = squashError.fieldValidationErrors.map(fve => this.makeErrorMessage(fve.fieldName, fve.i18nKey));
      this.errors.push(...messages);
      this.cdref.markForCheck();
    } else {
      console.error(error);
    }
  }

  private makeErrorMessage(field: string, i18nKey: string) {
    // For now this dialog only shows errors on label field...
    const labelKey = 'sqtm-core.administration-workspace.views.project.automation.jobs.tm-label';
    const fieldName = field === 'label' ? this.translateService.instant(labelKey) : field;
    return `${fieldName}: ${this.translateService.instant(i18nKey)}`;
  }

  private handleSuccess() {
    this.dialogRef.result = true;
    this.dialogRef.close();
  }
}

function hasDuplicates(array: any[]) {
  const alreadyThere = {};

  for (const item of array) {
    if (alreadyThere[item]) {
      return true;
    }

    alreadyThere[item] = true;
  }

  return false;
}
