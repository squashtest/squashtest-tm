import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerCreationDialogComponent } from './scm-server-creation-dialog.component';
import {DialogReference} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import createSpyObj = jasmine.createSpyObj;
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ScmServerCreationDialogComponent', () => {
  let component: ScmServerCreationDialogComponent;
  let fixture: ComponentFixture<ScmServerCreationDialogComponent>;

  const dialogReference = createSpyObj(['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }],
      declarations: [ ScmServerCreationDialogComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerCreationDialogComponent);
    component = fixture.componentInstance;
    component.data = {titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.new-scm-server',
      scmServerKinds: [{id: 'test' , label: 'test'}]};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
