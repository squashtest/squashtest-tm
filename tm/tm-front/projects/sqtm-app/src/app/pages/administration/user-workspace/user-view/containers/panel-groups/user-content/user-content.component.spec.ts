import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {UserContentComponent} from './user-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogModule} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {UserViewService} from '../../../services/user-view.service';
import {EMPTY} from 'rxjs';

describe('UserContentComponent', () => {
  let component: UserContentComponent;
  let fixture: ComponentFixture<UserContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, DialogModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: UserViewService,
          useValue: {
            componentData$: EMPTY
          }
        },
      ],
      declarations: [UserContentComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
