import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DeactivatePluginDialogComponent} from './deactivate-plugin-dialog.component';
import {DialogReference} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DeactivatePluginDialogComponent', () => {
  let component: DeactivatePluginDialogComponent;
  let fixture: ComponentFixture<DeactivatePluginDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [DeactivatePluginDialogComponent],
      providers: [
        {
          provide: DialogReference, useValue: {
            data: {}
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeactivatePluginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
