import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MilestoneDuplicationDialogComponent } from './milestone-duplication-dialog.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {DialogReference} from 'sqtm-core';
import createSpyObj = jasmine.createSpyObj;
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('MilestoneDuplicationDialogComponent', () => {
  let component: MilestoneDuplicationDialogComponent;
  let fixture: ComponentFixture<MilestoneDuplicationDialogComponent>;

  const dialogReference = createSpyObj(['close']);
  dialogReference.data = {milestoneId: -1};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MilestoneDuplicationDialogComponent ],
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneDuplicationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
