import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {SystemStatisticsPanelComponent} from './system-statistics-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {SystemViewState} from '../../../states/system-view.state';

describe('SystemStatisticsPanelComponent', () => {
  let component: SystemStatisticsPanelComponent;
  let fixture: ComponentFixture<SystemStatisticsPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule],
      declarations: [SystemStatisticsPanelComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemStatisticsPanelComponent);
    component = fixture.componentInstance;
    component.componentData = {
      statistics: {
        databaseSize: 0, executionsNumber: 0, iterationsNumber: 0, campaignsNumber: 0,
        testCasesNumber: 0, requirementsNumber: 0, usersNumber: 0, projectsNumber: 0,
        campaignIndexingDate: null, requirementIndexingDate: null, testcaseIndexingDate: null,
      },
      appVersion: 'SquashTM version',
      plugins: [],
    } as SystemViewState;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
