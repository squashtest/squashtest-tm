import {Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {
  DataRow,
  DialogService,
  GenericEntityViewComponentData,
  GenericEntityViewService,
  GridService,
  RestService
} from 'sqtm-core';
import {ScmServerViewService} from '../../services/scm-server-view.service';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {concatMap, filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {AdminScmServerViewState} from '../../states/admin-scm-server-view-state';
import {AdminScmServerState} from '../../states/admin-scm-server-state';

@Component({
  selector: 'sqtm-app-scm-server-view',
  templateUrl: './scm-server-view.component.html',
  styleUrls: ['./scm-server-view.component.less'],
  providers: [
    {
      provide: ScmServerViewService,
      useClass: ScmServerViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: ScmServerViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerViewComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminScmServerViewState>;

  private readonly unsub$ = new Subject<void>();

  constructor(private readonly route: ActivatedRoute,
              public readonly scmServerViewService: ScmServerViewService,
              private readonly gridService: GridService,
              private cdRef: ChangeDetectorRef,
              private restService: RestService,
              private dialogService: DialogService) {
    this.componentData$ = scmServerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('scmServerId')),
      ).subscribe((id) => {
      this.scmServerViewService.load(parseInt(id, 10));
    });

    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.scmServerViewService.complete();
  }

  handleDelete() {
    this.componentData$.pipe(
      take(1),
      withLatestFrom(this.gridService.selectedRows$),
      concatMap(([componentData, rows]: [AdminScmServerViewComponentData, DataRow[]]) =>
        this.showConfirmDeleteScmServerDialog(componentData.scmServer.id, rows)),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({scmServerId}) => this.deleteScmServersServerSide(scmServerId)),
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteScmServerDialog(scmServerId, rows: DataRow[])
    : Observable<{ confirmDelete: boolean, scmServerId: string }> {
    const serverIsUsedByProjects = rows.filter(row => row.data['projectCount'] > 0).length > 0;

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.delete-one',
      messageKey: serverIsUsedByProjects ?
        'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-one-with-project' :
        'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-one-without-project',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, scmServerId}))
    );
  }

  private deleteScmServersServerSide(scmServerId): Observable<void> {
    return this.restService.delete([`scm-servers`, scmServerId]);
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.scmServerViewService.simpleAttributeRequiringRefresh = [
      'name', 'url'
    ];

    this.scmServerViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() =>
      this.gridService.refreshDataAndKeepSelectedRows());
  }
}

export interface AdminScmServerViewComponentData extends GenericEntityViewComponentData<AdminScmServerState, 'scmServer'> {
}
