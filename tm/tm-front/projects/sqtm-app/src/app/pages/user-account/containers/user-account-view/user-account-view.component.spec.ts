import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserAccountViewComponent } from './user-account-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {UserAccountService} from '../../services/user-account.service';
import {EMPTY} from 'rxjs';

describe('UserAccountViewComponent', () => {
  let component: UserAccountViewComponent;
  let fixture: ComponentFixture<UserAccountViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAccountViewComponent ],
      providers: [
        {
          provide: UserAccountService,
          useValue: { componentData$: EMPTY },
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccountViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
