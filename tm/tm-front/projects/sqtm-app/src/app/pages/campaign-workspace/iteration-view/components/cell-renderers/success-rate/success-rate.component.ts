import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, GridService} from 'sqtm-core';


@Component({
  selector: 'sqtm-app-success-rate-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <span style="margin: auto 5px;" class="sqtm-grid-cell-txt-renderer"
              [ngClass]="textClass"
              [class.show-as-filtered-parent]="showAsFilteredParent"
              nz-tooltip [sqtmCoreLabelTooltip]="displayedText" [nzTooltipPlacement]="'topLeft'">
          {{displayedText}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./success-rate.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SuccessRateComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }

  get displayedText(): string {
    const value = this.row.data[this.columnDisplay.id];

    if (value == null) {
      return '0 %';
    }
    return `${Math.floor(value)} %`;
  }
}
