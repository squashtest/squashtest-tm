import {TestBed} from '@angular/core/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {IterationViewService} from './iteration-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {IterationTestPlanOperationHandler} from './iteration-test-plan-operation-handler';
import {mockGridService} from '../../../../utils/testing-utils/mocks.service';
import {of} from 'rxjs';
import {GridState} from 'sqtm-core';

describe('IterationTestPlanOperationHandler', () => {
  const iterationViewService = jasmine.createSpyObj(['changeItemsPosition']);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppTestingUtilsModule,
      HttpClientTestingModule,
      TranslateModule.forRoot()
    ],
    providers: [
      {
        provide: IterationViewService,
        useValue: iterationViewService,
      },
      IterationTestPlanOperationHandler
    ],
    schemas: [NO_ERRORS_SCHEMA],
  }));

  it('should be created', () => {
    const service: IterationTestPlanOperationHandler = TestBed.inject(IterationTestPlanOperationHandler);
    expect(service).toBeTruthy();
  });

  it('should handle internal reordering', () => {
    const service: IterationTestPlanOperationHandler = TestBed.inject(IterationTestPlanOperationHandler);
    service.grid = mockGridService();
    expect(service).toBeTruthy();
    service.grid.isSortedOrFiltered$ = of(false);

    iterationViewService.changeItemsPosition.and.returnValue(of({}));

    service.grid.gridState$ = of({
      dataRowState: {
        ids: [2, 5, 1, 3],
      },
      uiState: {
        dragState: {
          draggedRowIds: [3, 5],
          dragging: true,
          currentDndTarget: {
            id: 1,
            zone: 'bellow',
          }
        }
      }
    } as GridState);

    service.notifyInternalDrop();

    expect(iterationViewService.changeItemsPosition).toHaveBeenCalledWith([3, 5], 2);
  });
});
