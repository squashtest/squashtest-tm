import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionHistoryComponent, itvItpeHistoryTableDefinition} from './execution-history.component';
import {
  DialogReference,
  DialogService,
  GridService,
  gridServiceFactory,
  GridTestingModule,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {DatePipe} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ITV_ITPE_HISTORY_TABLE, ITV_ITPE_HISTORY_TABLE_CONF} from '../../iteration-view.constant';
import {RouterTestingModule} from '@angular/router/testing';
import {GENERIC_TEST_PLAN_VIEW_SERVICE} from '../../../generic-test-plan-view-service';

describe('ExecutionHistoryComponent', () => {
  let component: ExecutionHistoryComponent;
  let fixture: ComponentFixture<ExecutionHistoryComponent>;

  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  const dialogReference = {data: {}};
  const genericTestPlanViewService = jasmine.createSpyObj('genericTestPlanViewService', ['getEntityReference']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        GridTestingModule,
        AppTestingUtilsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot()],
      declarations: [ExecutionHistoryComponent],
      providers: [
        {
          provide: ITV_ITPE_HISTORY_TABLE_CONF,
          useFactory: itvItpeHistoryTableDefinition
        },
        {
          provide: ITV_ITPE_HISTORY_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, ITV_ITPE_HISTORY_TABLE_CONF, ReferentialDataService]
        },
        {
          provide: GridService,
          useExisting: ITV_ITPE_HISTORY_TABLE
        },
        {provide: DialogService, useValue: dialogService},
        {
          provide: DialogReference,
          useValue: dialogReference
        },
        {
          provide: GENERIC_TEST_PLAN_VIEW_SERVICE,
          useValue: genericTestPlanViewService,
        },
        DatePipe
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
