import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CampaignWorkspaceTreeComponent} from './campaign-workspace-tree.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {
  DialogService,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CAMPAIGN_WS_TREE, CAMPAIGN_WS_TREE_CONFIG} from '../../../campaign-workspace.constant';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {campaignTreeConfigFactory} from '../campaign-workspace/campaign-workspace.component';

describe('CampaignWorkspaceTreeComponent', () => {
  let component: CampaignWorkspaceTreeComponent;
  let fixture: ComponentFixture<CampaignWorkspaceTreeComponent>;

  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, NzDropDownModule, RouterTestingModule],
      declarations: [CampaignWorkspaceTreeComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: CAMPAIGN_WS_TREE_CONFIG,
          useFactory: campaignTreeConfigFactory,
        },
        {
          provide: CAMPAIGN_WS_TREE,
          useFactory: gridServiceFactory,
          deps: [RestService, CAMPAIGN_WS_TREE_CONFIG, ReferentialDataService]
        },
        {
          provide: GridService,
          useExisting: CAMPAIGN_WS_TREE
        }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignWorkspaceTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
