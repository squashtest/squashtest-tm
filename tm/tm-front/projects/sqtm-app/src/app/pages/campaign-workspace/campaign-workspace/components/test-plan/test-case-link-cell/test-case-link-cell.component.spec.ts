import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TestCaseLinkCellComponent} from './test-case-link-cell.component';
import {GRID_PERSISTENCE_KEY, GridService, GridWithStatePersistence} from 'sqtm-core';
import {mockGridService, mockPassThroughTranslateService} from '../../../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';

describe('TestCaseLinkCellComponent', () => {
  let component: TestCaseLinkCellComponent;
  let fixture: ComponentFixture<TestCaseLinkCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestCaseLinkCellComponent],
      providers: [
        {provide: GridService, useValue: mockGridService()},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'execution-plan-grid'
        },
        GridWithStatePersistence
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseLinkCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
