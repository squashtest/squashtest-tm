import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignViewDashboardComponent} from './campaign-view-dashboard.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CampaignViewService} from '../../service/campaign-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {OverlayModule} from '@angular/cdk/overlay';

describe('CampaignViewDashboardComponent', () => {
  let component: CampaignViewDashboardComponent;
  let fixture: ComponentFixture<CampaignViewDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot(), OverlayModule],
      providers: [CampaignViewService],
      declarations: [CampaignViewDashboardComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
