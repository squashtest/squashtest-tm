import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { CampaignMilestoneViewContentComponent } from './campaign-milestone-view-content.component';
import {CampaignMilestoneViewService} from '../../services/campaign-milestone-view.service';
import {TranslateModule} from '@ngx-translate/core';

describe('CampaignMilestoneViewContentComponent', () => {
  let component: CampaignMilestoneViewContentComponent;
  let fixture: ComponentFixture<CampaignMilestoneViewContentComponent>;
  const serviceMock = jasmine.createSpyObj<CampaignMilestoneViewService>(['init']);

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [CampaignMilestoneViewContentComponent],
      providers: [
        {provide: CampaignMilestoneViewService, useValue: serviceMock}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignMilestoneViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
