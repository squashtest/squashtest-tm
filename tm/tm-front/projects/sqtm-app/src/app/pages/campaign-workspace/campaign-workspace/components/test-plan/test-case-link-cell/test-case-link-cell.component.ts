import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService, GridWithStatePersistence} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-test-case-link-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <span *ngIf="wasTestCaseDeleted; else link"
              class="text-overflow link-cell">
          {{textToDisplay}}
        </span>
        <ng-template #link>
          <a [routerLink]="getUrl()" class="text-overflow link-cell" nz-tooltip
             (click)="saveTestPlanGridState()"
             [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
             [nzTooltipPlacement]="'topLeft'"
          >
            {{textToDisplay}}
          </a>
        </ng-template>
      </div>
    </ng-container>`,
  styleUrls: ['./test-case-link-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseLinkCellComponent extends AbstractCellRendererComponent {


  constructor(grid: GridService,
              cdr: ChangeDetectorRef,
              private translateService: TranslateService,
              private gridWithStatePersistence: GridWithStatePersistence) {
    super(grid, cdr);
  }

  get textToDisplay(): string {
    return this.wasTestCaseDeleted ?
      this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.deleted-test-case')
      : this.row.data[this.columnDisplay.id];
  }

  get wasTestCaseDeleted(): boolean {
    return !Boolean(this.row.data['testCaseId']);
  }

  getUrl() {
    return '/test-case-workspace/test-case/detail/' + this.row.data['testCaseId'];
  }

  saveTestPlanGridState() {
    this.gridWithStatePersistence.saveSnapshot();
  }
}

/*
 * Warning! This cell renderer require a GridWithStatePersistence in the dep tree.
 */
export function withTestCaseLinkColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TestCaseLinkCellComponent);
}
