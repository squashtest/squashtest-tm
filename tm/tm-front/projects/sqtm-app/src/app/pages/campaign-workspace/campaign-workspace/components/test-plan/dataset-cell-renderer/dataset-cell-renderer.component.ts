import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  Dataset,
  DatasetOptions,
  EntityViewService,
  GridService,
  isDataRowBoundToBlockingMilestone,
  isDatasetOptions,
  ListPanelItem,
  RestService,
  SimplePermissions,
  SqtmEntityState,
  TableValueChange
} from 'sqtm-core';
import {Overlay} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {catchError, filter, map, take} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ConnectedPosition} from '@angular/cdk/overlay/position/flexible-connected-position-strategy';

@Component({
  selector: 'sqtm-app-dataset-cell-renderer',
  templateUrl: './dataset-cell-renderer.component.html',
  styleUrls: ['./dataset-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
// tslint:disable-next-line:max-line-length
export class DatasetCellRendererComponent<S extends SqtmEntityState, T extends string, P extends SimplePermissions> extends AbstractListCellRendererComponent implements OnInit {

  @ViewChild('availableDatasetPortalContent', {read: TemplateRef})
  availableDatasetPortalContent: TemplateRef<any>;

  @ViewChild('displayedDatasetElement', {read: ElementRef})
  displayedDatasetElement: ElementRef;

  canEdit$: Observable<boolean>;

  constructor(public readonly grid: GridService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public readonly viewService: EntityViewService<S, T, P>) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
  }

  get availableDatasets(): Dataset[] {
    return this.row.data.availableDatasets ?? [];
  }

  get panelItems(): ListPanelItem[] {
    return [this.makeNoneOption(), ...asListPanelItems(this.availableDatasets)];
  }

  get displayedDataset(): string {
    return this.getDisplayedDataSet(this.panelItems);
  }

  private get itemIdType(): string {
    return this.getDatasetOptions().itemIdType;
  }

  private getDatasetOptions(): DatasetOptions {
    const datasetOptions = this.columnDisplay.options;

    if (!datasetOptions || !isDatasetOptions(datasetOptions)) {
      throw Error('This cell renderer must be provided with DatasetOptions.');
    }

    return datasetOptions;
  }

  ngOnInit(): void {
    this.canEdit$ = this.viewService.componentData$.pipe(
      map((componentData) => componentData.permissions.canWrite
        && !isDataRowBoundToBlockingMilestone(this.row)
        && this.availableDatasets.length > 0)
    );
  }

  showAvailableDatasetsList(): void {
    this.canEdit$.pipe(
      take(1),
      filter(canEdit => canEdit)
    ).subscribe(() => this.showList(this.displayedDatasetElement, this.availableDatasetPortalContent, LIST_POSITIONS));
  }

  change(datasetId: number): void {
    const dataset = this.panelItems.find(item => item.id === datasetId);
    const datasetName = dataset != null ? dataset.label : this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.dataset.none');

    this.restService.post(this.getChangeUrl(), {datasetId}).pipe(
      catchError((err) => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe(() => {
      const changes: TableValueChange[] = [{columnId: this.columnDisplay.id, value: datasetName}];
      this.grid.editRows([this.row.id], changes);
      this.close();
    });
  }

  private getChangeUrl(): (string | any)[] {
    return ['test-plan-item', this.row.data[this.itemIdType].toString(), this.getUrlSuffix()];
  }

  private getUrlSuffix(): string {
    if (this.itemIdType === 'itemTestPlanId') {
      return 'dataset';
    } else {
      return 'dataset-to-ctpi';
    }
  }

  private getDisplayedDataSet(listItems: ListPanelItem[]): string {
    if (listItems.length === 1) { // If the only option is the default one
      return '-';
    } else if (this.row.data[this.columnDisplay.id] == null) {
      return this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.dataset.none');
    } else {
      return this.row.data[this.columnDisplay.id];
    }
  }

  private makeNoneOption(): ListPanelItem {
    return {
      id: null,
      label: this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.dataset.none'),
    };
  }
}

export function dataSetColumn(id: string, options: DatasetOptions): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DatasetCellRendererComponent)
    .withHeaderPosition('left')
    .withOptions(options);
}

function asListPanelItems(datasets: Dataset[]): ListPanelItem[] {
  return datasets.map(dataset => ({
    id: dataset.id,
    label: dataset.name
  }));
}

const LIST_POSITIONS: ConnectedPosition[] = [
  {originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
  {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6},
];
