import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignViewContentComponent} from './campaign-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import SpyObj = jasmine.SpyObj;
import {CampaignViewService} from '../../service/campaign-view.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('CampaignViewContentComponent', () => {
  let component: CampaignViewContentComponent;
  let fixture: ComponentFixture<CampaignViewContentComponent>;

  const campaignViewService: SpyObj<CampaignViewService> = jasmine.createSpyObj('campaignViewService', ['load']);
  campaignViewService.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      declarations: [CampaignViewContentComponent],
      providers: [
        {
          provide: CampaignViewService,
          useValue: campaignViewService
        },
        {
          provide: DialogService,
          useValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
