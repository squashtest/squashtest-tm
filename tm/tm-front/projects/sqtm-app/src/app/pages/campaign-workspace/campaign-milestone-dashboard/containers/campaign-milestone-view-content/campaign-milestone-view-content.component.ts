import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CampaignMilestoneViewState} from '../../state/campaign-milestone-view-state';
import {Observable} from 'rxjs';
import {CampaignMilestoneViewService} from '../../services/campaign-milestone-view.service';
import {TranslateService} from '@ngx-translate/core';
import {
  CustomDashboardBinding,
  CustomDashboardModel,
  ExecutionStatusCount,
  FavoriteDashboardValue,
  getSupportedBrowserLang
} from 'sqtm-core';
import {tap} from 'rxjs/operators';
import * as _ from 'lodash';

@Component({
  selector: 'sqtm-app-campaign-milestone-view-content',
  templateUrl: './campaign-milestone-view-content.component.html',
  styleUrls: ['./campaign-milestone-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignMilestoneViewContentComponent implements OnInit {

  public componentData$: Observable<Readonly<CampaignMilestoneViewState>>;

  constructor(private viewService: CampaignMilestoneViewService, public translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  refreshStats($event: MouseEvent) {
    $event.stopPropagation();
    this.viewService.refreshStatistics();
  }

  changeDashboardToDisplay($event: MouseEvent, preferenceValue: FavoriteDashboardValue) {
    $event.stopPropagation();
    this.viewService.changeDashboardToDisplay(preferenceValue).pipe(
      tap(() => this.viewService.refreshStatistics())
    ).subscribe();
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

  hasTestPlanItems(campaignTestCaseStatusStatistics: ExecutionStatusCount) {
    return Object.values(campaignTestCaseStatusStatistics).reduce(_.add) > 0;
  }

  refreshDashboard($event: MouseEvent) {
    $event.stopPropagation();
  }

  getBrowserLanguage() {
    return getSupportedBrowserLang(this.translateService);
  }
}
