import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';


export const CAMPAIGN_FOLDER_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>
('Grid config for the issue table of campaign folder view');
export const CAMPAIGN_FOLDER_ISSUE_TABLE = new InjectionToken<GridService>
('Grid service instance for the issue table of campaign folder view');
