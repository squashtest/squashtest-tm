import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {CustomExportState} from '../../state/custom-export.state';
import {IdentifiedExportColumn} from '../../../../../components/custom-export-workbench/state/custom-export-workbench.state';
import {CustomExportColumn, ProjectData, ReferentialDataService} from 'sqtm-core';
import {take} from 'rxjs/operators';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'sqtm-app-custom-export-information-panel',
  templateUrl: './custom-export-information-panel.component.html',
  styleUrls: ['./custom-export-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class CustomExportInformationPanelComponent {

  identifiedColumns: IdentifiedExportColumn[] = [];

  @Input()
  set customExport(customExport: CustomExportState) {
    this._customExport = customExport;
    this.refreshIdentifiedColumns();
  }

  get customExport(): CustomExportState {
    return this._customExport;
  }

  private _customExport: CustomExportState;

  constructor(public readonly referentialDataService: ReferentialDataService,
              public readonly cdr: ChangeDetectorRef) {
  }

  private refreshIdentifiedColumns(): void {
    if (this._customExport) {
      this.referentialDataService.connectToProjectData(this._customExport.projectId).pipe(
        take(1),
      ).subscribe(projectData => {
        this.identifiedColumns = this.asIdentifiedColumns(this._customExport.columns, projectData);
        this.cdr.markForCheck();
      });
    }
  }

  private asIdentifiedColumns(columns: CustomExportColumn[], projectData: ProjectData): IdentifiedExportColumn[] {
    return columns.map(col => ({
      columnName: col.columnName,
      entityType: col.entityType,
      cufId: col.cufId,
      label: this.findLabel(col, projectData),
      id: undefined,
    }));
  }

  private findLabel(column: CustomExportColumn, projectData: ProjectData): string {
    if (Boolean(column.cufId)) {
      return this.findCufLabel(column, projectData);
    } else {
      return column.columnName;
    }
  }

  private findCufLabel(column: CustomExportColumn, projectData: ProjectData): string {
    const customField = Object.values(projectData.customFieldBinding).flat()
      .find(binding => binding.customField.id === column.cufId)?.customField;

    if (customField) {
      return customField.label;
    } else {
      return 'sqtm-core.generic.label.deleted';
    }
  }

}
