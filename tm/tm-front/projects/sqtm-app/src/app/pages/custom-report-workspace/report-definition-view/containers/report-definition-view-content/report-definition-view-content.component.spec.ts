import {ComponentFixture, TestBed} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import {ReportDefinitionViewContentComponent} from './report-definition-view-content.component';
import {ReportDefinitionViewService} from '../../services/report-definition-view.service';
import {EMPTY} from 'rxjs';
import {DocxReportService} from '../../../../../components/report-workbench/services/docx-report.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';

describe('ReportDefinitionViewContentComponent', () => {
  let component: ReportDefinitionViewContentComponent;
  let fixture: ComponentFixture<ReportDefinitionViewContentComponent>;
  const serviceMock = jasmine.createSpyObj(['load']);
  serviceMock.componentData$ = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, OverlayModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [ReportDefinitionViewContentComponent],
      providers: [
        {
          provide: ReportDefinitionViewService,
          useValue: serviceMock
        },
        DocxReportService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportDefinitionViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
