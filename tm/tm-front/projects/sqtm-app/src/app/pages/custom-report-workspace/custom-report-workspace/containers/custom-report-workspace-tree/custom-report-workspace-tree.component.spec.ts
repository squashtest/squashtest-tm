import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CustomReportWorkspaceTreeComponent} from './custom-report-workspace-tree.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {
  DialogService,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {CR_WS_TREE, CR_WS_TREE_CONFIG} from '../../../custom-report-workspace.constant';
import {customReportWorkspaceTreeConfigFactory} from '../custom-report-workspace/custom-report-workspace.component';

describe('CustomReportWorkspaceTreeComponent', () => {
  let component: CustomReportWorkspaceTreeComponent;
  let fixture: ComponentFixture<CustomReportWorkspaceTreeComponent>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, NzDropDownModule, RouterTestingModule],
      declarations: [CustomReportWorkspaceTreeComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: CR_WS_TREE_CONFIG,
          useFactory: customReportWorkspaceTreeConfigFactory,
          deps: []
        },
        {
          provide: CR_WS_TREE,
          useFactory: gridServiceFactory,
          deps: [RestService, CR_WS_TREE_CONFIG, ReferentialDataService]
        },
        {
          provide: GridService,
          useExisting: CR_WS_TREE
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomReportWorkspaceTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
