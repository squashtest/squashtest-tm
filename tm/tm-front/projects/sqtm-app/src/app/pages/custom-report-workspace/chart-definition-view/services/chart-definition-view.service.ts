import {Injectable} from '@angular/core';
import {
  AttachmentService,
  ChartDefinitionModel,
  ChartDefinitionService,
  CustomFieldValueService,
  CustomReportPermissions,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {ChartDefinitionState} from '../state/chart-definition.state';
import {TranslateService} from '@ngx-translate/core';
import {ChartDefinitionViewState, provideInitialChartDefinitionView} from '../state/chart-definition-view.state';
import {map, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class ChartDefinitionViewService extends EntityViewService<ChartDefinitionState, 'chartDefinition', CustomReportPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private chartDefinitionService: ChartDefinitionService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData) {
    return new CustomReportPermissions(projectData);
  }

  getInitialState(): ChartDefinitionViewState {
    return provideInitialChartDefinitionView();
  }

  load(customReportLibraryNodeId: number) {
    this.chartDefinitionService.getChartDefinition(customReportLibraryNodeId)
      .pipe(withLatestFrom(this.state$))
      .subscribe(([chartDefinitionModel, state]: [ChartDefinitionModel, ChartDefinitionViewState]) => {
        const chartDefinitionState: ChartDefinitionState = {
          ...state.chartDefinition, ...chartDefinitionModel,
          generatedOn: new Date()
        };
        this.store.commit(this.updateEntity(chartDefinitionState, state));
      }, err => this.notifyEntityNotFound(err));
  }

  refresh() {
    this.store.state$.pipe(
      take(1),
      map(state => state.chartDefinition.customReportLibraryNodeId)
    ).subscribe(customReportLibraryNodeId => this.load(customReportLibraryNodeId));
  }

  changeChartName(newChartName: string, customReportLibraryNodeId: number): Observable<ChartDefinitionViewState> {
    return this.changeChartNameServerSide(newChartName, customReportLibraryNodeId).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, ChartDefinitionViewState]) => withUpdatedChartName(state, newChartName)),
      tap(state => this.store.commit(state)),
      tap((state) => this.requireExternalUpdate(state.chartDefinition.customReportLibraryNodeId, 'name'))
    );
  }

  private changeChartNameServerSide(newChartName: string, customReportLibraryNodeId: number): Observable<any> {
    return this.restService.post(['chart-definition', customReportLibraryNodeId.toString(), 'name'], {name: newChartName});
  }
}

function withUpdatedChartName(state: ChartDefinitionViewState, newChartName: string): ChartDefinitionViewState {
  return {
    ...state,
    chartDefinition: {
      ...state.chartDefinition,
      name: newChartName
    }
  };
}
