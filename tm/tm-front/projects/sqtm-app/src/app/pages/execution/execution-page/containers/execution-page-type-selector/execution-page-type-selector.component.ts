import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {Observable} from 'rxjs';
import {ExecutionPageComponentData} from '../abstract-execution-page.component';
import {ExecutionPageService} from '../../services/execution-page.service';
import {Router} from '@angular/router';

@Component({
  selector: 'sqtm-app-execution-page-type-selector',
  templateUrl: './execution-page-type-selector.component.html',
  styleUrls: ['./execution-page-type-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageTypeSelectorComponent implements OnInit {

  componentData$: Observable<ExecutionPageComponentData>;

  constructor(private executionPageService: ExecutionPageService,
              private router: Router) {
    this.componentData$ = this.executionPageService.componentData$;
  }

  ngOnInit(): void {
  }

  back() {
    this.router.navigate(['campaign-workspace']);
  }
}
