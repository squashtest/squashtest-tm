import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteExecutionPageHistoryComponent} from './delete-execution-page-history.component';
import {
  DataRow,
  DialogService,
  GenericDataRow,
  getBasicGridDisplay,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExecutionPageComponent} from '../../../containers/execution-page/execution-page.component';
import {ExecutionPageService} from '../../../services/execution-page.service';

describe('DeleteExecutionPageHistoryComponent', () => {
  let component: DeleteExecutionPageHistoryComponent;
  let fixture: ComponentFixture<DeleteExecutionPageHistoryComponent>;

  const gridConfig = grid('grid-test').build();
  const executionPageService = jasmine.createSpyObj('executionPageService', ['create']);
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  const executionPageComponent = jasmine.createSpyObj('executionPageComponent', ['create']);
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteExecutionPageHistoryComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: ExecutionPageComponent,
          useValue: executionPageComponent
        },
        {
          provide: ExecutionPageService,
          useValue: executionPageService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  it('should create', waitForAsync(() => {
    prepareComponent({
      ...new GenericDataRow(),
      id: 'exe-1',
      data: {}
    });
    expect(component).toBeTruthy();
  }));

  function prepareComponent(row: DataRow) {
    fixture = TestBed.createComponent(DeleteExecutionPageHistoryComponent);
    component = fixture.componentInstance;
    component.gridDisplay = getBasicGridDisplay();
    component.columnDisplay = {
      id: 'NAME', show: true,
      widthCalculationStrategy: new Limited(200),
      headerPosition: 'left', contentPosition: 'left', showHeader: true,
      viewportName: 'mainViewport'
    };
    component.row = row;
    fixture.detectChanges();
  }
});
