// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
//
// import {ExecutionPrologueComponent} from './execution-prologue.component';
// import {NO_ERRORS_SCHEMA} from '@angular/core';
// import {ExecutionRunnerService} from '../../services/execution-runner.service';
// import {OnPushComponentTester, TestingUtilsModule} from 'sqtm-core';
// import {BehaviorSubject, of} from 'rxjs';
// import {ExecutionRunnerComponentData} from '../execution-runner/execution-runner.component';
// import {RouterTestingModule} from '@angular/router/testing';
// import {Router} from '@angular/router';
//
// const mock = jasmine.createSpyObj<ExecutionRunnerService>('executionRunnerService', ['load']);
// const componentDataSubject = new BehaviorSubject({
//   permissions: {canExecute: true},
//   execution: {executionSteps: {ids: []}}
// } as ExecutionRunnerComponentData);
// mock.componentData$ = componentDataSubject;
//
// describe('ExecutionPrologueComponent', () => {
//   let tester: ExecutionPrologueTester;
//   let fixture: ComponentFixture<ExecutionPrologueComponent>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [TestingUtilsModule, RouterTestingModule],
//       providers: [
//         {
//           provide: ExecutionRunnerService,
//           useValue: mock
//         },
//         {
//           provide: Router,
//           useValue: {}
//         }
//       ],
//       declarations: [ExecutionPrologueComponent],
//       schemas: [NO_ERRORS_SCHEMA]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(ExecutionPrologueComponent);
//     tester = new ExecutionPrologueTester(fixture);
//     fixture.detectChanges();
//   });
//
//   it('should show start execution button', () => {
//     expect(tester.getStartButton()).toBeTruthy();
//   });
// });
