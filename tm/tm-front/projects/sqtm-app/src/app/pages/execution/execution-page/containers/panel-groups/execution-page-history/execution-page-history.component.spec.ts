import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {executionHistoryTableDefinition, ExecutionPageHistoryComponent} from './execution-page-history.component';
import {
  DialogReference,
  DialogService,
  GridService,
  gridServiceFactory,
  GridTestingModule,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {DatePipe} from '@angular/common';
import {EXECUTION_HISTORY_TABLE, EXECUTION_HISTORY_TABLE_CONF} from '../../../execution.constant';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageComponent} from '../../execution-page/execution-page.component';

describe('ExecutionPageHistoryComponent', () => {
  let component: ExecutionPageHistoryComponent;
  let fixture: ComponentFixture<ExecutionPageHistoryComponent>;

  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  const dialogReference = {data: {}};
  const executionPageComponent = jasmine.createSpyObj('executionPageComponent', ['create']);

  beforeEach(waitForAsync( () => {
    TestBed.configureTestingModule({
      imports: [
        GridTestingModule,
        AppTestingUtilsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot()],
      declarations: [ ExecutionPageHistoryComponent ],
      providers: [
        DatePipe,
        {
          provide: EXECUTION_HISTORY_TABLE_CONF,
          useFactory: executionHistoryTableDefinition
        },
        {
          provide: ExecutionPageService,
          useValue: {
            componentData$: EMPTY
          }
        },
        {
          provide: ExecutionPageComponent,
          useValue: executionPageComponent
        },
        {
          provide: EXECUTION_HISTORY_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, EXECUTION_HISTORY_TABLE_CONF, ReferentialDataService]
        },
        {
          provide: GridService,
          useExisting: EXECUTION_HISTORY_TABLE
        },
        {provide: DialogService, useValue: dialogService},
        {
          provide: DialogReference,
          useValue: dialogReference
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
