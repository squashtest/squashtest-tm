import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSuiteTestPlanRunnerComponent } from './test-suite-test-plan-runner.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TestSuiteTestPlanRunnerComponent', () => {
  let component: TestSuiteTestPlanRunnerComponent;
  let fixture: ComponentFixture<TestSuiteTestPlanRunnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, AppTestingUtilsModule, OverlayModule, HttpClientTestingModule],
      declarations: [ TestSuiteTestPlanRunnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSuiteTestPlanRunnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
