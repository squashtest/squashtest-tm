import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ParameterDialogComponent} from './parameter-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import createSpyObj = jasmine.createSpyObj;

describe('ParameterDialogComponent', () => {
  let component: ParameterDialogComponent;
  let fixture: ComponentFixture<ParameterDialogComponent>;
  const dialogReference = createSpyObj(['close']);
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [ParameterDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogReference, useValue: dialogReference},
        {provide: RestService, useValue: restService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
