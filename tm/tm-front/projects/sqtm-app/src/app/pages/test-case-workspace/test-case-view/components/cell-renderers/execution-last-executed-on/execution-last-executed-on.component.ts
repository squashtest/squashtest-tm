import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, getSupportedBrowserLang, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-execution-last-executed-on',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <span
          style="margin: auto 0 auto 0">{{row.data[columnDisplay.id] | date:'shortDate': '' : getBrowserLanguage()}}</span>
      </div>
    </ng-container>`,
  styleUrls: ['./execution-last-executed-on.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionLastExecutedOnComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public gridService: GridService, public cdRef: ChangeDetectorRef, public translateService: TranslateService) {
    super(gridService, cdRef);
  }

  ngOnInit() {
  }

  getBrowserLanguage() {
    return getSupportedBrowserLang(this.translateService);
  }

}

export function executionLastExecutedOn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ExecutionLastExecutedOnComponent);
}
