import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  DialogReference,
  EditableRichTextComponent,
  EditableTextFieldComponent,
  FieldValidationError,
  Parameter,
  ParametersService
} from 'sqtm-core';
import {ParamInformationConfiguration} from './param-information-configuration';
import {Router} from '@angular/router';
import {Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-param-information-dialog',
  templateUrl: './param-information-dialog.component.html',
  styleUrls: ['./param-information-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParamInformationDialogComponent implements OnInit {

  @ViewChild('paramName')
  paramName: EditableTextFieldComponent;

  @ViewChild('paramDescription')
  paramDescription: EditableRichTextComponent;

  data: ParamInformationConfiguration;
  serverSideValidationErrors: FieldValidationError[] = [];

  constructor(private dialogReference: DialogReference<ParamInformationConfiguration>,
              private router: Router,
              private parametersService: ParametersService,
              private cdr: ChangeDetectorRef,
              private translateService: TranslateService) {
    this.data = dialogReference.data;
  }

  ngOnInit(): void {
  }

  navigateToTestCase() {
    this.dialogReference.close();
    this.router.navigate(['/test-case-workspace/test-case/detail', this.data.parameter.sourceTestCaseId]);
  }

  getValidatorsName() {
    return [Validators.required, Validators.maxLength(255)];
  }

  updateName(newName: string) {

    if (newName.search('^[A-Za-z0-9_-]{1,255}$') !== -1) {
      this.parametersService.renameParameter(this.data.parameter.id, newName).subscribe(() => {
        this.dialogReference.result = {id: this.data.parameter.id, name: newName};
        this.paramName.value = newName;
        this.cdr.markForCheck();
      }, error => {
        this.handleCreationFailure(error);
      });
    } else {
      this.paramName.showExternalErrorMessage([this.translateService.instant('sqtm-core.validation.errors.paramName')]);
    }
  }

  private handleCreationFailure(error: HttpErrorResponse) {
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.serverSideValidationErrors = squashError.fieldValidationErrors;
        this.paramName.showExternalErrorMessage([this.translateService.instant(squashError.fieldValidationErrors[0].i18nKey)]);
        this.cdr.markForCheck();
      }
    }
  }

  getTestCaseSourceName(parameter: Parameter) {
    let sourceName = `${parameter.sourceTestCaseName} (${parameter.sourceTestCaseProjectName})`;

    if (parameter.sourceTestCaseReference !== '') {
      sourceName = `${parameter.sourceTestCaseReference} - ${sourceName}`;
    }

    return sourceName;
  }

  updateDescription(newDescription: string) {
    this.parametersService.changeDescription(this.data.parameter.id, newDescription).subscribe(
      () => {
        this.dialogReference.result = {id: this.data.parameter.id, description: newDescription};
        this.paramDescription.value = newDescription;
      }
    );
  }
}
