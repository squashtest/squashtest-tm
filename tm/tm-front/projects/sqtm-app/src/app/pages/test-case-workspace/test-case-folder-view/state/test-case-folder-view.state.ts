import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {TestCaseFolderState} from './test-case-folder.state';

export interface TestCaseFolderViewState extends EntityViewState<TestCaseFolderState, 'testCaseFolder'> {
  testCaseFolder: TestCaseFolderState;
}

export function provideInitialTestCaseFolderView(): Readonly<TestCaseFolderViewState> {
  return provideInitialViewState<TestCaseFolderState, 'testCaseFolder'>('testCaseFolder');
}
