import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {TestCaseLibraryState} from './test-case-library.state';

export interface TestCaseLibraryViewState extends EntityViewState<TestCaseLibraryState, 'testCaseLibrary'> {
  testCaseLibrary: TestCaseLibraryState;
}

export function provideInitialTestCaseLibraryView(): Readonly<TestCaseLibraryViewState> {
  return provideInitialViewState<TestCaseLibraryState, 'testCaseLibrary'>('testCaseLibrary');
}
