import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {TestCaseWorkspaceTreeComponent} from './test-case-workspace-tree.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TC_WS_TREE, TC_WS_TREE_CONFIG} from '../../../test-case-workspace.constant';
import {
  GridService,
  DialogService,
  gridServiceFactory,
  RestService,
  ReferentialDataService
} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {testCaseWorkspaceTreeConfigFactory} from '../test-case-workspace/test-case-workspace.component';

describe('TestCaseWorkspaceTreeComponent', () => {
  let component: TestCaseWorkspaceTreeComponent;
  let fixture: ComponentFixture<TestCaseWorkspaceTreeComponent>;

  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, NzDropDownModule, RouterTestingModule],
      declarations: [TestCaseWorkspaceTreeComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: TC_WS_TREE_CONFIG,
          useFactory: testCaseWorkspaceTreeConfigFactory,
          deps: []
        },
        {
          provide: TC_WS_TREE,
          useFactory: gridServiceFactory,
          deps: [RestService, TC_WS_TREE_CONFIG, ReferentialDataService]
        },
        {
          provide: GridService,
          useExisting: TC_WS_TREE
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseWorkspaceTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
