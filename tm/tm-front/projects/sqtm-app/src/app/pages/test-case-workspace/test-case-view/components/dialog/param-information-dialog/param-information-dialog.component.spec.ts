import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ParamInformationDialogComponent} from './param-information-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference, Parameter} from 'sqtm-core';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ParamInformationConfiguration} from './param-information-configuration';
import {EMPTY} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ParamInformationDialogComponent', () => {
  let component: ParamInformationDialogComponent;
  let fixture: ComponentFixture<ParamInformationDialogComponent>;
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);
  const dialogReference: DialogReference<ParamInformationConfiguration> = new DialogReference<ParamInformationConfiguration>(
    '', null, overlayReference, {id: '', parameter: {} as Parameter, delegate: false, canWrite: true}
  );
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ParamInformationDialogComponent],
      imports: [RouterTestingModule, TranslateModule.forRoot(), HttpClientTestingModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogReference, useValue: dialogReference}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParamInformationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
