import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {TestCaseViewComponentData} from '../../containers/test-case-view/test-case-view.component';
import {ActionErrorDisplayService, EditableTaTestComponent} from 'sqtm-core';
import {catchError, finalize, tap} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-script-auto',
  templateUrl: './script-auto.component.html',
  styleUrls: ['./script-auto.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScriptAutoComponent implements OnInit {

  @Input()
  testCaseViewComponentData: TestCaseViewComponentData;

  @ViewChild('taTestField')
  taTestField: EditableTaTestComponent;

  get editable() {
    return this.testCaseViewComponentData.permissions.canWrite && this.testCaseViewComponentData.milestonesAllowModification;
  }

  constructor(private testCaseViewService: TestCaseViewService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  ngOnInit(): void {
  }

  getAutomatedTestName(componentData: TestCaseViewComponentData) {
    const taTest = componentData.testCase.automatedTest;
    if (taTest) {
      return taTest.fullLabel;
    } else {
      return '';
    }
  }

  changeAutomatedTest(taTest: string) {
    this.testCaseViewService.updateTaTest(taTest).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.taTestField.endAsync()),
      tap(() => this.taTestField.disableEditMode())
    ).subscribe();
  }

}
