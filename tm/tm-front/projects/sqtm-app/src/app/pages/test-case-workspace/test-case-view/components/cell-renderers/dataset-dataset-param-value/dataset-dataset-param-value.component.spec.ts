import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DatasetDatasetParamValueComponent} from './dataset-dataset-param-value.component';
import {DialogService, GridTestingModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TestCaseViewService} from '../../../service/test-case-view.service';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DatasetDatasetParamValueComponent', () => {
  let component: DatasetDatasetParamValueComponent;
  let fixture: ComponentFixture<DatasetDatasetParamValueComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule, TranslateModule.forRoot()],
      declarations: [DatasetDatasetParamValueComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService},
        {provide: DialogService, useValue: dialogService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetDatasetParamValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
