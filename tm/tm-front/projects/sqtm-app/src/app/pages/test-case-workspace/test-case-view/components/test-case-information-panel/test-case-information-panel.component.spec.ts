import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseInformationPanelComponent} from './test-case-information-panel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {EMPTY, of} from 'rxjs';
import {DatePipe} from '@angular/common';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {ReferentialDataService} from 'sqtm-core';

describe('TestCaseInformationPanelComponent', () => {
  let component: TestCaseInformationPanelComponent;
  let fixture: ComponentFixture<TestCaseInformationPanelComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  const datePipe = jasmine.createSpyObj('datePipe', ['transform']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [TestCaseInformationPanelComponent],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        },
        {
          provide: DatePipe,
          useValue: datePipe
        },
        {
          provide: ReferentialDataService,
          useValue: {
            globalConfiguration$: of({}),
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
