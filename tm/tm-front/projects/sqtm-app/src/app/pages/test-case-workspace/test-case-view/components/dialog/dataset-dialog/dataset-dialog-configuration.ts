import {CreationDialogData, Parameter} from 'sqtm-core';

export interface DatasetDialogConfiguration extends CreationDialogData {
  testCaseId: number;
  parameters: Parameter[];
}
