import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseViewAutomationPanelComponent} from './test-case-view-automation-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {WorkspaceCommonModule} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {DatePipe} from '@angular/common';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseViewAutomationPanelComponent', () => {
  let component: TestCaseViewAutomationPanelComponent;
  let fixture: ComponentFixture<TestCaseViewAutomationPanelComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const datePipe = jasmine.createSpyObj('datePipe', ['transform']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestCaseViewAutomationPanelComponent],
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, WorkspaceCommonModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        },
        {
          provide: DatePipe,
          useValue: datePipe
        }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseViewAutomationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
