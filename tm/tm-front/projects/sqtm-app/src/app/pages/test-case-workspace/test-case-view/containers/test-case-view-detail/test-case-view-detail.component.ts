import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, Renderer2, ViewContainerRef} from '@angular/core';
import {AbstractTestCaseViewComponent} from '../abstract-test-case-view.component';
import {
  TCW_CALLED_TC_TABLE,
  TCW_CALLED_TC_TABLE_CONF,
  TCW_COVERAGE_TABLE,
  TCW_COVERAGE_TABLE_CONF,
  TCW_DATASET_TABLE,
  TCW_DATASET_TABLE_CONF
} from '../../test-case-view.constant';
import {tcwCoverageTableDefinition} from '../../components/coverage-table/coverage-table.component';
import {
  AttachmentService,
  CopierService,
  CustomFieldValueService,
  DialogConfiguration,
  DialogService,
  DragAndDropService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  gridServiceFactory,
  MilestoneModeData,
  REFERENCE_FIELD,
  ReferentialDataService,
  RestService,
  TestCaseService,
  TestStepService
} from 'sqtm-core';
import {tcwDatasetsTableDefinition} from '../../components/datasets-table/datasets-table.component';
import {tcwCalledTCTableDefiniton} from '../../components/called-test-case/called-test-case.component';
import {TestStepViewService} from '../../service/test-step-view.service';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {filter, take} from 'rxjs/operators';
import {combineLatest} from 'rxjs';
import {
  NewTestCaseVersionDialogConfiguration,
  NewTestCaseVersionDialogResult
} from '../../components/dialog/new-version-dialog/new-test-case-version-dialog.configuration';
import {TestCaseViewComponentData} from '../test-case-view/test-case-view.component';
import {NewVersionDialogComponent} from '../../components/dialog/new-version-dialog/new-version-dialog.component';

@Component({
  selector: 'sqtm-app-test-case-view-detail',
  templateUrl: './test-case-view-detail.component.html',
  styleUrls: ['./test-case-view-detail.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TCW_COVERAGE_TABLE_CONF,
      useFactory: tcwCoverageTableDefinition
    },
    {
      provide: TCW_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_COVERAGE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: TCW_DATASET_TABLE_CONF,
      useFactory: tcwDatasetsTableDefinition
    },
    {
      provide: TCW_DATASET_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_DATASET_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: TCW_CALLED_TC_TABLE_CONF,
      useFactory: tcwCalledTCTableDefiniton
    },
    {
      provide: TCW_CALLED_TC_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_CALLED_TC_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: TestStepViewService,
      useClass: TestStepViewService,
      deps: [
        TestStepService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ReferentialDataService,
        CopierService
      ]
    },
    {
      provide: TestCaseViewService,
      useClass: TestCaseViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        TestCaseService,
        CustomFieldValueService,
        TestStepViewService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        TCW_COVERAGE_TABLE,
        TCW_DATASET_TABLE,
        TCW_CALLED_TC_TABLE
      ]
    },
    {
      provide: EntityViewService,
      useExisting: TestCaseViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestCaseViewService
    }]
})
export class TestCaseViewDetailComponent extends AbstractTestCaseViewComponent {

  @Input()
  showFoldAndCloseButtons = true;

  constructor(route: ActivatedRoute,
              router: Router,
              testCaseViewService: TestCaseViewService,
              referentialDataService: ReferentialDataService,
              cdRef: ChangeDetectorRef,
              translateService: TranslateService,
              dndService: DragAndDropService,
              renderer: Renderer2,
              vcr: ViewContainerRef,
              dialogService: DialogService) {
    super(route, router, testCaseViewService, referentialDataService, cdRef, translateService, dndService, renderer, vcr, dialogService);
  }

  isMilestoneModeEnabled() {
    let isMilestoneModeEnabled = false;
    this.referentialDataService.milestoneModeData$.pipe(
      take(1)
    ).subscribe((milestoneModeData: MilestoneModeData) => {
      isMilestoneModeEnabled = milestoneModeData.milestoneModeEnabled;
    });
    return isMilestoneModeEnabled;
  }

  createNewTestCaseVersion() {
    combineLatest([this.componentData$, this.referentialDataService.milestoneModeData$]).pipe(
      take(1),
    ).subscribe(([componentData, milestoneData]: [TestCaseViewComponentData, MilestoneModeData]) => {
      const configuration = this.buildNewVersionDialogConfiguration(componentData, milestoneData);
      this.dialogService.openDialog<NewTestCaseVersionDialogConfiguration, NewTestCaseVersionDialogResult>(configuration)
        .dialogClosed$.pipe(
        filter(result => Boolean(result)),
      ).subscribe();
    });
  }

  private buildNewVersionDialogConfiguration(componentData: TestCaseViewComponentData, milestoneData: MilestoneModeData) {
    const name = `${componentData.testCase.name}-${milestoneData.selectedMilestone.label}`;
    const reference = componentData.testCase.reference;
    const description = componentData.testCase.description;
    const tcKind = componentData.testCase.kind;
    const configuration: DialogConfiguration<NewTestCaseVersionDialogConfiguration> = {
      viewContainerReference: this.vcr,
      component: NewVersionDialogComponent,
      id: 'new-test-case-version',
      width: 800,
      data: {
        titleKey: 'sqtm-core.generic.label.add-new-version',
        id: 'new-test-case-version',
        originalTestCaseId: componentData.testCase.id,
        projectId: componentData.projectData.id,
        optionalTextFields: [{...REFERENCE_FIELD, defaultValue: reference}],
        defaultValues: {
          name,
          description
        },
        tcKind
      }
    };
    return configuration;
  }
}
