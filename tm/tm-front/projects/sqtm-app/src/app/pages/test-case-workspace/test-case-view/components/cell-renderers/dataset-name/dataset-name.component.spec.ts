import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DatasetNameComponent} from './dataset-name.component';
import {DialogService, GridTestingModule} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TestCaseViewService} from '../../../service/test-case-view.service';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DatasetNameComponent', () => {
  let component: DatasetNameComponent;
  let fixture: ComponentFixture<DatasetNameComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule],
      declarations: [DatasetNameComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService},
        {provide: DialogService, useValue: dialogService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
