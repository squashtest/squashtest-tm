import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ParameterHeaderComponent} from './parameter-header.component';
import {DialogService, GridTestingModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TestCaseViewService} from '../../../service/test-case-view.service';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ParameterHeaderComponent', () => {
  let component: ParameterHeaderComponent;
  let fixture: ComponentFixture<ParameterHeaderComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule, TranslateModule.forRoot()],
      declarations: [ParameterHeaderComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService},
        {provide: DialogService, useValue: dialogService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
