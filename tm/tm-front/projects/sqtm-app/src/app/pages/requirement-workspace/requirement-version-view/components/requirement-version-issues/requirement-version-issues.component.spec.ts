import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementVersionIssuesComponent, rvIssuesTableDefinition} from './requirement-version-issues.component';
import {EMPTY} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {RV_ISSUE_TABLE, RV_ISSUE_TABLE_CONF} from '../../requirement-version-view.constant';
import {gridServiceFactory, GridTestingModule, ReferentialDataService, RestService} from 'sqtm-core';

describe('RequirementVersionIssuesComponent', () => {
  let component: RequirementVersionIssuesComponent;
  let fixture: ComponentFixture<RequirementVersionIssuesComponent>;
  let requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load']);
  requirementVersionViewService = {...requirementVersionViewService, componentData$: EMPTY};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule, TranslateModule.forRoot()],
      declarations: [RequirementVersionIssuesComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: RV_ISSUE_TABLE_CONF,
          useFactory: rvIssuesTableDefinition
        },
        {
          provide: RV_ISSUE_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, RV_ISSUE_TABLE_CONF, ReferentialDataService]
        },
        {provide: RequirementVersionViewService, useValue: requirementVersionViewService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
