import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import { concatMap, take } from 'rxjs/operators';
import {RequirementVersionStatsBundle} from 'sqtm-core';
import { RequirementVersionViewComponentData } from '../../containers/requirement-version-view/requirement-version-view.component';
import { RequirementVersionViewService } from '../../services/requirement-version-view.service';

@Component({
  selector: 'sqtm-app-requirement-version-view-rate-panel',
  templateUrl: './requirement-version-view-rate-panel.component.html',
  styleUrls: ['./requirement-version-view-rate-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionViewRatePanelComponent implements OnInit {

  componentData$: Observable<RequirementVersionViewComponentData>;

  @Input()
  isHighLevelRequirementView: boolean;

  constructor(private readonly requirementViewService: RequirementVersionViewService) {
    this.componentData$ = requirementViewService.componentData$;
  }

  ngOnInit(): void {
    this.componentData$.pipe(
      take(1),
      concatMap(() => this.requirementViewService.refreshRates())
    ).subscribe();
  }

}
