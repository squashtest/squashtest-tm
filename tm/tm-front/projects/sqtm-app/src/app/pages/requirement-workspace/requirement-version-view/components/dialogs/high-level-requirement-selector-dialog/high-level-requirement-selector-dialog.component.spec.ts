import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HighLevelRequirementSelectorDialogComponent} from './high-level-requirement-selector-dialog.component';
import {DialogReference, GridModule} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import createSpyObj = jasmine.createSpyObj;
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';

describe('HighLevelRequirementSelectorDialogComponent', () => {
  let component: HighLevelRequirementSelectorDialogComponent;
  let fixture: ComponentFixture<HighLevelRequirementSelectorDialogComponent>;
  const dialogReference = createSpyObj(['close']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GridModule, HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [HighLevelRequirementSelectorDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HighLevelRequirementSelectorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
