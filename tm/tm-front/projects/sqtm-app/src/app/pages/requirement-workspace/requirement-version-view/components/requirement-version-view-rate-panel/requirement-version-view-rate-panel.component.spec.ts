import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EMPTY } from 'rxjs';
import { WorkspaceCommonModule } from '../../../../../../../../sqtm-core/src/lib/ui/workspace-common/workspace-common.module';
import { RequirementVersionViewService } from '../../services/requirement-version-view.service';

import { RequirementVersionViewRatePanelComponent } from './requirement-version-view-rate-panel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementVersionViewRatePanelComponent', () => {
  let component: RequirementVersionViewRatePanelComponent;
  let fixture: ComponentFixture<RequirementVersionViewRatePanelComponent>;

  let requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load']);
  requirementVersionViewService = {...requirementVersionViewService, componentData$: EMPTY};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementVersionViewRatePanelComponent ],
      imports: [ HttpClientTestingModule, TranslateModule.forRoot(), WorkspaceCommonModule ],
      providers: [
        { provide: RequirementVersionViewService,
          useValue: requirementVersionViewService
        }],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionViewRatePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
