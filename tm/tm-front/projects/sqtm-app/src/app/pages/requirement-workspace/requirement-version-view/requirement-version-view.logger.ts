import {sqtmAppLogger} from '../../../app-logger';

export const requirementVersionLogger = sqtmAppLogger.compose('requirement-version');
