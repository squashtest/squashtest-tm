import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BindReqToHighLevelReqDialogComponent } from './bind-req-to-high-level-req-dialog.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference} from 'sqtm-core';
import createSpyObj = jasmine.createSpyObj;
import {TranslateModule} from '@ngx-translate/core';

describe('BindRequirementToHighLevelRequirementDialogComponent', () => {
  let component: BindReqToHighLevelReqDialogComponent;
  let fixture: ComponentFixture<BindReqToHighLevelReqDialogComponent>;
  const dialogReference = createSpyObj(['close']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ BindReqToHighLevelReqDialogComponent ],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BindReqToHighLevelReqDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      highLevelRequirementsInSelection: [],
      childRequirementsInSelection: [],
      alreadyLinked: [],
      alreadyLinkedToAnotherHighLevelRequirement: [],
      requirementWithNotLinkableStatus: [],
      titleKey: '',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
