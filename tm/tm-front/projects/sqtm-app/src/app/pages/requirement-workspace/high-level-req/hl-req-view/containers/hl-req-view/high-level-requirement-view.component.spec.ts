import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { HighLevelRequirementViewComponent } from './high-level-requirement-view.component';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {RequirementVersionService, WorkspaceWithTreeComponent} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('HighLevelRequirementViewComponent', () => {
  let component: HighLevelRequirementViewComponent;
  let fixture: ComponentFixture<HighLevelRequirementViewComponent>;

  const requirementVersionService = jasmine.createSpyObj('requirementVersionService', ['load']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        RouterTestingModule
      ],
      declarations: [HighLevelRequirementViewComponent],
      providers: [{
        provide: RequirementVersionService,
        useValue: requirementVersionService
      }, {provide: WorkspaceWithTreeComponent, useValue: WorkspaceWithTreeComponent}],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighLevelRequirementViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
