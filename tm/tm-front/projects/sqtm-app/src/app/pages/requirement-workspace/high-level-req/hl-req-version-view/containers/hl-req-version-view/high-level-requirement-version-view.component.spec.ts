import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HighLevelRequirementVersionViewComponent} from './high-level-requirement-version-view.component';
import {EMPTY} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TranslateModule} from '@ngx-translate/core';
import {ReferentialDataService, SqtmDragAndDropModule} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';
import {RequirementVersionViewService} from '../../../../requirement-version-view/services/requirement-version-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';

describe('HighLevelRequirementVersionViewComponent', () => {
  let component: HighLevelRequirementVersionViewComponent;
  let fixture: ComponentFixture<HighLevelRequirementVersionViewComponent>;

  const requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load', 'complete']);
  requirementVersionViewService.componentData$ = EMPTY;
  const referentialDataService = jasmine.createSpyObj(['load']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule,
        NzDropDownModule,
        TranslateModule.forRoot(),
        SqtmDragAndDropModule,
        OverlayModule,
        RouterTestingModule,
      ],
      declarations: [HighLevelRequirementVersionViewComponent],
      providers: [
        {provide: RequirementVersionViewService, useValue: requirementVersionViewService},
        {provide: ReferentialDataService, useValue: referentialDataService}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighLevelRequirementVersionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
