import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RequirementVersionViewComponent} from './requirement-version-view.component';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {GridService, gridServiceFactory, ReferentialDataService, RestService, SqtmDragAndDropModule} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import {REQ_WS_TREE, REQ_WS_TREE_CONFIG} from '../../../requirement-workspace/requirement-workspace.constant';
import {reqTreeConfigFactory} from '../../../requirement-workspace/containers/requirement-workspace/requirement-workspace.component';

describe('RequirementVersionViewComponent', () => {
  let component: RequirementVersionViewComponent;
  let fixture: ComponentFixture<RequirementVersionViewComponent>;
  const requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load', 'complete']);
  requirementVersionViewService.componentData$ = EMPTY;
  const referentialDataService = jasmine.createSpyObj(['load']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule,
        NzDropDownModule,
        TranslateModule.forRoot(),
        SqtmDragAndDropModule,
        OverlayModule,
        RouterTestingModule
      ],
      declarations: [RequirementVersionViewComponent],
      providers: [
        {provide: RequirementVersionViewService, useValue: requirementVersionViewService},
        {provide: ReferentialDataService, useValue: referentialDataService},
        {provide: REQ_WS_TREE_CONFIG, useFactory: reqTreeConfigFactory},
        {
          provide: REQ_WS_TREE,
          useFactory: gridServiceFactory,
          deps: [RestService, REQ_WS_TREE_CONFIG, ReferentialDataService]
        },
        {
          provide: GridService,
          useExisting: REQ_WS_TREE
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
