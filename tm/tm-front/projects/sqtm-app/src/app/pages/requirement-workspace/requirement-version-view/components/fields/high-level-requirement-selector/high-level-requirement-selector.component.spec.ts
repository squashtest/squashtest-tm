import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HighLevelRequirementSelectorComponent} from './high-level-requirement-selector.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {EMPTY} from 'rxjs';
import {RequirementVersionViewService} from '../../../services/requirement-version-view.service';
import {ReferentialDataService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';


describe('HighLevelRequirementSelectorComponent', () => {
  let component: HighLevelRequirementSelectorComponent;
  let fixture: ComponentFixture<HighLevelRequirementSelectorComponent>;
  const requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load', 'complete']);
  requirementVersionViewService.componentData$ = EMPTY;
  const referentialDataService = jasmine.createSpyObj(['load']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HighLevelRequirementSelectorComponent],
      imports: [OverlayModule],
      providers: [
        {provide: RequirementVersionViewService, useValue: requirementVersionViewService},
        {provide: ReferentialDataService, useValue: referentialDataService}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HighLevelRequirementSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
