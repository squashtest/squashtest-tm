import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedLowLvlRequirementTableComponent } from './linked-low-lvl-requirement-table.component';

describe('LinkedLowLvlRequirementTableComponent', () => {
  let component: LinkedLowLvlRequirementTableComponent;
  let fixture: ComponentFixture<LinkedLowLvlRequirementTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinkedLowLvlRequirementTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedLowLvlRequirementTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
