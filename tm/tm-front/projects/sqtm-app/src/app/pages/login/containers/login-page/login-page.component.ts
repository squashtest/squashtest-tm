import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {LoginPageService} from '../../services/login-page.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {finalize, withLatestFrom} from 'rxjs/operators';
import {LOGIN_PAGE_REDIRECT_AFTER_AUTH, SquashPlatformNavigationService} from 'sqtm-core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'sqtm-app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: LoginPageService,
      useClass: LoginPageService
    }
  ]
})
export class LoginPageComponent implements OnInit, OnDestroy {

  pending: boolean;

  form = this.fb.group({
    login: '',
    password: ''
  });

  constructor(
    private fb: FormBuilder,
    public loginPageService: LoginPageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private platformNavigationService: SquashPlatformNavigationService,
    private domSanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.loginPageService.load();
  }

  ngOnDestroy(): void {
    this.loginPageService.complete();
  }

  handleLogin() {
    this.pending = true;

    this.loginPageService.login(this.form.get('login').value, this.form.get('password').value).pipe(
      withLatestFrom(this.activatedRoute.queryParamMap),
      finalize(() => this.pending = false),
    ).subscribe(
      ([response, paramMap]) => {
        if (response.authenticated) {
          if (response.showInformation) {
            const url = `information?${LOGIN_PAGE_REDIRECT_AFTER_AUTH}=${this.getUrl(paramMap)}`;
            this.platformNavigationService.navigateFromMainApplication(url);
          } else {
            const url = this.getUrl(paramMap);
            this.platformNavigationService.navigateFromMainApplication(url);
          }
        } else {
          this.loginPageService.showLoginErrorMessage();
        }
      }
    );
  }

  private getUrl(paramMap: ParamMap) {
    return paramMap.has(LOGIN_PAGE_REDIRECT_AFTER_AUTH) ? paramMap.get(LOGIN_PAGE_REDIRECT_AFTER_AUTH) : '/home-workspace';
  }

  public getLoginMessage(rawMessage: string) {
    return this.domSanitizer.bypassSecurityTrustHtml(rawMessage);
  }
}
