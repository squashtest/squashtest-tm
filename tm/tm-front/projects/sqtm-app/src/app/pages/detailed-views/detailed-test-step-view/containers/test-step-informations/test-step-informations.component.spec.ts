import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestStepInformationsComponent} from './test-step-informations.component';
import {RouterTestingModule} from '@angular/router/testing';
import {DetailedTestStepViewService} from '../../services/detailed-test-step-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestStepInformationsComponent', () => {
  let component: TestStepInformationsComponent;
  let fixture: ComponentFixture<TestStepInformationsComponent>;

  const serviceMock = jasmine.createSpyObj(['changeCurrentStepIndex']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        {
          provide: DetailedTestStepViewService,
          useValue: serviceMock
        }
      ],
      declarations: [TestStepInformationsComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestStepInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
