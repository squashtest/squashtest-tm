import {TestBed} from '@angular/core/testing';

import {AnchorService, registerLinkState} from './anchor.service';
import {AnchorsState, initialAnchorsState, LinkState} from './anchors.state';
import {Router, RouterModule} from '@angular/router';

const informationLinkState: LinkState = {
  id: 'information',
  group: 'content',
  active: false,
  visible: true,
};

const stepsLinkState: LinkState = {
  id: 'steps',
  group: 'steps',
  active: true,
  visible: true,
};

const parametersLinkState: LinkState = {
  id: 'parameters',
  group: 'content',
  active: false,
  visible: true,
};

const anchorsState: AnchorsState = initialAnchorsState;

describe('AnchorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterModule],
    providers: [{ provide: Router, useValue: { url: '' }}]
  }));

  it('should be created', () => {
    const service: AnchorService = TestBed.inject(AnchorService);
    expect(service).toBeTruthy();
  });


  it('register new view in state', () => {

    expect(anchorsState.ids.length).toBe(0);
    const nextState = registerLinkState('test-case-view', informationLinkState, anchorsState, '');
    expect(nextState.ids.length).toBe(1);
    expect(nextState.ids[0]).toBe('test-case-view');
    expect(nextState.entities['test-case-view'].ids[0]).toBe('information');
  });

  it('update specific view in state', () => {
    let nextState = registerLinkState('test-case-view', informationLinkState, anchorsState, '');
    const firstView = nextState.ids[0];
    expect(nextState.ids.length).toBe(1);
    expect(firstView).toBe('test-case-view');
    let viewState = getViewState(nextState, firstView);
    expect(viewState.ids.length).toBe(1);
    nextState = registerLinkState('test-case-view', parametersLinkState, nextState, '');
    expect(nextState.ids.length).toBe(1);
    viewState = getViewState(nextState, firstView);
    expect(viewState.ids.length).toBe(2);
    nextState = registerLinkState('test-case-view', stepsLinkState, nextState, '');
    expect(nextState.ids.length).toBe(1);
    viewState = getViewState(nextState, firstView);
    expect(viewState.ids.length).toBe(3);
  });

  it('should select the initial anchor', () => {
    const viewId = 'test-case-view';
    const initialGroupPanel = 'yup';

    let nextState = registerLinkState(viewId, mockLink('nope', '1'), anchorsState, initialGroupPanel);
    nextState = registerLinkState(viewId, mockLink(initialGroupPanel, 'the one'), nextState, initialGroupPanel);
    nextState = registerLinkState(viewId, mockLink(initialGroupPanel, 'too late'), nextState, initialGroupPanel);
    nextState = registerLinkState(viewId, mockLink('nop', '42'), nextState, '');

    expect(nextState.entities[viewId].selectedLinkId).toBe('the one');
  });

  function getViewState(state, viewId) {
    return state.entities[viewId];
  }

  function mockLink(group: string, id: string, active = true, visible = true): LinkState {
    return {group, id, active, visible};
  }
});
