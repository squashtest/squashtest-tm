import {Injectable} from '@angular/core';
import {
  activeGroupSelector,
  activeLinkSelector,
  anchorEntityAdapter,
  anchorsEntityAdapter,
  AnchorsState,
  AnchorState,
  initialAnchorsState,
  initialAnchorState,
  isAnchorLinkActiveAndVisibleSelector,
  LinkState,
  panelIsActiveSelector
} from './anchors.state';
import {createStore, Store} from '../../../core/store/store';
import {map, take, tap} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {BehaviorSubject, Observable} from 'rxjs';
import {anchorLogger} from '../anchor.logger';
import {Router} from '@angular/router';

const logger = anchorLogger.compose('AnchorService');

@Injectable({
  providedIn: 'root'
})
export class AnchorService {

  private store: Store<AnchorsState> = createStore(initialAnchorsState, {
    id: 'AnchorStore',
    logDiff: 'detailed'
  });

  public state$ = this.store.state$;

  private _requestScrollSubject = new BehaviorSubject<Pick<AnchorState, 'viewId' | 'selectedLinkId'>>({
    viewId: '',
    selectedLinkId: ''
  });

  public requestScroll$ = this._requestScrollSubject.asObservable();

  private maybeInitialPanelGroup: string;

  constructor(router: Router) {
    this.extractInitialPanelGroupFromUrl(router);
  }

  registerLink(viewId: string, link: LinkState) {
    this.store.state$.pipe(
      take(1),
      map(state => registerLinkState(viewId, link, state, this.maybeInitialPanelGroup))
    ).subscribe(state => this.store.commit(state));
  }

  beginLoadPhase() {
    this.store.state$.pipe(
      take(1),
      map(state => ({...state, loaded: false})),
    ).subscribe(state => this.store.commit(state));
  }

  notifyRestoredLink(viewId: string, linkId: string) {
    this.store.state$.pipe(
      take(1),
      map(state => ({...state, loaded: true})),
      map(state => anchorsEntityAdapter.updateOne({id: viewId, changes: {selectedLinkId: linkId}}, state)),
      tap(() => this._requestScrollSubject.next({viewId: viewId, selectedLinkId: linkId}))
    ).subscribe(state => this.store.commit(state));
  }

  notifyActiveLink(viewId: string, linkId: string) {
    this.store.state$.pipe(
      take(1),
      map(state => anchorsEntityAdapter.updateOne({id: viewId, changes: {selectedLinkId: linkId}}, state)),
    ).subscribe(state => this.store.commit(state));
  }

  notifyAndNavigateToActiveLink(viewId: string, linkId: string) {
    this.store.state$.pipe(
      take(1),
      map(state => anchorsEntityAdapter.updateOne({id: viewId, changes: {selectedLinkId: linkId}}, state)),
      // performing side effect by emitting into dedicated subject.
      tap(() => this._requestScrollSubject.next({viewId: viewId, selectedLinkId: linkId}))
    ).subscribe(state => this.store.commit(state));
  }

  notifyCollapsePanel(viewId: string, linkId: string, active: boolean) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        let nextState = {...state};
        let anchorState = nextState.entities[viewId];
        anchorState = anchorEntityAdapter.updateOne({id: linkId, changes: {active: active}}, anchorState);
        nextState = anchorsEntityAdapter.upsertOne(anchorState, nextState);
        return nextState;
      })
    ).subscribe(state => this.store.commit(state));
  }

  getActiveLink(viewId: string): Observable<string> {
    return this.state$.pipe(select(activeLinkSelector(viewId)));
  }

  getActiveGroup(viewId: string): Observable<string> {
    return this.state$.pipe(select(activeGroupSelector(viewId)));
  }

  isActive(viewId: string, linkId: string): Observable<boolean> {
    return this.state$.pipe(select(panelIsActiveSelector(viewId, linkId)));
  }

  isAnchorLinkActiveAndVisible(viewId: string, linkId: string): Observable<boolean> {
    return this.state$.pipe(select(isAnchorLinkActiveAndVisibleSelector(viewId, linkId)));
  }

  /**
   * Used at initialization, try to find a panel group ID in the URL in order to activate the correct anchor
   * Note: the route does not contain a anchor ID but a anchor group ID which means we cannot activate a specific
   * anchor. See registerLinkState.
   *
   * @param router
   * @private
   */
  private extractInitialPanelGroupFromUrl(router: Router): void {
    this.maybeInitialPanelGroup = router.url.split('/').pop();
  }

  changeLinkVisibility(viewId: string, linkId: string, visible: boolean): void {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const newAnchorState = anchorEntityAdapter.updateOne({
          id: linkId,
          changes: { visible },
        }, state.entities[viewId]);
        return anchorsEntityAdapter.updateOne({id: viewId, changes: {...newAnchorState}}, state);
      }),
    ).subscribe(state => this.store.commit(state));
  }

  // We need to clear anchors when switching page because different anchors could be registered with the same view id
  clearView(viewId: string) {
    this.state$.pipe(
      take(1),
      map(state => {
        const newAnchorState = anchorEntityAdapter.removeAll(state.entities[viewId]);
        return anchorsEntityAdapter.updateOne({id: viewId, changes: {...newAnchorState}}, state);
      }),
    ).subscribe((newState) => this.store.commit(newState));
  }
}

export function registerLinkState(viewId: string,
                                  link: LinkState,
                                  state: AnchorsState,
                                  maybeInitialGroupPanel: string): AnchorsState {

  if (! link.id || ! link.group) {
    throw new Error('Each anchor link must have a group ID and a ID defined.');
  }

  let nextState = {...state};
  let anchorState = state.entities[viewId];

  if (!Boolean(anchorState)) {
    logger.debug('Initialize view state ' + viewId);
    anchorState = {
      ...initialAnchorState,
      viewId
    };
  }

  // If current selected link wasn't found yet, compare it to the ID extracted from current route
  if (anchorState.selectedLinkId == null && link.group === maybeInitialGroupPanel) {
    anchorState.selectedLinkId = link.id;
    logger.debug('Found initially selected link', [link]);
  }

  anchorState = anchorEntityAdapter.addOne(link, anchorState);
  nextState = anchorsEntityAdapter.upsertOne(anchorState, nextState);
  return nextState;
}
