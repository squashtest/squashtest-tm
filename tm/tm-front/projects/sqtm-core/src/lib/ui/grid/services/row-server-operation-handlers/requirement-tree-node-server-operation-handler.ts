import {Injectable} from '@angular/core';
import {RestService} from '../../../../core/services/rest.service';
import {DialogService} from '../../../dialog/services/dialog.service';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {ActionErrorDisplayService} from '../../../../core/services/errors-handling/action-error-display.service';
import {DropOperationData, TreeNodeServerOperationHandler} from './tree-node-server-operation-handler';
import {finalize, take} from 'rxjs/operators';
import {getSelectedRows} from '../data-row-loaders/data-row-utils';
import {SquashTmDataRowType} from '../../../../model/grids/data-row.type';
import {RequirementVersionService} from '../../../../core/services/requirement/requirement-version.service';
import {DataRow} from '../../model/data-row.model';


@Injectable()
export class RequirementTreeNodeServerOperationHandler extends TreeNodeServerOperationHandler {

    constructor(protected restService: RestService,
                protected dialogService: DialogService,
                protected referentialDataService: ReferentialDataService,
                protected actionErrorDisplayService: ActionErrorDisplayService,
                private requirementVersionService: RequirementVersionService) {
        super(restService, dialogService, referentialDataService, actionErrorDisplayService);
    }

    notifyInternalDrop(): void {
        this.internalDrop().pipe(
            take(1),
            finalize(() => this.grid.completeAsyncOperation())
        ).subscribe((data: DropOperationData) => {
            this.grid.commit(data.gridState);
            const dataRows = getSelectedRows(data.gridState.dataRowState);
            if (this.singleRowSelectedIsrequirementOrHighLvlReq(dataRows)) {
                this.requirementVersionService.refreshView();
            }
        },
        err => this.handleServerDropError(err)
        );
    }

    private singleRowSelectedIsrequirementOrHighLvlReq(dataRows: DataRow[]): boolean {
        return dataRows.length === 1 &&
        (dataRows[0].type === SquashTmDataRowType.Requirement ||
            dataRows[0].type === SquashTmDataRowType.HighLevelRequirement);
    }

}
