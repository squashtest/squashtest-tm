import {GridState} from '../../model/state/grid.state';
import {Observable} from 'rxjs';
import {GridFilter} from '../../model/state/filter.state';
import {ChangeFilteringAction} from '../../../filters/state/change-filtering.action';
import {ProjectDataMap} from '../../../../model/project/project-data.model';
import {FilterGroup, Scope} from '../../../filters/state/filter.state';

export interface FilterManager {
  changeFilterValue(filterValue: ChangeFilteringAction, state: GridState): Observable<GridState>;

  changeFilterOperation(changeOperation: ChangeFilteringAction, state: GridState): Observable<GridState>;

  addFilters(filters: GridFilter[], state: GridState): GridState;

  replaceFilters(filters: GridFilter[], state: GridState): GridState;

  addFilterGroups(filterGroups: FilterGroup[], state: GridState): GridState;

  replaceFilterGroups(filterGroups: FilterGroup[], state: GridState): GridState;

  applyFilters(state: GridState): Observable<GridState>;

  toggleFilter(id: string, state: Readonly<GridState>): GridState;

  activateFilter(id: string, state: Readonly<GridState>): GridState;

  inactivateFilter(id: string, state: Readonly<GridState>): GridState;

  resetFilter(id: string, state: Readonly<GridState>): GridState;

  resetFilters(ids: string[], state: Readonly<GridState>): GridState;

  resetAllFilters(state: Readonly<GridState>): GridState;

  replaceScope(scope: Scope, projectMap: ProjectDataMap, state: GridState): GridState;

  replaceExtendedScope(state: Readonly<GridState>): GridState

  resetScope(state: GridState): GridState;

  applyMultiColumnsFilter(state: GridState, value: any): Observable<GridState>;

  updateMultiColumnsFilterValue(state: GridState, value: any): GridState;
}
