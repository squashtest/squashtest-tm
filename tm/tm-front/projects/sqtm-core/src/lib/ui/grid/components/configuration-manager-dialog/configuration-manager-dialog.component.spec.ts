import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ConfigurationManagerDialogComponent} from './configuration-manager-dialog.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {GridTestingModule} from '../../grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('ConfigurationManagerDialogComponent', () => {
  let component: ConfigurationManagerDialogComponent;
  let fixture: ComponentFixture<ConfigurationManagerDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule],
      declarations: [ConfigurationManagerDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationManagerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
