import {ColumnDefinitionOption, Sort, WidthCalculationStrategy} from './column-definition.model';
import {Type} from '@angular/core';
import {LevelEnum} from '../../../model/level-enums/level-enum';
import {GridFilter} from './state/filter.state';
import {Identifier} from '../../../model/entity.model';
import {POSITION} from './column-definition.builder';
import {GridViewportName} from './state/column.state';
import {DataRow} from './data-row.model';

export interface ColumnDisplay {
  id: Identifier;
  i18nKey?: string;
  label?: string;
  toolTipText?: string;
  widthCalculationStrategy: WidthCalculationStrategy;
  draggable?: boolean;
  sortable?: boolean;
  associateToFilter?: Identifier;
  resizable?: boolean;
  forceRenderOnNoValue?: boolean;
  sort?: Sort;
  show: boolean;
  cellRenderer?: Type<any>;
  levelEnum?: LevelEnum<any>;
  // levelEnumEditable?: boolean;
  headerRenderer?: Type<any>;
  iconName?: string;
  iconTheme?: string;
  cufId?: number;
  titleI18nKey?: string;
  viewportName: GridViewportName;
  headerPosition: POSITION;
  contentPosition: POSITION;
  editable?: boolean | ((columnDisplay: ColumnDisplay, row: DataRow) => boolean);
  showHeader: boolean;
  options?: ColumnDefinitionOption;
}

export type ColumnWithFilter = ColumnDisplay & { filter?: GridFilter };
