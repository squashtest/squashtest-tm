// import {GridComponent} from './grid.component';
// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
// import {TranslateModule} from '@ngx-translate/core';
// import {GridViewportDirective} from '../../directives/grid-viewport.directive';
// import {NO_ERRORS_SCHEMA} from '@angular/core';
// import {GridService} from '../../services/grid.service';
// import {GridDefinition} from '../../model/grid-definition.model';
// import {RestService} from '../../../../core/services/rest.service';
// import {gridServiceFactory} from '../../grid.service.provider';
// import {grid} from '../../../../model/grids/grid-builders';
// import {OverlayModule} from '@angular/cdk/overlay';
// import {WorkspaceCommonModule} from '../../../workspace-common';
// import {TestingUtilsModule} from '../../../testing-utils';
// import {HttpClientTestingModule} from '@angular/common/http/testing';
// import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
//
// describe('GridComponent', () => {
//   let component: GridComponent;
//   let fixture: ComponentFixture<GridComponent>;
//
//   const gridConfig = grid('grid-test').build();
//   const restService = {};
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [TestingUtilsModule, HttpClientTestingModule, TranslateModule, OverlayModule, WorkspaceCommonModule],
//       declarations: [GridComponent, GridViewportDirective],
//       providers: [
//         {
//           provide: GridDefinition,
//           useValue: gridConfig
//         }, {
//           provide: RestService,
//           useValue: restService
//         }, {
//           provide: GridService,
//           useFactory: gridServiceFactory,
//           deps: [RestService, GridDefinition, ReferentialDataService]
//         },
//       ],
//       schemas: [NO_ERRORS_SCHEMA]
//     })
//       .compileComponents()
//       .then(() => {
//         fixture = TestBed.createComponent(GridComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//       });
//   }));
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
