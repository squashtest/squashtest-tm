import {DraggableListDirective} from './draggable-list.directive';
import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {DragAndDropService} from './drag-and-drop.service';
import {of} from 'rxjs';

@Component({
  selector: 'sqtm-core-drag-and-drop-list-testing',
  template: `
    <div [sqtmCoreDraggableList]="'testing'"></div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DragAndDropListTestingComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

describe('DraggableListDirective', () => {

  let component: DragAndDropListTestingComponent;
  let fixture: ComponentFixture<DragAndDropListTestingComponent>;

  const dndService = jasmine.createSpyObj(['registerList', 'notifyListRemoved']);
  dndService.dragAndDrop$ = of(false);
  dndService.dragData$ = of(null);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DragAndDropListTestingComponent, DraggableListDirective],
      providers: [
        {provide: DragAndDropService, useValue: dndService}
      ]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(DragAndDropListTestingComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));


  it('should create a draggable list and register into dndService', () => {
    expect(dndService.registerList).toHaveBeenCalledTimes(1);
    expect(component).toBeTruthy();
  });
});
