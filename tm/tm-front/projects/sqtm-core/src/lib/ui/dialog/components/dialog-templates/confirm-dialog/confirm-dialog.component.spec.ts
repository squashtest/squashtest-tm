import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ConfirmDialogComponent} from './confirm-dialog.component';
import {DialogReference} from '../../../model/dialog-reference';
import {ConfirmConfiguration} from './confirm-configuration';
import {EMPTY} from 'rxjs';
import {TranslateModule} from '@ngx-translate/core';

describe('ConfirmDialogComponent', () => {
  let component: ConfirmDialogComponent;
  let fixture: ComponentFixture<ConfirmDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference<ConfirmConfiguration, boolean> = new DialogReference<ConfirmConfiguration, boolean>(
    'delete',
    null,
    overlayReference,
    {titleKey: 'titleKey', level: 'DANGER', id: 'delete'},
  );

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmDialogComponent],
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
