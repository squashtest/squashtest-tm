import {AuthenticationProtocol} from '../../../../model/third-party-server/authentication.model';

export interface BugTrackerConnectInfo {
  id: number;
  authProtocol: AuthenticationProtocol;
}

export class BugtrackerConnectConfiguration {
  bugTracker: BugTrackerConnectInfo;
}

