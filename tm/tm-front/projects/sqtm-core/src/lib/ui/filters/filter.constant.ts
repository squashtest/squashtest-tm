import {InjectionToken} from '@angular/core';
import {FilterFieldComponent} from './components/filter-field/filter-field.component';

export const FILTER_FIELD_REF = new InjectionToken<FilterFieldComponent>(
  'Token used to inject th reference to filter field in a Portal Component');


export const USER_HISTORY_PROVIDER = new InjectionToken<FilterFieldComponent>(
  'Token used to inject th reference to filter field in a Portal Component');
