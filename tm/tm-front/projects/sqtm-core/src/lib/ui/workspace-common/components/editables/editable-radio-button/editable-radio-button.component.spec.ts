import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableRadioButtonComponent} from './editable-radio-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';

describe('EditableRadioButtonComponent', () => {
  let component: EditableRadioButtonComponent;
  let fixture: ComponentFixture<EditableRadioButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableRadioButtonComponent],
      imports: [TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableRadioButtonComponent);
    component = fixture.componentInstance;
    component.options = [{label: 'Label', value: 'Value'}, {label: 'Label2', value: 'Value2'}, {
      label: 'Label3',
      value: 'Value3'
    }];
    component.value = 'Value3';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
