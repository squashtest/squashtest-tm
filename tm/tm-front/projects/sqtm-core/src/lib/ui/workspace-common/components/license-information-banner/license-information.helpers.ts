import {LicenseInformation} from '../../../../core/referential/state/license-information.state';
import {TranslateService} from '@ngx-translate/core';

/**
 * A helper class for error messages related to license information.
 * It's not an Angular service because it won't even manage state, it's just here to decode the somewhat
 * cryptic data provided by the server.
 * Note : a lot of work done here may be done server-side... I'm just keeping things like they were in 1.x for now.
 */
export class LicenseInformationMessageProvider {

  public readonly licenseInformation: ReadableLicenseInformation;

  constructor(licenseInformation: LicenseInformation,
              public readonly translateService: TranslateService) {
    this.licenseInformation = readLicenseInformation(licenseInformation);
  }

  getShortMessage(placement: LicenseMessagePlacement, isAdmin: boolean): string {
    const userKind = isAdmin ? 'ADMIN' : 'USER';
    return mergeMessagesWithSeparator(
      this.getShortDateMessage(placement, userKind),
      this.getShortUserMessage(placement, userKind),
      ' - ',
    );
  }

  getLongMessage(placement: LicenseMessagePlacement, isAdmin: boolean): string {
    const userKind = isAdmin ? 'ADMIN' : 'USER';
    return mergeMessagesWithSeparator(
      this.getLongDateMessage(placement, userKind),
      this.getLongUserMessage(placement, userKind),
      '<hr/>',
    );
  }

  private getShortDateMessage(placement: LicenseMessagePlacement, userKind: LicenseDestinationProfile): string {
    const dateMessageKind = this.licenseInformation.dateMessageKind;
    const isVisible = VISIBILITY_RULES[placement]?.[dateMessageKind]?.includes(userKind);
    return isVisible ? this.formatShort(dateMessageKind) : null;
  }

  private getLongDateMessage(placement: LicenseMessagePlacement, userKind: LicenseDestinationProfile): string {
    const dateMessageKind = this.licenseInformation.dateMessageKind;
    const isVisible = VISIBILITY_RULES[placement]?.[dateMessageKind]?.includes(userKind);
    return isVisible ? this.formatLong(dateMessageKind) : null;
  }

  private getShortUserMessage(placement: LicenseMessagePlacement, userKind: LicenseDestinationProfile): string {
    const userMessageKind = this.licenseInformation.userMessageKind;
    const isVisible = VISIBILITY_RULES[placement]?.[userMessageKind]?.includes(userKind);
    return isVisible ? this.formatShort(userMessageKind) : null;
  }

  private getLongUserMessage(placement: LicenseMessagePlacement, userKind: LicenseDestinationProfile): string {
    const userMessageKind = this.licenseInformation.userMessageKind;
    const isVisible = VISIBILITY_RULES[placement]?.[userMessageKind]?.includes(userKind);
    return isVisible ? this.formatLong(userMessageKind) : null;
  }

  private formatShort(messageKind: LicenseMessageKind): string {
    switch (messageKind) {
      case 'DATE_WARNING1':
      case 'DATE_WARNING2':
        return this.translateService.instant(licenseI18nKeys.SHORT_DATE_WARNING);
      case 'DATE_EXPIRED':
        return this.translateService.instant(licenseI18nKeys.SHORT_DATE_EXPIRED);
      case 'USER_WARNING':
        return this.translateService.instant(licenseI18nKeys.SHORT_USER_WARNING);
      case 'USER_EXCESS':
        return this.translateService.instant(licenseI18nKeys.SHORT_USER_EXCESS);
      default:
        return null;
    }
  }

  private formatLong(messageKind: LicenseMessageKind): string {
    const dateParams = {
      0: getExpirationDate(this.licenseInformation.daysToExpiration),
      1: getExpirationDatePlus2Months(this.licenseInformation.daysToExpiration),
    };

    const userParams = {
      0: this.licenseInformation.maxUsersAllowed,
      1: this.licenseInformation.activeUserCount,
    };

    switch (messageKind) {
      case 'DATE_WARNING1':
        return this.translateService.instant(licenseI18nKeys.LONG_DATE_WARNING1, dateParams);
      case 'DATE_WARNING2':
        return this.translateService.instant(licenseI18nKeys.LONG_DATE_WARNING2, dateParams);
      case 'DATE_EXPIRED':
        return this.translateService.instant(licenseI18nKeys.LONG_DATE_EXPIRED, dateParams);
      case 'USER_WARNING':
        return this.translateService.instant(licenseI18nKeys.LONG_USER_WARNING, userParams);
      case 'USER_EXCESS':
        return this.translateService.instant(licenseI18nKeys.LONG_USER_EXCESS, userParams);
      default:
        return null;
    }
  }
}

export const licenseI18nKeys = {
  SHORT_USER_WARNING: 'sqtm-core.license.user-warning.short',
  LONG_USER_WARNING: 'sqtm-core.license.user-warning.long',
  SHORT_USER_EXCESS: 'sqtm-core.license.user-excess.short',
  LONG_USER_EXCESS: 'sqtm-core.license.user-excess.long',

  SHORT_DATE_WARNING: 'sqtm-core.license.date-warning.short',
  LONG_DATE_WARNING1: 'sqtm-core.license.date-warning.first',
  LONG_DATE_WARNING2: 'sqtm-core.license.date-warning.second',

  SHORT_DATE_EXPIRED: 'sqtm-core.license.date-expired.short',
  LONG_DATE_EXPIRED: 'sqtm-core.license.date-expired.long',
};

const NUM_DAYS_FOR_FIRST_WARNING = 61;
const NUM_DAYS_FOR_SECOND_WARNING = 30;

interface ReadableLicenseInformation {
  hasDateMessage: boolean;
  dateMessageKind: LicenseMessageKind;
  daysToExpiration: number;

  hasUserMessage: boolean;
  activeUserCount: number;
  maxUsersAllowed: number;
  allowCreateUsers: boolean;
  userMessageKind: LicenseMessageKind;
}

function readLicenseInformation(raw: LicenseInformation): ReadableLicenseInformation {
  const readable: ReadableLicenseInformation = {
    hasDateMessage: false,
    hasUserMessage: false,
    activeUserCount: null,
    allowCreateUsers: true,
    maxUsersAllowed: null,
    userMessageKind: null,
    dateMessageKind: null,
    daysToExpiration: null,
  };

  if (raw == null) {
    return readable;
  }

  if (raw.activatedUserExcess) {
    const userInfos = raw.activatedUserExcess.split('-');
    readable.activeUserCount = parseInt(userInfos[0], 10);
    readable.maxUsersAllowed = parseInt(userInfos[1], 10);
    const allowCreateUsers = Boolean(JSON.parse(userInfos[2]));
    readable.allowCreateUsers = allowCreateUsers;
    readable.userMessageKind = allowCreateUsers ? 'USER_WARNING' : 'USER_EXCESS';
    readable.hasUserMessage = true;
  }

  if (raw.pluginLicenseExpiration) {
    const daysToExpiration = parseInt(raw.pluginLicenseExpiration, 10);
    readable.daysToExpiration = daysToExpiration;
    readable.dateMessageKind = findDateMessageKind(daysToExpiration);
    readable.hasDateMessage = Boolean(readable.dateMessageKind);
  }

  return readable;
}

function findDateMessageKind(daysToExpiration: number): LicenseMessageKind {
  if (daysToExpiration < 0) {
    return 'DATE_EXPIRED';
  } else if (daysToExpiration < NUM_DAYS_FOR_SECOND_WARNING) {
    return 'DATE_WARNING2';
  } else if (daysToExpiration <= NUM_DAYS_FOR_FIRST_WARNING) {
    return 'DATE_WARNING1';
  } else {
    return null;
  }
}

function getExpirationDate(daysRemaining: number): string {
  const currentDate = new Date();
  const expirationDate = new Date();
  expirationDate.setDate(currentDate.getDate() + daysRemaining);

  return expirationDate.toLocaleDateString(undefined, {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  });
}

function getExpirationDatePlus2Months(daysRemaining: number): string {
  const currentDate = new Date();
  const expirationDate = new Date();
  expirationDate.setDate(currentDate.getDate() + daysRemaining + 61);

  return expirationDate.toLocaleDateString(undefined, {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  });
}

function mergeMessagesWithSeparator(first: string, second: string, separator: string): string {
  if (first && second) {
    return `${first}${separator}${second}`;
  } else if (first) {
    return first;
  } else if (second) {
    return second;
  } else {
    return '';
  }
}

// Where is the license message shown
export enum LicenseMessagePlacement {
  HOME = 'HOME',
  WORKSPACES = 'WORKSPACES',
  ADMIN_WORKSPACE = 'ADMIN_WORKSPACE',
  USER_CREATION = 'USER_CREATION',
  ON_LOGGED_IN = 'ON_LOGGED_IN',
}

// What is the current user's profile
type LicenseDestinationProfile = 'ADMIN' | 'USER';
type LicenseMessageKind = 'USER_WARNING' | 'USER_EXCESS' | 'DATE_WARNING1' | 'DATE_WARNING2' | 'DATE_EXPIRED';

type VisibilityRules = {
  [where in LicenseMessagePlacement]: { [kind in LicenseMessageKind]: LicenseDestinationProfile[]; }
};

const VISIBILITY_RULES: VisibilityRules = {
  HOME: {
    USER_WARNING: ['ADMIN'],
    USER_EXCESS: ['ADMIN', 'USER'],
    DATE_WARNING1: ['ADMIN'],
    DATE_WARNING2: ['ADMIN'],
    DATE_EXPIRED: ['ADMIN', 'USER'],
  },
  WORKSPACES: {
    USER_WARNING: ['ADMIN'],
    USER_EXCESS: ['ADMIN', 'USER'],
    DATE_WARNING1: [],
    DATE_WARNING2: ['ADMIN'],
    DATE_EXPIRED: ['ADMIN', 'USER'],
  },
  ADMIN_WORKSPACE: {
    USER_WARNING: ['ADMIN'],
    USER_EXCESS: ['ADMIN', 'USER'],
    DATE_WARNING1: ['ADMIN'],
    DATE_WARNING2: ['ADMIN'],
    DATE_EXPIRED: ['ADMIN', 'USER'],
  },
  USER_CREATION: {
    USER_WARNING: ['ADMIN', 'USER'],
    USER_EXCESS: ['ADMIN', 'USER'],
    DATE_WARNING1: [],
    DATE_WARNING2: [],
    DATE_EXPIRED: [],
  },
  ON_LOGGED_IN: {
    USER_WARNING: [],
    USER_EXCESS: ['ADMIN'],
    DATE_WARNING1: [],
    DATE_WARNING2: [],
    DATE_EXPIRED: ['ADMIN'],
  },
};
