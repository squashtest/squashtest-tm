import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractCapsuleInformationDirective} from '../abstract-capsule-information';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-capsule-information',
  templateUrl: './capsule-information.component.html',
  styleUrls: ['./capsule-information.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CapsuleInformationComponent extends AbstractCapsuleInformationDirective implements OnInit {

  @Input()
  customIconSize: number = null;

  constructor(translateService: TranslateService) {
    super(translateService);
  }

  ngOnInit(): void {
  }

  getCustomIconSize() {
    if (this.customIconSize != null) {
      return {
        'font-size': `${this.customIconSize.toString()}px`,
      };
    }
  }

}
