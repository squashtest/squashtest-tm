import {Directive, Input} from '@angular/core';
import {throwError} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Directive()
export class AbstractCapsuleInformationDirective {

  _informationData: CapsuleInformationData;

  @Input()
  set informationData(informationData: CapsuleInformationData) {
    this._informationData = informationData;
  }

  get informationData() {
    return this._informationData;
  }

  constructor(public translateService: TranslateService) {
  }

  getTooltipTitle(informationData: CapsuleInformationData) {
    if (Boolean(informationData.title)) {
      return informationData.title;
    } else if (Boolean(informationData.titleI18nKey)) {
      return this.translateService.instant(informationData.titleI18nKey);
    }

  }

  getDisplayValue(informationData: CapsuleInformationData) {
    if (Boolean(informationData.label)) {
      return informationData.label;
    } else if (Boolean(informationData.labelI18nKey)) {
      return this.translateService.instant(informationData.labelI18nKey);
    } else {
      throwError('No value to display');
    }

  }

  getStyle(color: string) {
    if (Boolean(color)) {
      return {border: `1px solid ${color}`};
    } else {
      return {border: `1px solid var(--container-border-color)`};
    }
  }

  getIconColor(color: string) {
    if (Boolean(color)) {
      return color;
    } else {
      return '#666666';
    }
  }

}

export interface CapsuleInformationData {
  id?: string | number;
  label?: string;
  labelI18nKey?: string;
  color?: string;
  title?: string;
  titleI18nKey?: string;
  icon?: string;
}
