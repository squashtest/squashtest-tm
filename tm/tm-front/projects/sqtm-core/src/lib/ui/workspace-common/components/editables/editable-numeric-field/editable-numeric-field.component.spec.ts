import {TestBed, waitForAsync} from '@angular/core/testing';

import {EditableNumericFieldComponent} from './editable-numeric-field.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {TestElement} from 'ngx-speculoos';
import {By} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {OnPushComponentTester} from '../../../../testing-utils/on-push-component-tester';

class EditableNumericFieldComponentTester extends OnPushComponentTester<EditableNumericFieldComponent> {
  constructor() {
    super(EditableNumericFieldComponent);
  }

  get span(): TestElement<any> {
    return this.element('span');
  }

  get errorMessages(): HTMLElement[] {
    return this.fixture.debugElement.queryAll(By.css('span.sqtm-core-error-message')).map(
      (dbgElem) => dbgElem.nativeElement);
  }

  get confirmButton(): HTMLButtonElement {
    const htmlButtonElement = this.getButtons();
    if (htmlButtonElement && htmlButtonElement.length > 0) {
      return htmlButtonElement[0];
    }
    return null;
  }

  get cancelButton(): HTMLButtonElement {
    const htmlButtonElement = this.getButtons();
    if (htmlButtonElement && htmlButtonElement.length > 0) {
      return htmlButtonElement[1];
    }
    return null;
  }

  private getButtons(): HTMLButtonElement[] {
    return this.fixture.nativeElement.querySelectorAll('button');
  }
}

describe('EditableNumericFieldComponent', () => {
  let tester: EditableNumericFieldComponentTester;
  const data = 12;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, FormsModule],
      declarations: [EditableNumericFieldComponent],
      providers: [{
        provide: TranslateService,
        useValue: translateService
      }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
      .then(() => {
        tester = new EditableNumericFieldComponentTester();
        tester.componentInstance.ngOnInit();
        tester.componentInstance.value = data;
        tester.detectChanges();
      });
  }));

  it('should create', (done) => {
    expect(tester.componentInstance).toBeTruthy();
    done();
  });

  it('should display data in no-editing mode', ((done) => {
    const initialSpan = tester.fixture.nativeElement.querySelector('div');
    expect(initialSpan.textContent).toContain(data);
    done();
  }));

  it(`should show error when confirming blank value while being required`, (done) => {
    tester.componentInstance.required = true;
    tester.detectChanges();

    activateEditMode();

    tester.componentInstance.model = '';
    tester.confirmButton.click();
    tester.detectChanges();

    expect(tester.confirmButton).toBeTruthy();
    expect(tester.cancelButton).toBeTruthy();
    expect(tester.errorMessages.length).toEqual(1);

    done();
  });

  function activateEditMode() {
    const span = tester.fixture.debugElement.query(By.css('span'));
    span.triggerEventHandler('click', null);
    tester.detectChanges();
    expect(tester.cancelButton).toBeTruthy();
    expect(tester.confirmButton).toBeTruthy();
  }
});
