// Helper class to build URLs with query strings
export class UrlBuilder {
  private readonly params: UrlParam[] = [];

  constructor(private readonly baseUrl: string) {}

  withParamArray(name: string, array: any[]): this {
    if (Array.isArray(array) && array.length > 0) {
      return this.withStringParam(name, array.join(','));
    }

    return this;
  }

  withStringParam(name: string, value: string): this {
    this.params.push({name, value});
    return this;
  }

  build(): string {
    let url = this.baseUrl;

    if (this.params.length > 0) {
      url += '?';

      this.params.forEach((param, index) => {
        if (index > 0) {
          url += '&';
        }

        url += `${param.name}=${param.value}`;
      });
    }

    return url;
  }
}

interface UrlParam {
  name: string;
  value: string;
}
