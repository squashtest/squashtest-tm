export function isLeftClick(mouseEvent: MouseEvent) {
  return mouseEvent.button === 0;
}
