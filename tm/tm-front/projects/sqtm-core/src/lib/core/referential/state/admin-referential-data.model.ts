import {AuthenticatedUser} from '../../../model/user/authenticated-user.model';
import {GlobalConfiguration} from './global-configuration.state';
import {LicenseInformation} from './license-information.state';
import {CustomField} from '../../../model/customfield/customfield.model';
import {TestAutomationServerKind} from '../../../model/test-automation/test-automation-server.model';
import {TemplateConfigurablePlugin} from '../../../model/plugin/template-configurable-plugin.model';
import {BugTrackerReferentialDto} from '../../../model/bugtracker/bug-tracker.model';
import {ApiDocumentationLink} from './api-documentation-link';

/**
 * Class representing the AdminReferentialData fetched from server.
 */
export class AdminReferentialDataModel {
  user: AuthenticatedUser;
  globalConfiguration: GlobalConfiguration;
  licenseInformation: LicenseInformation;
  customFields: CustomField[];
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  canManageLocalPassword: boolean;
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
  bugTrackers: BugTrackerReferentialDto[];
  documentationLinks: ApiDocumentationLink[];
  callbackUrl: string;
}
