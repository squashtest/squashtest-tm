import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

/**
 * Service that hold the copied things that can pasted in different screens.
 * Aka the test steps that you can copy from a test case to another.
 */
@Injectable({
  providedIn: 'root'
})
export class CopierService {

  _copiedTestStepIds = new BehaviorSubject<number[]>([]);
  copiedTestStep$ = this._copiedTestStepIds.asObservable();

  constructor() {
  }

  notifyCopySteps(stepIds: number[]) {
    this._copiedTestStepIds.next(stepIds);
  }
}
