import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {AutomationRequestStatusKeys} from '../../../model/level-enums/level-enum';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutomationRequestService {

  constructor(private restService: RestService) {
  }

  assignToAutomationRequest(testCaseIds: number[]): Observable<void> {
    return this.restService.post(['automation-requests', testCaseIds.toString(), 'assign']);
  }

  unAssignToAutomationRequest(testCaseIds: number[]): Observable<void> {
    return this.restService.post(['automation-requests', testCaseIds.toString(), 'unassign']);
  }

  updateAutomationRequestStatus(testCaseIds: number[], requestStatus: AutomationRequestStatusKeys) {
    return this.restService.post(['automation-requests', testCaseIds.toString(), 'request-status'], {requestStatus});
  }

  resolveTaScript(testCaseIds: number[]) {
    return this.restService.post(['automation-requests', testCaseIds.toString(), 'associate-TA-script']);
  }

}
