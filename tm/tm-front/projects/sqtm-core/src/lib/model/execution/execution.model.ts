import {DenormalizedCustomFieldValueModel} from './denormalized-custom-field-value.model';
import {EntityModel} from '../../core/services/entity-view/entity-view.state';
import {ExecutionStatusKeys, TestCaseImportanceKeys, TestCaseStatusKeys} from '../level-enums/level-enum';
import {RequirementVersionCoverage} from '../test-case/requirement-version-coverage-model';
import {Milestone} from '../milestone/milestone.model';
import {TestAutomationServerKind} from '../test-automation/test-automation-server.model';

export interface ExecutionModel extends EntityModel {
  id: number;
  name: string;
  executionOrder: number;
  executionStepViews: ExecutionStepModel[];
  prerequisite: string;
  testCaseId?: number;
  tcNatLabel: string;
  tcNatIconName: string;
  tcTypeLabel: string;
  tcTypeIconName: string;
  tcStatus: TestCaseStatusKeys;
  tcImportance: TestCaseImportanceKeys;
  tcDescription: string;
  comment: string;
  datasetLabel?: string;
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  coverages: RequirementVersionCoverage[];
  executionMode: string;
  lastExecutedOn: Date;
  lastExecutedBy: string;
  executionStatus: ExecutionStatusKeys;
  testAutomationServerKind: TestAutomationServerKind;
  automatedExecutionResultUrl: string;
  automatedExecutionResultSummary: string;
  automatedJobUrl: string;
  nbIssues: number;
  iterationId: number;
  kind: ExecutionKind;
  milestones: Milestone[];
  testPlanItemId: number;
  executionsCount: number;
}

export type ExecutionKind = 'STANDARD' | 'GHERKIN' | 'KEYWORD';

export interface ExecutionStepModel extends EntityModel {
  order: number;
  executionStatus: ExecutionStatusKeys;
  action: string;
  expectedResult: string;
  comment: string;
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  lastExecutedOn: Date;
  lastExecutedBy: string;
  testStepId?: number;
}
