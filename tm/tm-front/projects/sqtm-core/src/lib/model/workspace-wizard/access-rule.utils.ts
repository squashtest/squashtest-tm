import {SimplePermissions} from '../permissions/simple-permissions';
import {
  AccessRule,
  isNodeSelectionAccessRule,
  isSelectedNodePermissionAccessRule,
  SelectedNodePermission,
  SelectionMode,
  TreeNodeType
} from './workspace-wizard.model';
import {DataRow} from '../../ui/grid/model/data-row.model';
import {SquashTmDataRowType} from '../grids/data-row.type';
import {Permissions} from '../permissions/permissions.model';

// The wizard plugins API is really flexible regarding access rules. Basically, an AccessRule is an empty interface and each subclass
// has its proper structure which makes it hard to work with in a dynamic language such as JS.
// As a consequence, this helper might not cover every situation. Please implement other cases when you encounter them!
export function checkAccessRulesForGridSelection(accessRule: AccessRule, rows: DataRow[]): boolean {
  if (accessRule == null) {
    return false;
  }

  if (isNodeSelectionAccessRule(accessRule)) {
    const isSelectionModeOk = (accessRule.selectionMode === SelectionMode.SINGLE_SELECTION && rows.length === 1)
      || (accessRule.selectionMode === SelectionMode.MULTIPLE_SELECTION && rows.length > 0);

    return isSelectionModeOk && accessRule.rules.some(rule => {
      if (isSelectedNodePermissionAccessRule(rule)) {
        return checkRowsAgainstSelectedNodePermission(rule, rows);
      }

      // Other access rules are not handled at the moment !
      return false;
    });
  }

  // Other access rules are not handled at the moment !
  return false;
}

function checkRowsAgainstSelectedNodePermission(accessRule: SelectedNodePermission, rows: DataRow[]): boolean {
  return rows.some(row => {
    const nodeTypeOk = checkNodeType(row.type, accessRule.nodeType);
    const permissionOk = checkPermission(row.simplePermissions, accessRule.permission);
    return nodeTypeOk && permissionOk;
  });
}

function checkNodeType(rowType: any, expectedNodeType: TreeNodeType): boolean {
  switch (expectedNodeType) {
    case TreeNodeType.REQUIREMENT:
      return rowType === SquashTmDataRowType.Requirement;
    case TreeNodeType.TEST_CASE:
      return rowType === SquashTmDataRowType.TestCase;
    case TreeNodeType.CAMPAIGN:
      return rowType === SquashTmDataRowType.Campaign;
    case TreeNodeType.ITERATION:
      return rowType === SquashTmDataRowType.Iteration;
    case TreeNodeType.FOLDER:
      return [
        SquashTmDataRowType.RequirementFolder,
        SquashTmDataRowType.TestCaseFolder,
        SquashTmDataRowType.CampaignFolder,
        SquashTmDataRowType.TAFolder,
        SquashTmDataRowType.CustomReportFolder,
      ].includes(rowType);
    case TreeNodeType.LIBRARY:
      return [
        SquashTmDataRowType.RequirementLibrary,
        SquashTmDataRowType.TestCaseLibrary,
        SquashTmDataRowType.CampaignLibrary,
        SquashTmDataRowType.CustomReportLibrary,
        SquashTmDataRowType.ActionWordLibrary,
      ].includes(rowType);
    default:
      return false;
  }
}

function checkPermission(rowPermission: SimplePermissions, expectedPermission: Permissions) {
  switch (expectedPermission) {
    case Permissions.READ:
      return rowPermission.canRead;
    case Permissions.CREATE:
      return rowPermission.canCreate;
    case Permissions.DELETE:
      return rowPermission.canDelete;
    case Permissions.ATTACH:
      return rowPermission.canAttach;
    case Permissions.WRITE:
      return rowPermission.canWrite;
    case Permissions.EXTENDED_DELETE:
      return rowPermission.canExtendedDelete;
    case Permissions.IMPORT:
      return rowPermission.canImport;
    default:
      return false;
  }
}
