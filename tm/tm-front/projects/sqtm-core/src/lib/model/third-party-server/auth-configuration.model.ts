export interface OAuth1aConfiguration {
  implementedProtocol: 'OAUTH_1A';
  type: 'OAUTH_1A';
  consumerKey: string;
  clientSecret: string;
  signatureMethod: SignatureMethod;
  requestTokenHttpMethod: HttpRequestMethod;
  requestTokenUrl: string;
  userAuthorizationUrl: string;
  accessTokenHttpMethod: HttpRequestMethod;
  accessTokenUrl: string;
}

type HttpRequestMethod = 'GET' | 'POST';

export type AuthConfiguration = OAuth1aConfiguration;

export enum SignatureMethod {
  HMAC_SHA1 = 'HMAC_SHA1',
  RSA_SHA1 = 'RSA_SHA1',
}
