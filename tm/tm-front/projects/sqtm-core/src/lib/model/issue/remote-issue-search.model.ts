import {Field} from './remote-issue.model';

// Model received from server to display remote issue search form
export interface RemoteIssueSearchForm {
  fields: Field[];
}

// Model sent to server when looking for a RemoteIssue
export interface RemoteIssueSearchTerms { [searchTermId: string]: any; }
