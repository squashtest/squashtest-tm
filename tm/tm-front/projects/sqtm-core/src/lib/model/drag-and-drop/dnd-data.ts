export abstract class DndData {

  protected constructor(public readonly origin: string) {
  }

  abstract readonly kind: DndDataKind;
}

export const squashDndDataType = 'text/plain';

export const gridDndDataKind: DndDataKind = 'sqtm-core-grid-dnd-data';
export const appDndDataKind: DndDataKind = 'sqtm-app-dnd-data';

export type DndDataKind = 'sqtm-core-grid-dnd-data' | 'sqtm-app-dnd-data';

export function setDragAndDropData($event: DragEvent, data: DndData): void {
  const stringData = JSON.stringify(data);
  $event.dataTransfer.setData(squashDndDataType, stringData);
  $event.dataTransfer.effectAllowed = 'move';
}

export function getDragAndDropData($event: DragEvent): DndData {
  const stringData = $event.dataTransfer.getData(squashDndDataType) || '';
  const obj = JSON.parse(stringData);
  if (isSquashTmDndData(obj)) {
    return obj;
  }
  throw Error('Unable to parse Squash Dnd Data from event');
}

function isSquashTmDndData(obj: any): obj is DndData {
  return obj.kind && (obj.kind === appDndDataKind || obj.kind === gridDndDataKind)
    && obj.origin && typeof obj.origin === 'string';
}
