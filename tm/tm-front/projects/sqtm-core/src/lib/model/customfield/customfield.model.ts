import {CustomFieldOption} from './custom-field-option.model';
import {InputType} from './input-type.model';

export class CustomField {
  id: number;
  code: string;
  label: string;
  name: string;
  defaultValue?: string;
  largeDefaultValue?: string;
  numericDefaultValue?: number;
  inputType: InputType;
  optional: boolean;
  options: CustomFieldOption[] = [];
}

export class CustomFieldData extends CustomField {
  cfvId?: number;
  value: string | string[];
}

// Types used in creation process of entities that hold CustomFieldValues
// Here P is the CUF id, not the CFV id as the CFV doesn't exist at this stage (entity is not created yet)
export interface RawValueMap {
  [P: number]: string | string[];
}

export function isCUFCodePatternValid(code: string) {
  return ((!code.match(/\s+/)) && code
    .match(/^[A-Za-z0-9_^;]*$/));
}
