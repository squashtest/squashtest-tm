import {Credentials} from '../third-party-server/credentials.model';
import {AuthConfiguration} from '../third-party-server/auth-configuration.model';
import {AuthenticationPolicy, AuthenticationProtocol} from '../third-party-server/authentication.model';
import {SqtmGenericEntityState} from '../../core/services/genric-entity-view/generic-entity-view-state';

export interface AdminBugTrackerState extends SqtmGenericEntityState {
  id: number;
  name: string;
  url: string;
  kind: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  iframeFriendly: boolean;
  bugTrackerKinds: string[];
  supportedAuthenticationProtocols: string[];
  authConfiguration?: AuthConfiguration;
  credentials?: Credentials;
}
