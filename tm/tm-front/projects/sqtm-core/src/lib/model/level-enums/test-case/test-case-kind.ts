import {I18nEnum} from '../level-enum';
import {DisplayOption} from '../../../ui/workspace-common/components/forms/display-option';

export type TestCaseKindKeys = 'STANDARD' | 'GHERKIN' | 'KEYWORD';
export const TestCaseKind: I18nEnum<TestCaseKindKeys> = {
  STANDARD: {id: 'STANDARD', i18nKey: 'sqtm-core.entity.test-case.kind.standard'},
  GHERKIN: {id: 'GHERKIN', i18nKey: 'sqtm-core.entity.test-case.kind.gherkin'},
  KEYWORD: {id: 'KEYWORD', i18nKey: 'sqtm-core.entity.test-case.kind.keyword'}
};

export type TestCaseClassesKeys =
  'org.squashtest.tm.domain.testcase.TestCase'
  | 'org.squashtest.tm.domain.testcase.ScriptedTestCase'
  | 'org.squashtest.tm.domain.testcase.KeywordTestCase';
export const TestCaseClasses: I18nEnum<TestCaseClassesKeys> = {
  'org.squashtest.tm.domain.testcase.TestCase': {
    id: 'org.squashtest.tm.domain.testcase.TestCase',
    i18nKey: 'sqtm-core.entity.test-case.kind.standard'
  },
  'org.squashtest.tm.domain.testcase.ScriptedTestCase': {
    id: 'org.squashtest.tm.domain.testcase.ScriptedTestCase',
    i18nKey: 'sqtm-core.entity.test-case.kind.gherkin'
  },
  'org.squashtest.tm.domain.testcase.KeywordTestCase': {
    id: 'org.squashtest.tm.domain.testcase.KeywordTestCase',
    i18nKey: 'sqtm-core.entity.test-case.kind.keyword'
  }
};

export function i18NEnumToOptions(levelEnum: I18nEnum<any>): DisplayOption[] {
  const items = Object.values(levelEnum);
  return items.map(i => ({
    label: i.i18nKey,
    id: i.id
  }));
}
