/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.security.authentication.customauth

import spock.lang.Specification

class CustomAuthTokenGeneratorTest extends Specification {
	CustomAuthTokenGenerator<TestPayload> service = new CustomAuthTokenGenerator<TestPayload>("toto")

	def "should generate tokens"() {
		when:
		def payload = new TestPayload("toto", "a4091801-9ce5-4912-bb10-54dd4ec75ae5")
		def token = service.generateToken(payload)

		then:
		token == "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InRvdG8iLCJzdWl0ZUlkIjoiYTQwOTE4MDEtOWNlNS00OTEyLWJiMTAtNTRkZDRlYzc1YWU1In0.-vK-rbH-xDCeXLe121tpejF-A-RQ40OQbNu4vP4Evpg"
	}

	def "should validate JWT tokens"() {
		when:
		def isValid = service.isValidToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InRvdG8iLCJzdWl0ZUlkIjoiYTQwOTE4MDEtOWNlNS00OTEyLWJiMTAtNTRkZDRlYzc1YWU1In0.-vK-rbH-xDCeXLe121tpejF-A-RQ40OQbNu4vP4Evpg")

		then:
		isValid
	}

	def "should reject invalid JWT tokens"() {
		when:
		service.isValidToken("totoeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InRvdG8iLCJzdWl0ZUlkIjoiYTQwOTE4MDEtOWNlNS00OTEyLWJiMTAtNTRkZDRlYzc1YWU1In0.-vK-rbH-xDCeXLe121tpejF-A-RQ40OQbNu4vP4Evpg")

		then:
		thrown InvalidCustomAuthTokenException
	}

	static class TestPayload {
		String login
		String suiteId

		TestPayload(String login, String suiteId) {
			this.login = login
			this.suiteId = suiteId
		}

	}
}
