/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.security.authentication.customauth;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Generates a JWT token used for custom authentication in the automated test result publication workflow.
 * @param <T> the payload object, should be fully serializable
 * @see org.squashtest.tm.service.testautomation.model.SquashAutomTokenPayload for an example payload
 */
public class CustomAuthTokenGenerator<T> {
	private static final String JWT_HEADER = "{\"alg\":\"HS256\",\"typ\":\"JWT\"}";

	private final ObjectMapper objectMapper;

	private final String secret;

	public CustomAuthTokenGenerator(@NotBlank String secret) {
		if (StringUtils.isEmpty(secret)) {
			throw new IllegalArgumentException("Crypto secret must be configured.");
		}

		this.secret = secret;
		this.objectMapper = new ObjectMapper();
	}

	public String generateToken(T payload) throws IOException {
		final String jsonPayload = objectMapper.writeValueAsString(payload);
		final String signature = hmacSha256(encBase64(JWT_HEADER) + "." + encBase64(jsonPayload), secret);
		return encBase64(JWT_HEADER) + "." + encBase64(jsonPayload) + "." + signature;
	}

	public boolean isValidToken(String token) {
		final String[] parts = token.split("\\.");
		final String header = decBase64(parts[0]);
		checkHeader(header);
		checkSignature(parts[0], parts[1], parts[2]);
		return true;
	}

	private void checkHeader(String header) {
		if (!JWT_HEADER.equals(header)) {
			throw new InvalidCustomAuthTokenException("Invalid header.");
		}
	}

	private void checkSignature(String header, String payload, String signature) {
		String headerAndPayloadHashed = hmacSha256(header + "." + payload, secret);

		if (headerAndPayloadHashed == null || !headerAndPayloadHashed.equals(signature)) {
			throw new InvalidCustomAuthTokenException("Invalid signature.");
		}
	}

	private static String hmacSha256(String data, String secret) {
		try {
			byte[] hash = secret.getBytes(StandardCharsets.UTF_8);
			Mac sha256Hmac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKey = new SecretKeySpec(hash, "HmacSHA256");
			sha256Hmac.init(secretKey);
			byte[] signedBytes = sha256Hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));
			return encBase64(signedBytes);
		} catch (NoSuchAlgorithmException | InvalidKeyException ex) {
			throw new InvalidCustomAuthTokenException("Error while encoding data.", ex);
		}
	}

	private static String encBase64(String input) {
		return encBase64(input.getBytes());
	}

	private static String decBase64(String input) {
		return decBase64(input.getBytes());
	}

	private static String encBase64(byte[] bytes) {
		return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
	}

	private static String decBase64(byte[] bytes) {
		return new String(Base64.getUrlDecoder().decode(bytes));
	}
}
