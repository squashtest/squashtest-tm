/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export;

import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.domain.requirement.ExportRequirementData;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.report.service.JasperReportsService;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/backend/requirement/export")
public class RequirementExportController {
	private static final String JASPER_EXPORT_FILE = "WEB-INF/reports/requirement-export.jasper";

	private final MessageSource messageSource;

	private final JasperReportsService jrServices;

	private final RequirementLibraryNavigationService requirementLibraryNavigationService;

	public RequirementExportController(MessageSource messageSource,
									   JasperReportsService jrServices, RequirementLibraryNavigationService requirementLibraryNavigationService) {
		this.messageSource = messageSource;
		this.jrServices = jrServices;
		this.requirementLibraryNavigationService = requirementLibraryNavigationService;
	}

	@RequestMapping(value = "searchExports", produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM, method = RequestMethod.GET,
		params = {RequestParams.FILENAME, RequestParams.NODES, RequestParams.RTEFORMAT})
	@ResponseBody
	public FileSystemResource searchExportAsExcel(@RequestParam(RequestParams.FILENAME) String filename,
												  @RequestParam(RequestParams.NODES) List<Long> nodeIds,
												  @RequestParam(RequestParams.TYPE) String type,
												  @RequestParam(RequestParams.RTEFORMAT) Boolean keepRteFormat,
												  HttpServletResponse response) throws FileNotFoundException {

		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".xls");

		File export = requirementLibraryNavigationService.searchExportRequirementAsExcel(nodeIds, keepRteFormat,
			messageSource, type);

		return new FileSystemResource(export);
	}

	@RequestMapping(value = "/content/xls", produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM, method = RequestMethod.GET, params = {
		RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES, RequestParams.RTEFORMAT})
	@ResponseBody
	public FileSystemResource exportAsExcel(@RequestParam(RequestParams.FILENAME) String fileName,
											@RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds,
											@RequestParam(RequestParams.NODES) List<Long> nodeIds,
											@RequestParam(RequestParams.RTEFORMAT) Boolean keepRtxFormat,
											HttpServletResponse response) {
		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + fileName + ".xls");

		File export = requirementLibraryNavigationService.exportRequirementAsExcel(libraryIds, nodeIds, keepRtxFormat, messageSource);
		return new FileSystemResource(export);
	}

	@RequestMapping(value = "/content/csv", produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM, method = RequestMethod.GET, params = {
		RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES, RequestParams.RTEFORMAT, RequestParams.IS_CURRENT_VERSION})
	@ResponseBody
	public void exportAsCsv(@RequestParam(RequestParams.FILENAME) String filename,
							@RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds,
							@RequestParam(RequestParams.NODES) List<Long> nodeIds,
							@RequestParam(RequestParams.RTEFORMAT) Boolean keepRteFormat,
							//only true when the export is called from the Requirement Search page with 'last version' options checked
							@RequestParam(RequestParams.IS_CURRENT_VERSION) Boolean isCurrentVersion,
							Locale locale,
							HttpServletResponse response) {
		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".xls");

		List<ExportRequirementData> dataSource = new ArrayList<>();

		if (!libraryIds.isEmpty()) {
			dataSource.addAll(requirementLibraryNavigationService.findRequirementsToExportFromLibrary(libraryIds));
		}

		if (!nodeIds.isEmpty()) {
			dataSource.addAll(requirementLibraryNavigationService.findRequirementsToExportFromNodes(nodeIds));
		}

		convertHtmlSpecialCharactersToUnicode(dataSource);

		printExport(dataSource, filename, response, locale, keepRteFormat);
	}

	private void printExport(List<ExportRequirementData> dataSource, String filename, HttpServletResponse response, Locale locale, Boolean keepRteFormat) {
		ExportUtil.printExport(dataSource, filename, RequirementExportController.JASPER_EXPORT_FILE, response, locale, "csv", keepRteFormat,
			new HashMap<>(), messageSource, jrServices);
	}

	private void convertHtmlSpecialCharactersToUnicode(List<ExportRequirementData> dataSource) {
		for (ExportRequirementData data : dataSource) {
			data.setDescription(HtmlUtils.htmlUnescape(data.getDescription()));
		}
	}

}
