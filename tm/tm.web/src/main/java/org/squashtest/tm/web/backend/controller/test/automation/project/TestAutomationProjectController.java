/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.project;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.exception.DomainException;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.web.backend.model.testautomation.TAUsageStatus;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/test-automation-projects")
public class TestAutomationProjectController {

	@Inject
	private TestAutomationProjectManagerService service;

	private static final Logger LOGGER = LoggerFactory.getLogger(TestAutomationProjectController.class);

	private static final String PROJECT_ID = "/{projectId}";
	private static final String PROJECT_IDS = "/{projectIds}";

	@RequestMapping(value = PROJECT_ID, method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTestAutomationProject(@PathVariable long projectId) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Delete test automation project of id #{}", projectId);
		}
		service.deleteProject(projectId);
	}

	@RequestMapping(value = PROJECT_ID, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, List<TestAutomationProjectDto>> editTestAutomationProject(@PathVariable long projectId,
										  @RequestBody TestAutomationProjectController.Patch patch) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Edit test automation project of id #{}", projectId);
		}
		try {
			TestAutomationProject testAutomationProject = new TestAutomationProject();
			testAutomationProject.setCanRunGherkin(patch.isCanRunBdd());
			testAutomationProject.setJobName(patch.getRemoteName());
			testAutomationProject.setLabel(patch.getLabel());
			testAutomationProject.setSlaves(patch.getExecutionEnvironment());
			service.editProject(projectId, testAutomationProject);

			final Long tmProjectId = service.findProjectById(projectId).getTmProject().getId();
			return Collections.singletonMap("taProjects", service.findAllByTMProject(tmProjectId));
		} catch (DomainException de) {
			de.setObjectName("ta-project");
			throw de;
		}
	}

	@RequestMapping(value = PROJECT_IDS + "/usage-status", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, TAUsageStatus> getTestAutomationUsageStatus(@PathVariable List<Long> projectIds) {
		final boolean hasExecutedTests = projectIds.stream().anyMatch(id -> service.hasExecutedTests(id));
		return Collections.singletonMap("usageStatus", new TAUsageStatus(hasExecutedTests));
	}

	@PostMapping(value = PROJECT_ID + "/label")
	@ResponseBody
	public Map<String, List<TestAutomationProjectDto>> changeLabel(@PathVariable long projectId,
							@RequestBody TestAutomationProjectController.Patch patch) {
		if (patch.getLabel() == null || patch.getLabel().trim().isEmpty()) {
			throw new RequiredFieldException("label");
		}

		try {
			service.changeLabel(projectId, patch.getLabel().trim());
			final Long tmProjectId = service.findProjectById(projectId).getTmProject().getId();
			return Collections.singletonMap("taProjects", service.findAllByTMProject(tmProjectId));
		} catch (DomainException de) {
			de.setObjectName("ta-project");
			throw de;
		}
	}

	@PostMapping(value = PROJECT_ID + "/can-run-bdd")
	@ResponseBody
	public Map<String, List<TestAutomationProjectDto>> changeCanRunBdd(@PathVariable long projectId,
																	   @RequestBody TestAutomationProjectController.Patch patch) {
		try {
			service.changeCanRunGherkin(projectId, patch.isCanRunBdd());

			final Long tmProjectId = service.findProjectById(projectId).getTmProject().getId();
			return Collections.singletonMap("taProjects", service.findAllByTMProject(tmProjectId));
		} catch (DomainException de) {
			de.setObjectName("ta-project");
			throw de;
		}
	}

	static class Patch {
		String label;
		boolean canRunBdd;
		Long taProjectId;
		String remoteName;
		String executionEnvironment;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public boolean isCanRunBdd() {
			return canRunBdd;
		}

		public void setCanRunBdd(boolean canRunBdd) {
			this.canRunBdd = canRunBdd;
		}

		public Long getTaProjectId() { return taProjectId; }

		public void setTaProjectId(Long taProjectId) { this.taProjectId = taProjectId; }

		public String getExecutionEnvironment() { return executionEnvironment; }

		public void setExecutionEnvironment(String executionEnvironment) { this.executionEnvironment = executionEnvironment; }

		public String getRemoteName() {
			return remoteName;
		}

		public void setRemoteName(String remoteName) {
			this.remoteName = remoteName;
		}
	}
}
