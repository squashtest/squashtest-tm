/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EntityReferences {

	private Set<EntityReference> entityReferences;

	public EntityReferences(Set<EntityReference> entityReferences) {
		this.entityReferences = entityReferences;
	}
	public EntityReferences(List<EntityReference> entityReferences) {
		this(new HashSet<>(entityReferences));
	}

	public Set<EntityReference> asSet() {
		return this.entityReferences;
	}

	private Stream<EntityReference> asStream() {
		return entityReferences.stream();
	}

	private static Collector<NodeReference, Set<Long>, Set<Long>> toIdSet() {
		return Collector.of(
			HashSet::new,
			(ids, nodeReference) -> ids.add(nodeReference.getId()),
			(a, b) -> {
				throw new RuntimeException("Not concurrent collector");
			}
		);
	}

	public boolean isEmpty() {
		return this.entityReferences.isEmpty();
	}

	public NodeReferences toNodeReferences() {
		return new NodeReferences(this.asStream().map(EntityReference::toNodeReference).collect(Collectors.toSet()));
	}
}
