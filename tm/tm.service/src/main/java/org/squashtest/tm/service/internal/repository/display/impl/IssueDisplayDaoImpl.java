/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectHavingStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.service.internal.repository.display.IssueDisplayDao;

import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.RequirementVersion.REQUIREMENT_VERSION;

@Repository
public class IssueDisplayDaoImpl implements IssueDisplayDao {

	private final DSLContext dsl;

	public IssueDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public Integer countIssuesByRequirementVersionId(Long requirementVersionId) {
		Requirement requirementChild = REQUIREMENT.as("r2");
		RequirementVersion requirementVersionChild = REQUIREMENT_VERSION.as("rv2");

		return baseRequest()
			.where(EXECUTION.TCLN_ID.in(
				DSL.select(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID)
					.from(REQUIREMENT_VERSION)
					.innerJoin(REQUIREMENT).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
					.innerJoin(RLN_RELATIONSHIP_CLOSURE).on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
					.innerJoin(requirementChild).on(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(requirementChild.RLN_ID))
					.innerJoin(requirementVersionChild).on(requirementChild.CURRENT_VERSION_ID.eq(requirementVersionChild.RES_ID))
					.innerJoin(REQUIREMENT_VERSION_COVERAGE).on(requirementVersionChild.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
					.where(REQUIREMENT_VERSION.RES_ID.eq(requirementVersionId))))
			.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.fetchOne(DSL.count());
	}

	@Override
	public Integer countIssuesByHighLvlRequirementVersionId(Long highLvlRequirementVersionId) {
		Requirement linkedStandardRequirement = REQUIREMENT.as("r2");
		RequirementVersion linkedStandardRequirementVersion = REQUIREMENT_VERSION.as("rv2");



		return baseRequest()
				.where(EXECUTION.TCLN_ID.in(
						DSL.selectDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID)
								.from(REQUIREMENT_VERSION)
								.innerJoin(REQUIREMENT).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
								.innerJoin(linkedStandardRequirement)
									.on(REQUIREMENT.RLN_ID.eq(linkedStandardRequirement.HIGH_LEVEL_REQUIREMENT_ID)
									.or(REQUIREMENT.RLN_ID.eq(linkedStandardRequirement.RLN_ID)))
								.innerJoin(linkedStandardRequirementVersion).on(linkedStandardRequirement.RLN_ID.eq(linkedStandardRequirementVersion.REQUIREMENT_ID))
								.innerJoin(REQUIREMENT_VERSION_COVERAGE).on(linkedStandardRequirementVersion.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
								.where(REQUIREMENT_VERSION.RES_ID.eq(highLvlRequirementVersionId))))
				.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
				.fetchOne(DSL.count());
	}

	@Override
	public int countIssuesByCampaignId(Long campaignId) {
		return baseRequest()
			.where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(campaignId))
			.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.groupBy(CAMPAIGN_LIBRARY_NODE.CLN_ID)
			.fetchOne(0, int.class);
	}

	@Override
	public int countIssuesByIterationId(Long iterationId) {
		return baseRequest()
			.where(ITERATION.ITERATION_ID.eq(iterationId))
			.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.groupBy(ITERATION.ITERATION_ID)
			.fetchOne(0, int.class);
	}

	@Override
	public int countIssuesByTestSuiteId(Long testSuiteId) {
		return baseRequest()
			.innerJoin(TEST_SUITE_TEST_PLAN_ITEM).on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
			.where(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(testSuiteId))
			.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.groupBy(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID)
			.fetchOne(0, int.class);
	}

	@Override
	public int countIssuesByCampaignFolderId(Long folderId) {

		SelectHavingStep<?> getCampaigns = dsl.select(CAMPAIGN.CLN_ID)
			.from(CAMPAIGN)
			.where(CAMPAIGN.CLN_ID.in(
				dsl.select(CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID)
					.from(CLN_RELATIONSHIP_CLOSURE)
					.where(CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(folderId))));

		return dsl.selectCount()
			.from(EXECUTION_ISSUES_CLOSURE)
			.innerJoin(ISSUE).on(ISSUE.ISSUE_ID.eq(EXECUTION_ISSUES_CLOSURE.ISSUE_ID))
			.innerJoin(EXECUTION).on(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
			.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(getCampaigns).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(getCampaigns.field("CLN_ID", Long.class)))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(getCampaigns.field("CLN_ID", Long.class).eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.innerJoin(PROJECT).on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(BUGTRACKER_BINDING).on(BUGTRACKER_BINDING.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.where(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.fetchOne(0, int.class);

	}

	@Override
	public int countIssuesByExecutionId(Long executionId) {
		return baseRequest()
			.where(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(executionId))
			.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.groupBy(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID)
			.fetchOne(0, int.class);
	}

	@Override
	public int countIssuesByTestCaseId(Long testCaseId) {
		return baseRequest()
			.where(EXECUTION.TCLN_ID.eq(testCaseId).and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID)))
			.fetchOne(0, int.class);
	}

	private SelectOnConditionStep<Record1<Integer>> baseRequest() {
		return dsl.selectCount()
			.from(EXECUTION)
			.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.innerJoin(PROJECT).on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(BUGTRACKER_BINDING).on(BUGTRACKER_BINDING.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(EXECUTION_ISSUES_CLOSURE).on(EXECUTION.EXECUTION_ID.eq(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID))
			.innerJoin(ISSUE).on(ISSUE.ISSUE_ID.eq(EXECUTION_ISSUES_CLOSURE.ISSUE_ID));
	}
}
