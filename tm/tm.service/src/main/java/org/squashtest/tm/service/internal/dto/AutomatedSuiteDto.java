/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto;

import org.squashtest.tm.domain.execution.ExecutionStatus;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AutomatedSuiteDto {

	private Date createdOn;
	private String createdBy;
	private ExecutionStatus executionStatus;
	private Date lastModifiedOn;
	private String suiteId;
	private String lastModifiedBy;
	private boolean hasExecutions;
	private long attachmentListId;
	private List<AttachmentDTO> attachmentList;
	private Set<String> urlList = new HashSet<>();

	public Set<String> getUrlList() {
		return urlList;
	}

	public void setUrlList(Set<String> urlList) {
		this.urlList = urlList;
	}

	public List<AttachmentDTO> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<AttachmentDTO> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public long getAttachmentListId() {
		return attachmentListId;
	}

	public void setAttachmentListId(long attachmentListId) {
		this.attachmentListId = attachmentListId;
	}

	public boolean isHasExecutions() {
		return hasExecutions;
	}

	public void setHasExecutions(boolean hasExecutions) {
		this.hasExecutions = hasExecutions;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public ExecutionStatus getExecutionStatus() {
		return executionStatus;
	}

	public void setExecutionStatus(ExecutionStatus executionStatus) {
		this.executionStatus = executionStatus;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(String suiteId) {
		this.suiteId = suiteId;
	}
}

