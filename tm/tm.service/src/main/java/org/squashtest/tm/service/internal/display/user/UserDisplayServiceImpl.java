/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.user;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.project.ProjectPermission;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.domain.users.UsersGroup;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewTeamDto;
import org.squashtest.tm.service.internal.display.dto.UsersGroupDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.UserGrid;
import org.squashtest.tm.service.internal.dto.DetailedUserDto;
import org.squashtest.tm.service.internal.repository.CustomTeamDao;
import org.squashtest.tm.service.internal.repository.CustomUserDao;
import org.squashtest.tm.service.internal.repository.display.UserDisplayDao;
import org.squashtest.tm.service.internal.security.AuthenticationProviderContext;
import org.squashtest.tm.service.project.ProjectsPermissionFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.user.UserAdministrationService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service
@Transactional
public class UserDisplayServiceImpl implements UserDisplayService {

	private final DSLContext dsl;
	private final CustomUserDao userDao;
	private final UserDisplayDao userDisplayDao;
	private final UserAdministrationService adminService;
	private final ProjectsPermissionFinder permissionFinder;
	private final PermissionEvaluationService permissionEvaluationService;
	private final CustomTeamDao teamDao;
	private final AuthenticationProviderContext authenticationProviderContext;

	@Inject
	public UserDisplayServiceImpl(DSLContext dsl,
								  CustomUserDao userDao,
								  UserDisplayDao userDisplayDao,
								  UserAdministrationService adminService,
								  ProjectsPermissionFinder permissionFinder,
								  PermissionEvaluationService permissionEvaluationService,
								  CustomTeamDao teamDao,
								  AuthenticationProviderContext authenticationProviderContext) {
		this.dsl = dsl;
		this.userDao = userDao;
		this.userDisplayDao = userDisplayDao;
		this.adminService = adminService;
		this.permissionFinder = permissionFinder;
		this.permissionEvaluationService = permissionEvaluationService;
		this.teamDao = teamDao;
		this.authenticationProviderContext = authenticationProviderContext;
	}

	@Override
	public DetailedUserDto findCurrentUser() {
		DetailedUserDto detailedUserDto = fetchBasicUser();
		appendRoles(detailedUserDto);
		appendTeams(detailedUserDto);
		return detailedUserDto;
	}

	private void appendTeams(DetailedUserDto detailedUserDto) {
		List<Long> teamIds = teamDao.findTeamIds(detailedUserDto.getUserId());
		detailedUserDto.setTeamIds(teamIds);
	}

	private void appendRoles(DetailedUserDto detailedUserDto) {
		detailedUserDto.setAdmin(permissionEvaluationService.hasRole(Roles.ROLE_ADMIN));
		detailedUserDto.setProjectManager(permissionEvaluationService.hasRole(Roles.ROLE_TM_PROJECT_MANAGER));
		detailedUserDto.setFunctionalTester(permissionEvaluationService.hasRole(Roles.ROLE_TF_FUNCTIONAL_TESTER) || detailedUserDto.isProjectManager());
		detailedUserDto.setAutomationProgrammer(permissionEvaluationService.hasRole(Roles.ROLE_TF_AUTOMATION_PROGRAMMER) || detailedUserDto.isProjectManager());
	}

	private DetailedUserDto fetchBasicUser() {
		String username = UserContextHolder.getUsername();

		DetailedUserDto detailedUserDto = dsl
			.select(CORE_USER.PARTY_ID.as("USER_ID"), CORE_USER.FIRST_NAME, CORE_USER.LAST_NAME)
			.from(CORE_USER)
			.where(CORE_USER.LOGIN.eq(username))
			.fetchOneInto(DetailedUserDto.class);

		detailedUserDto.setUsername(username);

		return detailedUserDto;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public GridResponse findAll(GridRequest request) {
		UserGrid userGrid = new UserGrid();
		return userGrid.getRows(request, dsl);
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public UserAdminViewDto getUserView(long userId) {
		UserAdminViewDto user = userDisplayDao.getUserById(userId);
		appendUsersGroups(user);
		user.setProjectPermissions(getProjectPermissions(userId));
		user.setTeams(userDisplayDao.getTeamsByUser(userId));
		user.setCanManageLocalPassword(authenticationProviderContext.isInternalProviderEnabled());

		return user;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<ProjectPermissionDto> getProjectPermissions(Long userId) {
		List<ProjectPermission> projectPermissions = permissionFinder.findProjectPermissionByParty(userId);

		return projectPermissions.stream().map(projectPermission -> {
			ProjectPermissionDto dto = new ProjectPermissionDto();
			dto.setProjectId(projectPermission.getProject().getId());
			dto.setProjectName(projectPermission.getProject().getName());
			dto.setPermissionGroup(projectPermission.getPermissionGroup());
			return dto;
		}).collect(Collectors.toList());
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<UserAdminViewTeamDto> getAssociatedTeams(Long userId) {
		return userDisplayDao.getTeamsByUser(userId);
	}

	@Override
	public List<NamedReference> getUsersWhoCanAccessProjects(List<Long> projectIds) {
		List<Long> partyIds = userDisplayDao.findPartyIdsCanAccessProject(projectIds);
		List<User> allAdmins = userDao.findAllAdmin();
		Set<Long> allIds = allAdmins.stream().map(Party::getId).collect(Collectors.toSet());
		allIds.addAll(partyIds);
		return userDisplayDao.findUserLoginsByPartyIds(new ArrayList<>(allIds));
	}

	private void appendUsersGroups(UserAdminViewDto user) {
		List<UsersGroup> usersGroups = adminService.findAllUsersGroupOrderedByQualifiedName();
		List<UsersGroupDto> usersGroupDto = new ArrayList<>();
		usersGroups.forEach(group -> {
			UsersGroupDto groupDto = new UsersGroupDto();
			groupDto.setId(group.getId());
			groupDto.setQualifiedName(group.getQualifiedName());
			usersGroupDto.add(groupDto);
		});

		user.setUsersGroups(usersGroupDto);
	}

}
