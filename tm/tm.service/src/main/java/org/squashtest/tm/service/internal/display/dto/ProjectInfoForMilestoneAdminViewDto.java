/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;


public class ProjectInfoForMilestoneAdminViewDto {

	private Long projectId;
	private String projectName;
	private boolean template;
	private boolean isBoundToMilestone;
	private boolean isMilestoneBoundToOneObjectOfProject;

	public Long getProjectId() { return projectId; }

	public void setProjectId(Long projectId) { this.projectId = projectId; }

	public String getProjectName() { return projectName; }

	public void setProjectName(String projectName) { this.projectName = projectName; }

	public boolean isTemplate() { return template; }

	public void setTemplate(boolean template) { this.template = template; }

	public boolean isBoundToMilestone() { return isBoundToMilestone; }

	public void setBoundToMilestone(boolean boundToMilestone) { isBoundToMilestone = boundToMilestone; }

	public boolean isMilestoneBoundToOneObjectOfProject() {	return isMilestoneBoundToOneObjectOfProject; }

	public void setMilestoneBoundToOneObjectOfProject(boolean milestoneBoundToOneObjectOfProject) {	isMilestoneBoundToOneObjectOfProject = milestoneBoundToOneObjectOfProject; }
}
