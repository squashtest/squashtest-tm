/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.jooq.domain.tables.Attachment;
import org.squashtest.tm.jooq.domain.tables.AttachmentList;
import org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender;
import org.squashtest.tm.service.display.campaign.AutomatedSuiteDisplayService;
import org.squashtest.tm.service.internal.display.dto.campaign.AutomatedSuiteExecutionReportDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.AutomatedSuiteExecutionGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.IterationAutomatedTestSuiteGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.TestSuiteAutomatedTestSuiteGrid;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;

@Service
@Transactional(readOnly = true)
public class AutomatedSuiteDisplayServiceImpl implements AutomatedSuiteDisplayService {

	private static final String AUTOMATED_SUITE_ATTACHMENT = "automatedSuiteAttachment";
	private static final String AUTOMATED_SUITE_ATTACHMENT_LIST = "automatedSuiteAttachmentList";
	private static final String AUTOMATED_SUITE_HAS_ATTACHMENT = "AUTOMATED_SUITE_HAS_ATTACHMENT";
	private static final String EXECUTION_ATTACHMENT = "executionAttachment";
	private static final String EXECUTION_ATTACHMENT_LIST = "executionAttachmentList";
	private static final String EXECUTION_EXTENDER_HAS_ATTACHMENT = "EXECUTION_EXTENDER_HAS_ATTACHMENT";
	private static final String HAS_EXECUTION = "HAS_EXECUTION";
	private static final String HAS_RESULT_URL = "HAS_RESULT_URL";

	private DSLContext dslContext;

	private AutomatedSuiteDisplayDao automatedSuiteDisplayDao;

	private AttachmentDisplayDao attachmentDisplayDao;

	public AutomatedSuiteDisplayServiceImpl(DSLContext dslContext,
											AutomatedSuiteDisplayDao automatedSuiteDisplayDao,
											AttachmentDisplayDao attachmentDisplayDao) {
		this.dslContext = dslContext;
		this.automatedSuiteDisplayDao = automatedSuiteDisplayDao;
		this.attachmentDisplayDao = attachmentDisplayDao;
	}

	@Override
	public GridResponse findAutomatedSuitesByIterationId(Long iterationId, GridRequest request) {
		IterationAutomatedTestSuiteGrid grid = new IterationAutomatedTestSuiteGrid(iterationId);

		GridResponse response = grid.getRows(request, this.dslContext);
		appendReportsData(response.getDataRows());

		return response;
	}

	private void appendReportsData(List<DataRow> dataRows) {
		Set<String> automatedSuiteIds = dataRows.stream().map(DataRow::getId).collect(Collectors.toSet());

		AttachmentList automatedSuiteAttachmentList = ATTACHMENT_LIST.as(AUTOMATED_SUITE_ATTACHMENT_LIST);
		AttachmentList executionAttachmentList = ATTACHMENT_LIST.as(EXECUTION_ATTACHMENT_LIST);
		Attachment automatedSuiteAttachment = ATTACHMENT.as(AUTOMATED_SUITE_ATTACHMENT);
		Attachment executionAttachment = ATTACHMENT.as(EXECUTION_ATTACHMENT);
		Map<String, AutomatedSuiteReportData> map = dslContext.select(AUTOMATED_SUITE.SUITE_ID,
			DSL.field(DSL.count(EXECUTION.EXECUTION_ID).gt(0)).as(HAS_EXECUTION),
			DSL.field(DSL.count(DSL.field(AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER.RESULT_URL.like(""))).gt(0)).as(HAS_RESULT_URL),
			DSL.field(DSL.count(automatedSuiteAttachment.ATTACHMENT_ID).gt(0)).as(AUTOMATED_SUITE_HAS_ATTACHMENT),
			DSL.field(DSL.count(executionAttachment.ATTACHMENT_ID).gt(0)).as(EXECUTION_EXTENDER_HAS_ATTACHMENT)
		)
			.from(AUTOMATED_SUITE)
			.leftJoin(AUTOMATED_EXECUTION_EXTENDER).on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
			.leftJoin(EXECUTION).on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
			.leftJoin(automatedSuiteAttachmentList).on(AUTOMATED_SUITE.ATTACHMENT_LIST_ID.eq(automatedSuiteAttachmentList.ATTACHMENT_LIST_ID))
			.leftJoin(automatedSuiteAttachment).on(automatedSuiteAttachmentList.ATTACHMENT_LIST_ID.eq(automatedSuiteAttachment.ATTACHMENT_LIST_ID))
			.leftJoin(executionAttachmentList).on(EXECUTION.ATTACHMENT_LIST_ID.eq(executionAttachmentList.ATTACHMENT_LIST_ID))
			.leftJoin(executionAttachment).on(executionAttachmentList.ATTACHMENT_LIST_ID.eq(executionAttachment.ATTACHMENT_LIST_ID))
			.where(AUTOMATED_SUITE.SUITE_ID.in(automatedSuiteIds))
			.groupBy(AUTOMATED_SUITE.SUITE_ID)
			.fetch().intoMap(AUTOMATED_SUITE.SUITE_ID, AutomatedSuiteReportData.class);

		Converter<String, String> converter = CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
		String hasExecution = converter.convert(HAS_EXECUTION);
		String hasResultUrl = converter.convert(HAS_RESULT_URL);
		String executionExtenderHasAttachment = converter.convert(AUTOMATED_SUITE_HAS_ATTACHMENT);
		String automatedSuiteHasAttachment = converter.convert(EXECUTION_EXTENDER_HAS_ATTACHMENT);

		dataRows.forEach(dataRow -> {
			AutomatedSuiteReportData automatedSuiteTest = map.getOrDefault(dataRow.getId(), new AutomatedSuiteReportData());
			dataRow.getData().put(hasExecution, automatedSuiteTest.isHasExecution());
			dataRow.getData().put(hasResultUrl, automatedSuiteTest.isHasResultUrl());
			dataRow.getData().put(executionExtenderHasAttachment, automatedSuiteTest.isExecutionExtenderHasAttachment());
			dataRow.getData().put(automatedSuiteHasAttachment, automatedSuiteTest.isAutomatedSuiteHasAttachment());
		});
	}

	@Override
	public GridResponse findExecutionByAutomatedSuiteID(String automatedSuiteId, GridRequest request) {
		AutomatedSuiteExecutionGrid grid = new AutomatedSuiteExecutionGrid(automatedSuiteId);
		return grid.getRows(request, this.dslContext);
	}

	@Override
	public AutomatedSuiteExecutionReportDto findReportUrlsByAutomatedSuite(String automatedSuiteId) {
		AutomatedSuiteExecutionReportDto dto = new AutomatedSuiteExecutionReportDto();
		dto.setReportUrls(automatedSuiteDisplayDao.findReportUrlsByAutomatedSuiteId(automatedSuiteId));
		Set<Long> attachmentListIds = new HashSet<>(automatedSuiteDisplayDao.findAttachmentListIdByAutomatedSuite(automatedSuiteId));
		dto.setAttachmentLists(attachmentDisplayDao.findAttachmentListByIds(attachmentListIds));
		return dto;
	}

	@Override
	public GridResponse findAutomatedSuitesBySuiteId(Long suiteId, GridRequest request) {
		TestSuiteAutomatedTestSuiteGrid grid = new TestSuiteAutomatedTestSuiteGrid(suiteId);
		GridResponse response = grid.getRows(request, this.dslContext);
		appendReportsData(response.getDataRows());

		return response;
	}

	private static class AutomatedSuiteReportData {
		private boolean hasExecution;
		private boolean hasResultUrl;
		private boolean executionExtenderHasAttachment;
		private boolean automatedSuiteHasAttachment;

		public boolean isHasExecution() {
			return hasExecution;
		}

		public void setHasExecution(boolean hasExecution) {
			this.hasExecution = hasExecution;
		}

		public boolean isHasResultUrl() {
			return hasResultUrl;
		}

		public void setHasResultUrl(boolean hasResultUrl) {
			this.hasResultUrl = hasResultUrl;
		}

		public boolean isExecutionExtenderHasAttachment() {
			return executionExtenderHasAttachment;
		}

		public void setExecutionExtenderHasAttachment(boolean executionExtenderHasAttachment) {
			this.executionExtenderHasAttachment = executionExtenderHasAttachment;
		}

		public boolean isAutomatedSuiteHasAttachment() {
			return automatedSuiteHasAttachment;
		}

		public void setAutomatedSuiteHasAttachment(boolean automatedSuiteHasAttachment) {
			this.automatedSuiteHasAttachment = automatedSuiteHasAttachment;
		}
	}
}
