/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation.model;

/**
 * This is the payload used to generate authentication tokens for SquashAUTOM.
 * The SquashAUTOM automated tests connector generates a token that will be used by the ResultPublisher plugin to
 * update test plan after automated tests execution. This token replaces the standard authentication process, so that
 * we don't have to use real user credentials.
 */
public class SquashAutomTokenPayload {
	private final String login;
	private final String suiteId;

	public SquashAutomTokenPayload(String login, String suiteId) {
		this.login = login;
		this.suiteId = suiteId;
	}

	public String getLogin() {
		return login;
	}

	public String getSuiteId() {
		return suiteId;
	}
}
