/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permission;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.exception.requirement.IllegalRequirementVersionCreationException;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto.LinkedLowLevelRequirementDto;
import org.squashtest.tm.service.internal.dto.HighLevelRequirementExceptionSummary;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.display.HighLevelRequirementDisplayDao;
import org.squashtest.tm.service.requirement.HighLevelRequirementService;
import org.squashtest.tm.service.security.Authorizations;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.squashtest.tm.service.security.Authorizations.CREATE_REQUIREMENT_OR_ROLE_ADMIN;

@Component
@Transactional
public class HighLevelRequirementServiceImpl implements HighLevelRequirementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(HighLevelRequirementServiceImpl.class);

	@PersistenceContext
	private final EntityManager entityManager;

	private final PermissionEvaluationService permissionEvaluationService;

	private RequirementDao requirementDao;

	private HighLevelRequirementDisplayDao highLevelRequirementDisplayDao;

	private RequirementFactory requirementFactory;

	public HighLevelRequirementServiceImpl(EntityManager entityManager,
										   PermissionEvaluationService permissionEvaluationService,
										   RequirementDao requirementDao,
										   HighLevelRequirementDisplayDao highLevelRequirementDisplayDao,
										   RequirementFactory requirementFactory) {
		this.entityManager = entityManager;
		this.permissionEvaluationService = permissionEvaluationService;
		this.requirementDao = requirementDao;
		this.highLevelRequirementDisplayDao = highLevelRequirementDisplayDao;
		this.requirementFactory = requirementFactory;
	}

	@Override
	public void linkToHighLevelRequirement(Long highLevelRequirementId, Long lowLevelRequirementId) {
		HighLevelRequirement highLevelRequirement = this.entityManager.find(HighLevelRequirement.class, highLevelRequirementId);
		Requirement lowLevelRequirement = this.entityManager.find(Requirement.class, lowLevelRequirementId);
		boolean permOnHighReq = permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, Permission.LINK.name(), highLevelRequirement);
		boolean permOnLowReq = permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, Permission.LINK.name(), lowLevelRequirement);
		boolean requirementIsNotChild = !requirementDao.checkIfRequirementIsChild(lowLevelRequirementId);
		if (permOnHighReq && permOnLowReq) {
			if (requirementIsNotChild) {
				lowLevelRequirement.setHighLevelRequirement(highLevelRequirement);
				addHighLevelRequirementLinkToChildren(lowLevelRequirementId, highLevelRequirement);
			}
		} else {
			throw new AccessDeniedException("No permissions...");
		}
	}

	@Override
	@PreAuthorize(Authorizations.LINK_REQUIREMENT_OR_ROLE_ADMIN)
	public void unlinkToHighLevelRequirement(Long requirementId) {
		Requirement lowLevelRequirement = this.entityManager.find(Requirement.class, requirementId);
		lowLevelRequirement.setHighLevelRequirement(null);
		List<Long> allChildrenIds = requirementDao.findAllChildrenIdsFromRequirementId(lowLevelRequirement.getId());
		List<Requirement> children = requirementDao.findAllByIds(allChildrenIds);
		children.forEach(child -> child.setHighLevelRequirement(null));
	}

	@Override
	public void bindRequirementsToHighLevelRequirement(Long highLevelRequirementId, List<Long> lowLevelRequirementIds, HighLevelRequirementExceptionSummary summary) {
		checkPermissionOnRequirements(lowLevelRequirementIds, highLevelRequirementId);

		HighLevelRequirement highLevelRequirement = this.entityManager.find(HighLevelRequirement.class, highLevelRequirementId);
		List<Long> allRequirementIdsWithoutFolder = requirementDao.findAllRequirementsIdsByNodes(lowLevelRequirementIds);
		List<Requirement> requirementList = requirementDao.findAllByIds(allRequirementIdsWithoutFolder);

		List<Requirement> filteredRequirementList = removeChildrenReqWithAncestorInSelection(requirementList, allRequirementIdsWithoutFolder);

		for (Requirement requirement : filteredRequirementList) {
			boolean canBindToHighLevelRequirement = canBindToHighLevelRequirement(highLevelRequirement, requirement, summary);

			if (canBindToHighLevelRequirement) {
				requirement.setHighLevelRequirement(highLevelRequirement);
				addHighLevelRequirementLinkToChildren(requirement.getId(), highLevelRequirement);
			}
		}
	}

	private List<Requirement> removeChildrenReqWithAncestorInSelection(List<Requirement> requirementList, List<Long> allRequirementIdsWithoutFolder) {
		List <Requirement> filteredRequirementList = new ArrayList<>();
		requirementList.forEach(requirement -> {
			boolean requirementIsChild = requirementDao.checkIfRequirementIsChild(requirement.getId());
			if (!requirementIsChild) {
				filteredRequirementList.add(requirement);
			} else if (!checkIfAncestorIsInSelection(allRequirementIdsWithoutFolder, requirement.getId())) {
				filteredRequirementList.add(requirement);
			}
		});
		return filteredRequirementList;
	}


	private void addHighLevelRequirementLinkToChildren(Long lowLevelRequirementId, HighLevelRequirement highLevelRequirement) {
		List<Long> allChildrenIds = requirementDao.findAllChildrenIdsFromRequirementId(lowLevelRequirementId);
		List<Requirement> children = requirementDao.findAllByIds(allChildrenIds);
		children.forEach(child -> child.setHighLevelRequirement(highLevelRequirement));
	}

	private boolean checkIfAncestorIsInSelection(List<Long> allRequirementIdsWithoutFolder, Long requirementId) {
		Long ancestorId = requirementDao.findRequirementAncestorId(requirementId);
		return allRequirementIdsWithoutFolder.contains(ancestorId);
	}

	private void checkPermissionOnRequirements(List<Long> lowLevelRequirementIds, Long highLevelRequirementId) {
		List<Long> requirementIds = new ArrayList<>(lowLevelRequirementIds);
		requirementIds.add(highLevelRequirementId);

		PermissionsUtils.checkPermission(permissionEvaluationService, requirementIds, "LINK", Requirement.class.getName());
	}

	private boolean canBindToHighLevelRequirement(Requirement highLevelRequirement, Requirement requirement, HighLevelRequirementExceptionSummary summary) {
		Long highLevelRequirementId = highLevelRequirement.getId();
		return !checkIfLinkAlreadyExists(highLevelRequirementId, requirement, summary)
			&& !checkIfHighLevelRequirementsInSelection(requirement, summary)
			&& !checkIfAlreadyLinkedToAnotherHighLevelRequirement(highLevelRequirementId, requirement, summary)
			&& !checkIfVersionsAreLinkable(highLevelRequirement.getCurrentVersion(), requirement.getCurrentVersion(), summary)
			&& !checkIfChildRequirementsIsInSelection(requirement, summary);
	}

	@Override
	public void unbindRequirementFromHighLevelRequirement(Long highLevelRequirementId, List<Long> lowLevelRequirementIds) {
		checkPermissionOnRequirements(lowLevelRequirementIds, highLevelRequirementId);

		List<Requirement> requirementList = requirementDao.findAllByIds(lowLevelRequirementIds);
		for (Requirement requirement : requirementList) {
			if (! requirementDao.checkIfRequirementIsChild(requirement.getId())) {
				requirement.setHighLevelRequirement(null);
				removeHighLevelRequirementLinkFromChildren(requirement.getId());
			}
		}
	}

	private void removeHighLevelRequirementLinkFromChildren(Long lowLevelRequirementId) {
		List<Long> allChildrenIds = requirementDao.findAllChildrenIdsFromRequirementId(lowLevelRequirementId);
		List<Requirement> children = requirementDao.findAllByIds(allChildrenIds);
		children.forEach(child -> child.setHighLevelRequirement(null));
	}

	@Override
	public List<LinkedLowLevelRequirementDto> findLinkedLowLevelRequirements(Long requirementId) {
		return highLevelRequirementDisplayDao.findLinkedLowLevelRequirements(requirementId);
	}

	@Override
	public List<Requirement> findStandardRequirementsByHighLvlReqId(Long highLevelRequirementId) {
		Set<Long> linkedLowLevelReqIds = highLevelRequirementDisplayDao.findStandardRequirementsByHighLvlReqId(highLevelRequirementId);
		return requirementDao.findAllByIds(linkedLowLevelReqIds);
	}

	@PreAuthorize(CREATE_REQUIREMENT_OR_ROLE_ADMIN)
	@Override
	public void convertIntoHighLevelRequirement(long requirementId) {
		final Requirement req = requirementDao.findById(requirementId);

		if (req == null) {
			throw new IllegalArgumentException(String.format("Cannot find requirement with ID %d", requirementId));
		}

		final boolean isSynchronized = req.isSynchronized();
		final boolean isChild = requirementDao.checkIfRequirementIsChild(requirementId);

		if (! isChild && ! isSynchronized) {
			requirementFactory.convertIntoHighLevelRequirement(requirementId);
			HighLevelRequirement highLevelRequirement = entityManager.find(HighLevelRequirement.class, requirementId);
			addHighLevelRequirementLinkToChildren(requirementId, highLevelRequirement);
		} else {
			LOGGER.debug("Requirement children and synchronized requirements can not be converted into high level requirement.");
		}
	}

	private boolean checkIfVersionsAreLinkable(RequirementVersion reqVersion, RequirementVersion relatedReqVersion, HighLevelRequirementExceptionSummary summary) {
		if (!reqVersion.isLinkable() || !relatedReqVersion.isLinkable()) {
			summary.getRequirementWithNotLinkableStatus().add(relatedReqVersion.getFullName());
			return true;
		}
		return false;
	}

	private boolean checkIfChildRequirementsIsInSelection(Requirement requirement, HighLevelRequirementExceptionSummary summary) {
		if (requirementDao.checkIfRequirementIsChild(requirement.getId())) {
			summary.getChildRequirementsInSelection().add(requirement.getCurrentVersion().getFullName());
			return true;
		}
		return false;
	}

	private boolean checkIfLinkAlreadyExists(Long reqId, Requirement relatedReq, HighLevelRequirementExceptionSummary summary) {
		HighLevelRequirement highLevelRequirement = relatedReq.getHighLevelRequirement();
		if (highLevelRequirement != null && reqId.equals(highLevelRequirement.getId())) {
			summary.getAlreadyLinked().add(relatedReq.getCurrentVersion().getFullName());
			return true;
		}
		return false;
	}

	private boolean checkIfHighLevelRequirementsInSelection(Requirement requirement, HighLevelRequirementExceptionSummary summary) {
		if (requirement.isHighLevel()) {
			summary.getHighLevelRequirementsInSelection().add(requirement.getCurrentVersion().getFullName());
			return true;
		}
		return false;
	}

	private boolean checkIfAlreadyLinkedToAnotherHighLevelRequirement(Long reqId, Requirement requirement, HighLevelRequirementExceptionSummary summary) {
		HighLevelRequirement highLevelRequirement = requirement.getHighLevelRequirement();
		if (requirement.getHighLevelRequirement() != null && !reqId.equals(highLevelRequirement.getId())) {
			summary.getAlreadyLinkedToAnotherHighLevelRequirement().add(requirement.getCurrentVersion().getFullName());
			return true;
		}
		return false;
	}

}
