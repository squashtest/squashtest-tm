/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors.libraries;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.internal.repository.display.impl.collectors.AbstractTreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.count;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

@Component
public abstract class AbstractLibraryCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

	private MilestoneDisplayDao milestoneDisplayDao;

	public AbstractLibraryCollector(DSLContext dsl, CustomFieldValueDisplayDao customFieldValueDisplayDao, ActiveMilestoneHolder activeMilestoneHolder, MilestoneDisplayDao milestoneDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
		this.milestoneDisplayDao = milestoneDisplayDao;
	}

	@Override
	protected Map<Long, DataRow> doCollect(List<Long> ids) {
		Map<Long, DataRow> libraryRows = dsl.select(
			// @formatter:off
			getLibraryPrimaryKeyColumn(),
			PROJECT.PROJECT_ID.as(PROJECT_ID_ALIAS),
			PROJECT.NAME,
			count(getContentIdColumnInContentTable()).as(CHILD_COUNT_ALIAS))
			.from(getLibraryTable())
			.innerJoin(PROJECT).using(getLibraryColumnInProjectTable())
			.leftJoin(getLibraryContentTable()).on(getLibraryPrimaryKeyColumn().eq(getLibraryIdColumnInContentTable()))
			.where(getLibraryPrimaryKeyColumn().in(ids))
			.groupBy(PROJECT.PROJECT_ID, getLibraryPrimaryKeyColumn())
			// @formatter:on
			.fetch().stream()
			.collect(Collectors.toMap(
				tuple -> tuple.get(getLibraryPrimaryKeyColumn()),
				tuple -> {
					DataRow dataRow = new DataRow();
					dataRow.setId(new NodeReference(getHandledEntityType(), tuple.get(getLibraryPrimaryKeyColumn())).toNodeId());
					dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
					dataRow.setState(tuple.get(CHILD_COUNT_ALIAS, Integer.class) > 0 ? DataRow.State.closed : DataRow.State.leaf);
					dataRow.setData(tuple.intoMap());
					return dataRow;
				}
			));
		appendMilestonesByProject(libraryRows);
		return libraryRows;
	}

	protected abstract TableField<ProjectRecord, Long> getLibraryColumnInProjectTable();

	protected abstract Field<Long> getLibraryIdColumnInContentTable();

	protected abstract Field<Long> getContentIdColumnInContentTable();

	protected abstract Table<?> getLibraryContentTable();

	protected abstract Table<?> getLibraryTable();

	protected abstract TableField<?, Long> getLibraryPrimaryKeyColumn();
}
