/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.display.search.SearchGridDisplayService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.SearchDisplayDao;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractSearchGridDisplayService implements SearchGridDisplayService {

	private final SearchDisplayDao searchDisplayDao;

	public AbstractSearchGridDisplayService(SearchDisplayDao searchDisplayDao) {
		this.searchDisplayDao = searchDisplayDao;
	}

	@Override
	public GridResponse fetchResearchRows(ResearchResult researchResult) {
		GridResponse gridResponse = searchDisplayDao.getRows(researchResult.getIds());
		gridResponse.setCount(researchResult.getCount());
		reorderAccordingToResults(researchResult, gridResponse);
		return gridResponse;
	}

	private void reorderAccordingToResults(ResearchResult researchResult, GridResponse gridResponse) {
		Map<Long, DataRow> dataRowMap = createDataRowMapById(gridResponse);
		List<DataRow> orderedRows = reorderRows(researchResult, dataRowMap);
		gridResponse.setDataRows(orderedRows);
	}

	private List<DataRow> reorderRows(ResearchResult researchResult, Map<Long, DataRow> dataRowMap) {
		return researchResult.getIds().stream()
			.map(dataRowMap::get)
			.collect(Collectors.toList());
	}

	private Map<Long, DataRow> createDataRowMapById(GridResponse gridResponse) {
		return gridResponse.getDataRows().stream()
			.collect(Collectors.toMap(
				row -> Long.parseLong(row.getId()),
				Function.identity())
			);
	}
}
