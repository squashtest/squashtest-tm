/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.squashtest.tm.domain.requirement.RequirementVersionLinkType;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.exception.requirement.link.CodesAndRolesInconsistentException;
import org.squashtest.tm.exception.requirement.link.LinkTypeCodeAlreadyExistsDomainException;
import org.squashtest.tm.exception.requirement.link.LinkTypeIsDefaultTypeException;
import org.squashtest.tm.service.internal.repository.RequirementVersionLinkDao;
import org.squashtest.tm.service.internal.repository.RequirementVersionLinkTypeDao;
import org.squashtest.tm.service.requirement.RequirementVersionLinkTypeManagerService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by jlor on 14/06/2017.
 */
@Transactional
@Service("squashtest.tm.service.RequirementVersionLinkTypeManagerService")
public class RequirementVersionLinkTypeManagerServiceImpl implements RequirementVersionLinkTypeManagerService {

	@Inject
	private RequirementVersionLinkTypeDao linkTypeDao;

	@Inject
	private RequirementVersionLinkDao reqLinkDao;

	@Override
	public void addLinkType(RequirementVersionLinkType newLinkType) {

		// We removed the LinkTypeCodeAlreadyExistsException because we needed a domain exception to handle the error
		// with angular client
		if (linkTypeDao.doesCodeAlreadyExist(newLinkType.getRole1Code())) {
			throw new LinkTypeCodeAlreadyExistsDomainException("code", "role1Code");
		} else if (linkTypeDao.doesCodeAlreadyExist(newLinkType.getRole2Code())) {
			throw new LinkTypeCodeAlreadyExistsDomainException("code", "role2Code");
		}

		if (newLinkType.getRole1Code().equals(newLinkType.getRole2Code())
			&& !newLinkType.getRole1().equals(newLinkType.getRole2())) {
			throw new CodesAndRolesInconsistentException(newLinkType.getRole1(), newLinkType.getRole1Code(), "role1");
		}

		linkTypeDao.save(newLinkType);
	}

	@Override
	public boolean doesLinkTypeCodeAlreadyExist(String code) {
		return linkTypeDao.doesCodeAlreadyExist(code);
	}

	@Override
	public boolean doesLinkTypeCodeAlreadyExist(String code, Long linkTypeId) {
		return linkTypeDao.doesCodeAlreadyExist(code, linkTypeId);
	}

	@Override
	public void changeDefault(Long linkTypeId) {
		RequirementVersionLinkType newDefaultReqLinkType = linkTypeDao.getOne(linkTypeId);
		List<RequirementVersionLinkType> allReqLinkTypes = linkTypeDao.getAllRequirementVersionLinkTypes();
		for(RequirementVersionLinkType linkType : allReqLinkTypes) {
			linkType.setDefault(false);
		}
		newDefaultReqLinkType.setDefault(true);

	}

	@Override
	public void changeRole1(Long linkTypeId, String newRole1) {

		RequirementVersionLinkType linkType = linkTypeDao.getOne(linkTypeId);

		if (linkType.getRole1Code().equals(linkType.getRole2Code())) {
			if (!areCodesAndRolesConsistent(newRole1, linkType.getRole1Code(), linkType.getRole2(), linkType.getRole2Code())) {
				throw new CodesAndRolesInconsistentException(newRole1, linkType.getRole1Code(), "role1");
			}
		} else {
			checkFieldIsNotBlank(newRole1, "role1");
		}

		linkType.setRole1(newRole1);
	}

	@Override
	public void changeRole2(Long linkTypeId, String newRole2) {

		RequirementVersionLinkType linkType = linkTypeDao.getOne(linkTypeId);

		if (linkType.getRole1Code().equals(linkType.getRole2Code())) {
			if (!areCodesAndRolesConsistent( linkType.getRole1(), linkType.getRole1Code(), newRole2 , linkType.getRole2Code())) {
				throw new CodesAndRolesInconsistentException(newRole2, linkType.getRole2Code(), "role2");
			}
		} else {
			checkFieldIsNotBlank(newRole2, "role2");
		}

		linkType.setRole2(newRole2);

	}

	@Override
	public void changeCode1(Long linkTypeId, String newCode1) {

		RequirementVersionLinkType linkType = linkTypeDao.getOne(linkTypeId);

		if (newCode1.equals(linkType.getRole2Code())) {
			if (!areCodesAndRolesConsistent(linkType.getRole1(), newCode1, linkType.getRole2(), linkType.getRole2Code())) {
				throw new CodesAndRolesInconsistentException(linkType.getRole1(), newCode1, "role1Code");
			}
		} else {
			checkCodeFieldErrors(newCode1, "role1Code");
		}

		linkType.setRole1Code(newCode1);

	}

	@Override
	public void changeCode2(Long linkTypeId, String newCode2) {

		RequirementVersionLinkType linkType = linkTypeDao.getOne(linkTypeId);

		if (newCode2.equals(linkType.getRole1Code())) {
			if (!areCodesAndRolesConsistent(linkType.getRole1(), linkType.getRole1Code(), linkType.getRole2(), newCode2)) {
				throw new CodesAndRolesInconsistentException(linkType.getRole2(), newCode2, "role2Code");
			}
		} else {
			checkCodeFieldErrors(newCode2, "role2Code");
		}

		linkType.setRole2Code(newCode2);
	}

	@Override
	public boolean isLinkTypeDefault(Long linkTypeId) {
		return linkTypeDao.isLinkTypeDefault(linkTypeId);
	}

	@Override
	public boolean isLinkTypeUsed(Long linkTypeId) {
		return linkTypeDao.isLinkTypeUsed(linkTypeId);
	}

	@Override
	public void deleteLinkType(Long linkTypeId) {
		RequirementVersionLinkType linkTypeToDelete = linkTypeDao.getOne(linkTypeId);
		if(linkTypeDao.isLinkTypeDefault(linkTypeId)) {
			throw new LinkTypeIsDefaultTypeException();
		} else {
			RequirementVersionLinkType defaultLinkType = linkTypeDao.getDefaultRequirementVersionLinkType();
			reqLinkDao.setLinksTypeToDefault(linkTypeToDelete, defaultLinkType);
			linkTypeDao.delete(linkTypeToDelete);
		}
	}

	@Override
	public boolean doesContainDefault(List<Long> linkTypesIdsToCheck) {
		Iterable<RequirementVersionLinkType> linkTypesToCheck = linkTypeDao.findAllById(linkTypesIdsToCheck);
		for(RequirementVersionLinkType linkType : linkTypesToCheck) {
			if(linkType.isDefault()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void deleteLinkTypes(List<Long> linkTypeIdsToDelete) {
		for(Long id : linkTypeIdsToDelete) {
			deleteLinkType(id);
		}
	}

	private boolean areCodesAndRolesConsistent(String role1, String role1Code, String role2, String role2Code) {
		if (role1Code.equals(role2Code)
			&& !role1.equals(role2)) {
			return false;
		}

		return true;
	}

	private void checkCodeFieldErrors(String code, String fieldName) {
		checkFieldIsNotBlank(code, fieldName);

		if (linkTypeDao.doesCodeAlreadyExist(code)) {
			throw new LinkTypeCodeAlreadyExistsDomainException("code", fieldName);
		}
	}

	private void  checkFieldIsNotBlank(String fieldValue, String fieldName) {
		if (StringUtils.isEmpty(fieldValue.trim())) {
			throw new RequiredFieldException(fieldName);
		}
	}

}
