/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid

import org.jooq.Field
import org.jooq.Record
import org.jooq.Table
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn
import spock.lang.Specification

import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE

class AbstractGridTest extends Specification {

	class TestGrid extends AbstractGrid {

		@Override
		protected List<GridColumn> getColumns() {
			return [
				new GridColumn(TEST_CASE.TCLN_ID),
				new GridColumn(TEST_CASE.PREREQUISITE),
				new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as("TEST_CASE_NAME")),
			]
		}

		@Override
		protected Table<? extends Record> getTable() {
			return TEST_CASE.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID));
		}

		@Override
		protected Field<?> getIdentifier() {
			return TEST_CASE.TCLN_ID
		}

		@Override
		protected Field<?> getProjectIdentifier() {
			return TEST_CASE_LIBRARY_NODE.PROJECT_ID
		}
	}

	def "should create grid"() {
		expect:
		def grid = new TestGrid()
		grid.columns.size() == 3
		grid.fieldToAliasDictionary.size() == 3
		grid.fieldToAliasDictionary.get(TEST_CASE.TCLN_ID.getName()) == "tclnId"
		grid.fieldToAliasDictionary.get(TEST_CASE.PREREQUISITE.getName()) == "prerequisite"
		grid.fieldToAliasDictionary.get(TEST_CASE_LIBRARY_NODE.NAME.as("TEST_CASE_NAME").getName()) == "testCaseName"
		grid.aliasToFieldDictionary.size() == 3
		grid.aliasToFieldDictionary.get("tclnId") == new GridColumn(TEST_CASE.TCLN_ID)
		grid.aliasToFieldDictionary.get("prerequisite") == new GridColumn(TEST_CASE.PREREQUISITE)
		grid.aliasToFieldDictionary.get("testCaseName") == new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as("TEST_CASE_NAME"))
	}

	def "should create order by clause"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.sort = [
		    new GridSort("testCaseName", GridSort.SortDirection.ASC),
		    new GridSort("tclnId", GridSort.SortDirection.DESC),
		]

		when:
		def clause = new TestGrid().getOrderClause(gridRequest)

		then:
		clause.size() == 2
		clause.get(0) == TEST_CASE_LIBRARY_NODE.NAME.as("TEST_CASE_NAME").asc()
		clause.get(1) == TEST_CASE.TCLN_ID.desc()
	}

}
