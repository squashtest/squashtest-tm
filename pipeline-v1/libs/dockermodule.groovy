/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
	Utilities for usage in docker.

	For technical reasons the Internal part of that script is listed
	before the API part, please scroll down to check what functionalities
	 this module offers.


*/


// INTERNAL
// A pity here, but we have to load shell module without "def" to ensure
// a global visibility
shellmodule = load "${workspace}/pipeline-v1/libs/shellmodule.groovy"



// ************** API ***********************


/**
 * Executes on the given container the command "docker exec -u root command".
 * The semantic is just the same than for shellmodule.execute
 *
 * @param container
 * @param command
 * @return
 */
def rootexec(container, command){
	shellmodule.execute "docker exec -u root ${container.id} $command"
}


/**
 * Executes on the given container the command "docker exec -u root command"
 * but won't fail if the command fail. The return status is returned.
 * This is the docker equivalent of shellmodule.failsafe.
 *
 * @param container
 * @param command
 * @return
 */
def rootfailsafe(container, command){
	shellmodule.failsafe "docker exec -u root ${container.id} $command"
}


/**
 * Preferred way to initiates a SideDockerSpec. Accepts an imagename (including the tag)
 * Please refer to the comments of SideDockerSpec to see what this is about.
 *
 * @param imagename
 * @return
 */
def sideDocker(imagename){
	new SideContainerSpec(imagename)
}

/**
 * Once a SideContainerSpec is ready (see 'sideDocker'), runs that 'spec' then runs the closure 'action'; once
 * the action is over the container is stopped and cleaned up.
 *
 * The Closure 'action' is given a ContainerInfo as unique argument.
 *
 * Important : this is a side container : the closure will run on the host, not the guest (unlike docker().inside { ... }
 *
 * @param spec
 * @param action
 * @return
 */
def runAsSide(SideContainerSpec spec, Closure action){

	def cId = shellmodule.eval "docker create ${spec.rOptions} ${spec.name}"

	echo "created container : ${cId}"

	def container = null

	try {
		// we gather the info right now, although the container has not run yet
		// it means, it has no IP yet
		// still it's useful to gather the mount information as early as possible
		// in case the whole thing crash and we need it in the clause 'finally'
		container = containerInfo(cId)

		spec.copyContent.each { src, dest ->
			shellmodule.execute "docker cp ${src} ${container.id}:${dest}"
		}

		echo "running..."
		shellmodule.execute "docker start ${container.id}"

		// refresh the container info, this time with the IP
		container = containerInfo(cId)

		action.call(container)

		echo "stopping container ${cId}"
	}
	finally {
		if (container != null) {
			echo "cleanup container ${container.id}"
			cleanupContainer(container)
		}
	}

	return container

}



/**
 * Give a ContainerInfo containing the information of a
 * Docker container, identified by its containerId
 *
 * @param containerId
 */
def containerInfo(containerId){
	def container = new ContainerInfo()
	container.id = containerId
	container.ip = ipOfContainer(containerId)
	container.volumes = volumesOfContainer(containerId)
	return container
}


/**
 * Returns the IP of the given container.
 *
 * @param container
 * @return
 */
def ipOfContainer(containerId){
	shellmodule.eval "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${containerId}"
}


/**
 * Stops and removes the container along with its volumes for the given ContainerInfo.
 *
 * Note that only volumes are removed, not bind-mount (of course).
 * The operation is non-force : only orphaned volumes will be removed.
 * Of course the said container must have been removed beforehand.
 *
 * @param containerInfo
 */
def cleanupContainer(ContainerInfo containerInfo){

	shellmodule.failsafe "docker stop ${containerInfo.id}"
	shellmodule.failsafe "docker rm ${containerInfo.id}"

	containerInfo.volumes
		.findAll { it.getType().equals("volume") }
		.each { vol ->
			shellmodule.failsafe "docker volume rm ${vol.name}"
		}
}


/**
 * Returns the volume ID mounted by a container
 *
 * @param container
 * @return
 */
def volumesOfContainer(String containerId){

	def strmounts = shellmodule.eval "docker inspect -f '{{json .Mounts}}' ${containerId} "

	def parsed = readJSON text: strmounts

	def mounts = parsed.collect { mountpoint ->
		def info = new ContainerMountInfo()
		mountpoint.each { key, value ->
			info.(key.toLowerCase()) = value
		}
		info
	}

	return mounts
}


// ************* Internal **********************


/**
 * Definition of a Side Container, that will provision the said container with
 * docker cp instead of bind-mount.
 *
 * Why
 * -----
 * Bind-mount is indeed a problem when running docker-in-docker with a shared socket, because the
 * filesystem effectively used is that of the host and this greatly confuses the container that tries to run another container
 * and bind-mounting things with it.
 *
 * The workaround will then be to docker cp the desired content instead, which is less optimal. The docker cp will happen
 * before the container is run, so you can copy configuration files if you like. Indeed the underlying sequence is
 * 'docker create', 'docker cp' and finally 'docker start', instead of 'docker run'.
 *
 * Usage
 * ------
 *	This class should not be used directly, it should be created using the factory method 'sideContainer'. This is not
 * mandatory, but using it would be more idiomatic (see below).
 *
 * Likewise it should be run with the method 'runAsSide' (see below).
 *
 * Example Usage :
 *
 * 	def spec = sideContainer("image-name")
 * 		.runOptions("-d -t ")
 * 		.dockercpContent([
 * 				"local/folder" : "guest/folder
 * 			])
 *
 * runAsSide(spec){
 *
 * 		// do you things
 *
 *  }
 *
 *  See the doc on the relevant sections.
 *
 */
class SideContainerSpec {
	def name = ""
	def rOptions = ""
	def copyContent = [:]

	// constructor : accepts the image name
	// this is private, use the factory method 'sideContainer' (below)
	SideContainerSpec(name) {
		this.name = name
	}

	/**
	 * Any options to use, and should not include --mount or -v or alikes.
	 *
	 * @param options
	 * @return
	 */
	SideContainerSpec runOptions(options) {
		this.rOptions = options
		return this
	}

	/**
	 * Accepts a map of path from local folder/files to guest folder/files. Each
	 * of them will be processed via 'docker cp'.
	 *
	 *
	 * @param content
	 * @return
	 */
	SideContainerSpec dockercpContent(content) {
		this.copyContent = content
		return this
	}



}





// *************** Docker Information Extraction method **************************



/**
 * Data of a container. Is best created using 'containerInfo' (see below)
 */
class ContainerInfo{
	String id;
	String ip;
	Collection<ContainerMountInfo> volumes
}

/**
 * Data regaring mounted volumes.
 *
 * The properties are the same as returned by docker inspect, except that the type is lowercase
 *
 */
class ContainerMountInfo{
	String type
	String name
	String source
	String destination
	String driver
	String mode
	boolean rw
	String propagation

	// some versions of docker wouldn't return the attribute "Type"
	// so we crudely infer it and hope we're lucky with our guess
	String getType(){
		if (type == null){
			type = (source?.contains('/var/lib/docker')) ? "volume" : "bind"
		}
		type
	}

	public String toString(){
		"""{
			type: ${getType()},
			name: $name,
			source: $source,
			destination: $destination,
			driver: $driver
			mode: $mode,
			rw: $rw,
			propagation: $propagation
		}"""
	}
}




return this
