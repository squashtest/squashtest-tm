/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.security.authentication;


import java.util.List;

/**
 * This is a duplicate of SecurityExemptionEndPoint but this one is dedicated to REST API security configuration.
 *
 * Note : this interface was designed as a way to implement a custom token authentication on some plugin endpoints.
 * It is a TEMPORARY patch that may be removed in the next versions. The real issue is that a non-physical user
 * (a test automation server) is talking to Squash TM trough its REST API. This is a mistake, automata should not use
 * the REST API.
 *
 * @see SecurityExemptionEndPoint
 */
public interface ApiSecurityExemptionEndPoint {
	List<String> getIgnoreAuthUrlPatterns();
}
