/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

/**
 * Commonly used keys for InputType configuration. From version 2.0.0, some of those are ignored front-side
 * (TIME_FORMAT, FORMAT, MAX_LENGTH).
 *
 * Historically supported metadata are :
 *
 *   <ul>
 *   	<li>
 *          {@link #TIME_FORMAT} : since 1.5.1. Used to format the time in a DATE_TIME input.
 *   	</li>
 *   	<li>{@link #FORMAT} : since 1.8.0. A format string that the widget can use to format its input or output. Widgets using this option are :
 *   		<ul>
 *   			<li>{@link InputType.TypeName.DATE_PICKER} (use the standard java date format)</li>
 *   			<li>{@link InputType.TypeName.DATE_TIME} (use the standard java date format)</li>
 *   		</ul>
 *   	<li>{@link #ONCHANGE} : since 1.5.1.
 *      If set, when the widget on the Squash UI changes its value, it will emit a {@link DelegateCommand} to the bugtracker connector. Not all widgets
 *      supports this, as of 3.0.0 only text_field and tags (free_tag_list, tag_list and multi_select) can do so.
 *
 *   	Native squash widgets will emit a DelegateCommand, using the value you supplied for 'onchange' as command name and its {@link FieldValue#getName()} as argument.
 *      Customized widgets shipped with an extension can of course specify something else, it will be up to your connector to know how to interpret them.
 *   	This mechanism is used for instance by the text_fields for autocompletion.
 *   	</li>
 *   	<li>
 *   		{@link #MAX_LENGTH} : if set (to a positive numeric value), will cap the size of the input to that specified value. For now only plaijn TEXT_FIELD supports it.
 *   	</li>
 *   </ul>
 */
public enum CommonConfigurationKey {

	FORMAT("format"),
	TIME_FORMAT("time-format"),
	ONCHANGE("onchange"),
	MAX_LENGTH("max-length"),

	/**
	 * Allow to define a confirmation message to show before the value changes get applied.
	 */
	CONFIRM_MESSAGE("confirmMessage"),

	/**
	 * Allow to define some text to be displayed as a tooltip.
	 */
	HELP_MESSAGE("help-message"),

	/**
	 * If present, allow to filter among possible values in dropdown lists.
	 */
	FILTER_POSSIBLE_VALUES("filter-possible-values"),

	/**
	 * Boolean to define if the field needs to be return as pure HTML.
	 */
	RENDER_AS_HTML("render-as-html");

	public final String value;

	CommonConfigurationKey(String value) {
		this.value = value;
	}
}
