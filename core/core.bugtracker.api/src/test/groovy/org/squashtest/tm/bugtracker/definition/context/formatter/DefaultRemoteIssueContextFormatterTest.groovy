/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context.formatter

import org.junit.Assert
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.stubbing.Answer
import org.springframework.context.MessageSource
import org.squashtest.tm.bugtracker.definition.context.ExecutionInfo
import org.squashtest.tm.bugtracker.definition.context.ExecutionStepInfo
import org.squashtest.tm.bugtracker.definition.context.KeywordExecutionStepInfo
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext
import org.squashtest.tm.bugtracker.definition.context.ScriptedExecutionStepInfo
import org.squashtest.tm.bugtracker.definition.context.StandardExecutionStepInfo
import org.squashtest.tm.bugtracker.definition.context.TestCaseInfo

class DefaultRemoteIssueContextFormatterTest {

	private static MessageSource mockMessageSource() {
		MessageSource messageSource = Mockito.mock(MessageSource.class)
		// Return the message key
		Mockito.when(messageSource.getMessage(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any()))
			.thenAnswer((Answer<String>) { invocation -> invocation.getArgument(0) })
		return messageSource
	}

	@Test
	void formatDescriptionForStandardExecution() {
		RemoteIssueContext context = createStandardExecutionContext()

		String actualDescription = DefaultRemoteIssueContextFormatter.getDefaultDescription(context, mockMessageSource())

		String expectedDescription = "issue.default.description.testCase : [ref] name\n" +
			"issue.default.description.execution : http://dum.my\n" +
			"issue.default.description.concernedStep : 2/3\n" +
			"\n" +
			"issue.default.description.description\n" +
			"\n" +
			"- test-case.prerequisite.label\n" +
			"\tprerequisite\n" +
			"\n" +
			"- issue.default.additionalInformation.step 1/3\n" +
			"\t- issue.default.additionalInformation.action\n" +
			"\t\taction1\n" +
			"\t\ton\n" +
			"\t\tmultiple lines\n" +
			"\t\n" +
			"\t- issue.default.additionalInformation.expectedResult\n" +
			"\t\tresult1\n" +
			"\t\n" +
			"- issue.default.additionalInformation.step 2/3\n" +
			"\t- issue.default.additionalInformation.action\n" +
			"\t\taction2\n" +
			"\t\n" +
			"\t- issue.default.additionalInformation.expectedResult\n" +
			"\t\tresult2\n" +
			"\t"

		Assert.assertEquals(expectedDescription, actualDescription)
	}

	@Test
	void formatDescriptionForScriptedExecution() {
		RemoteIssueContext context = createScriptedExecutionContext()

		String actualDescription = DefaultRemoteIssueContextFormatter.getDefaultDescription(context, mockMessageSource())

		String expectedDescription = "issue.default.description.testCase : [ref] name\n" +
			"issue.default.description.execution : http://dum.my\n" +
			"issue.default.description.concernedStep : 2/2\n" +
			"\n" +
			"issue.default.description.description\n" +
			"\n" +
			"- label.Background\n" +
			"\tprerequisite\n" +
			"\n" +
			"- issue.default.additionalInformation.script\n" +
			"\tscript2\n" +
			"\ton\n" +
			"\tmultiple\n" +
			"\tlines\n"

		Assert.assertEquals(expectedDescription, actualDescription)
	}

	@Test
	void formatDescriptionForKeywordExecution() {
		RemoteIssueContext context = createKeywordExecutionContext()

		String actualDescription = DefaultRemoteIssueContextFormatter.getDefaultDescription(context, mockMessageSource())

		String expectedDescription = "issue.default.description.testCase : [ref] name\n" +
			"issue.default.description.execution : http://dum.my\n" +
			"issue.default.description.concernedStep : 5/5\n" +
			"\n" +
			"issue.default.description.description\n" +
			"\n" +
			"- issue.default.additionalInformation.step 5/5\n" +
			"\t- label.testSteps\n" +
			"\t\tgiven my glass is full of water\n" +
			"\t\tand I'm really thirsty\n" +
			"\t\twhen I drink the water\n" +
			"\t\tthen the glass is empty\n" +
			"\t\tand I'm not thirsty anymore\n"

		Assert.assertEquals(expectedDescription, actualDescription)
	}

	static RemoteIssueContext createStandardExecutionContext() {
		List<ExecutionStepInfo> steps = Arrays.asList(
			new StandardExecutionStepInfo(1L, 0, "action1\non\nmultiple lines", null, "result1", null),
			new StandardExecutionStepInfo(2L, 1, "action2", null, "result2", null),
			new StandardExecutionStepInfo(3L, 2, "action3", null, "result3", null),
		)
		TestCaseInfo testCase = new TestCaseInfo(123L, "ref", "name", TestCaseInfo.Kind.STANDARD)
		ExecutionInfo execution = new ExecutionInfo(321L, "http://dum.my", "prerequisite", null)
		return new RemoteIssueContext(steps, 2L, testCase, execution, null)
	}

	static RemoteIssueContext createScriptedExecutionContext() {
		List<ExecutionStepInfo> steps = Arrays.asList(
			new ScriptedExecutionStepInfo(1L, 0, "script1\non\nmultiple\nlines"),
			new ScriptedExecutionStepInfo(2L, 1, "script2\non\nmultiple\nlines"),
		)
		TestCaseInfo testCase = new TestCaseInfo(123L, "ref", "name", TestCaseInfo.Kind.GHERKIN)
		ExecutionInfo execution = new ExecutionInfo(321L, "http://dum.my", "prerequisite", null)
		return new RemoteIssueContext(steps, 2L, testCase, execution, null)
	}

	static RemoteIssueContext createKeywordExecutionContext() {
		List<ExecutionStepInfo> steps = Arrays.asList(
			new KeywordExecutionStepInfo(1L, 0, "given my glass is full of water", null),
			new KeywordExecutionStepInfo(22L, 1, "and I'm really thirsty", null),
			new KeywordExecutionStepInfo(3L, 2, "when I drink the water", null),
			new KeywordExecutionStepInfo(41L, 3, "then the glass is empty", null),
			new KeywordExecutionStepInfo(17L, 4, "and I'm not thirsty anymore", null),
		)
		TestCaseInfo testCase = new TestCaseInfo(123L, "ref", "name", TestCaseInfo.Kind.KEYWORD)
		ExecutionInfo execution = new ExecutionInfo(321L, "http://dum.my", "prerequisite", null)
		return new RemoteIssueContext(steps, 17L, testCase, execution, null)
	}
}
