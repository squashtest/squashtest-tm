/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.basetest;

public enum Users {
	ADMIN("admin","admin"),
	PROJECT_MANAGER("project_manager","admin"),
	ADVANCE_TESTER("advance_tester","admin"),
	TEST_DESIGNER("test_designer","admin"),
	TEST_EDITOR("test_editor","admin"),
	TEST_RUNNER("test_runner","admin"),
	VALIDATOR("validator","admin"),
	AUTOMATED_TEST_WRITER("automated_test_writer","admin"),
	VIEWER("viewer","admin");
	private final String username;
	private final String password;

	Users(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "Users{" +
			"username='" + username + '\'' +
			'}';
	}
}
