<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>org.squashtest.tm</groupId>
    <artifactId>tm-integration</artifactId>
    <version>4.0.0.IT1-SNAPSHOT</version>
  </parent>

  <artifactId>backend-tests</artifactId>

  <properties>

    <database.changelog>${db.resources.dir}/global.changelog-master.xml</database.changelog>

    <db.skip>false</db.skip>

    <!--  DEP VERSION -->
    <dbunit.version>2.6.0</dbunit.version>
    <guava.version>28.2-jre</guava.version>
    <junit-jupiter.version>5.5.2</junit-jupiter.version>
    <squash.url />
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>${project.groupId}</groupId>
        <artifactId>squash-tm-bom</artifactId>
        <version>${project.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>

    <dependency>
      <groupId>org.dbunit</groupId>
      <artifactId>dbunit</artifactId>
      <version>${dbunit.version}</version>
      <exclusions>
        <exclusion>
          <groupId>postgresql</groupId>
          <artifactId>postgresql</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
      <version>${guava.version}</version>
    </dependency>

    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>jcl-over-slf4j</artifactId>
    </dependency>

    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>json-path</artifactId>
      <version>4.2.0</version>
    </dependency>

    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>json-schema-validator</artifactId>
      <version>4.2.0</version>
    </dependency>

    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter</artifactId>
      <version>${junit-jupiter.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-slf4j-impl</artifactId>
      <version>2.13.0</version>
    </dependency>

    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
    </dependency>

    <dependency>
      <groupId>org.postgresql</groupId>
      <artifactId>postgresql</artifactId>
    </dependency>

    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>rest-assured</artifactId>
      <version>4.2.0</version>
    </dependency>

    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>tm.service</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>squashtest-tm-database</artifactId>
    </dependency>

    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>xml-path</artifactId>
      <version>4.2.0</version>
    </dependency>

  </dependencies>

  <build>
    <testSourceDirectory>src/main/java</testSourceDirectory>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-resources-plugin</artifactId>
        <configuration>
          <resources>
            <resource>
              <directory>src/main/resources</directory>
              <filtering>true</filtering>
            </resource>
          </resources>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.22.2</version>
        <configuration>
          <systemPropertyVariables>
            <database.driver>${liquibase.driver}</database.driver>
            <database.url>${liquibase.url}</database.url>
            <database.user>${liquibase.username}</database.user>
            <database.password>${liquibase.password}</database.password>
            <squash.url>${squash.url}</squash.url>
          </systemPropertyVariables>
        </configuration>
      </plugin>

      <!-- ====================
       Run configuration check (see parent pom)
     ========================-->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <executions>
          <execution>
            <id>enforce-configuration</id>
            <phase>validate</phase>
          </execution>
          <execution>
            <id>ban-unwanted-stuffs</id>
            <phase>none</phase>
          </execution>
        </executions>
      </plugin>
      <!-- ====================
        /Run configuration check
      ========================-->

      <!-- ====================
        fetch the database generation module
      ========================-->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-remote-resources-plugin</artifactId>
      </plugin>
      <!-- ====================
        /fetch the database generation module
      ========================-->

      <!-- ====================
        Generate the database
      ========================-->
      <plugin>
        <groupId>org.liquibase</groupId>
        <artifactId>liquibase-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>generate-database</id>
            <goals>
              <goal>update</goal>
            </goals>
            <phase>generate-resources</phase>
            <configuration>
              <skip>${db.skip}</skip>
              <dropFirst>true</dropFirst>
              <changeLogFile>${database.changelog}</changeLogFile>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <!-- ====================
        /Generate the database
      ========================-->

    </plugins>
  </build>
  <profiles>

    <profile>
      <!-- postgresql profile -->
      <id>postgresql</id>
      <activation>
        <property>
          <name>database</name>
          <value>postgresql</value>
        </property>
      </activation>

      <properties>

        <database.dialect>postgresql</database.dialect>
        <database.drivername>${liquibase.driver}</database.drivername>
        <database.metadata>org.dbunit.database.DefaultMetadataHandler</database.metadata>
        <database.typefactory>org.dbunit.ext.postgresql.PostgresqlDataTypeFactory</database.typefactory>
        <hibernate.dialect>org.squashtest.tm.domain.jpql.PostgresEnhancedDialect</hibernate.dialect>
        <hibernate.new_generator_mappings>true</hibernate.new_generator_mappings>
        <jooq.sql.dialect>POSTGRES</jooq.sql.dialect>
        <liquibase.driver>org.postgresql.Driver</liquibase.driver>
      </properties>

      <dependencies>
        <dependency>
          <groupId>org.postgresql</groupId>
          <artifactId>postgresql</artifactId>
        </dependency>
      </dependencies>
    </profile>

    <profile>
      <!-- mysql profile -->
      <id>mysql</id>
      <activation>
        <property>
          <name>database</name>
          <value>mysql</value>
        </property>
      </activation>

      <properties>
        <database.dialect>mysql</database.dialect>
        <database.drivername>${liquibase.driver}</database.drivername>
        <database.metadata>org.dbunit.ext.mysql.MySqlMetadataHandler</database.metadata>
        <database.typefactory>org.dbunit.ext.mysql.MySqlDataTypeFactory</database.typefactory>
        <hibernate.dialect>org.squashtest.tm.domain.jpql.MySQLEnhancedDialect</hibernate.dialect>
        <hibernate.new_generator_mappings>true</hibernate.new_generator_mappings>
        <jooq.sql.dialect>MYSQL</jooq.sql.dialect>
        <liquibase.driver>com.mysql.jdbc.Driver</liquibase.driver>
      </properties>

      <dependencies>
        <dependency>
          <groupId>mysql</groupId>
          <artifactId>mysql-connector-java</artifactId>
        </dependency>
      </dependencies>
    </profile>

    <!-- ======================
      /Database profiles
    ======================= -->

    <profile>
      <id>skip-database</id>
      <activation>
        <property>
          <name>database.nocreate</name>
        </property>
      </activation>

      <properties>
        <db.skip>true</db.skip>
      </properties>

    </profile>
  </profiles>
</project>
