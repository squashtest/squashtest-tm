/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.milestone

import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.config.EnabledAclSpecConfig
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.squashtest.tm.service.internal.dto.UserDto
import org.squashtest.tm.service.user.UserAccountService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
@ContextHierarchy([
	// enabling the ACL management that was disabled in DbunitServiceSpecification
	@ContextConfiguration(name = "aclcontext", classes = [EnabledAclSpecConfig], inheritLocations = false)
])
class MilestoneDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private MilestoneDisplayService milestoneDisplayService

	def "should fetch a milestones grid as admin"() {
		given:
		UserAccountService userAccountService = Mock()
		UserDto admin = Mock()
		admin.isAdmin() >> true
		this.milestoneDisplayService.userAccountService = userAccountService
		this.milestoneDisplayService.userAccountService.findCurrentUserDto() >> admin

		def gridRequest = new GridRequest()
		gridRequest.size = 25

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

		when:
		def gridResponse = this.milestoneDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 4
		def rows = gridResponse.dataRows

		// Rows are sorted by end date DESC

		def first = rows.get(0)
		first.id == "-3"
		first.data.get("milestoneId") == -3
		first.data.get("label") == "label3"
		first.data.get("status") == "IN_PROGRESS"
		first.data.get("endDate") == dateFormat.parse("2020-10-09")
		first.data.get("projectCount") == 0
		first.data.get("ownerFirstName") == "john"
		first.data.get("ownerLastName") == "doe"
		first.data.get("ownerLogin") == "john.doe"
		first.data.get("ownerId") == -3
		first.data.get("description") == "descr3"
		first.data.get("createdOn") == dateFormat.parse("2020-10-06")
		first.data.get("createdBy") == "john.doe"

		def second = rows.get(1)
		second.id == "-2"
		second.data.get("milestoneId") == -2
		second.data.get("label") == "label2"
		second.data.get("status") == "IN_PROGRESS"
		second.data.get("endDate") == dateFormat.parse("2020-10-08")
		second.data.get("projectCount") == 0
		second.data.get("ownerFirstName") == "jane"
		second.data.get("ownerLastName") == "doe"
		second.data.get("ownerLogin") == "jane.doe"
		second.data.get("ownerId") == -2
		second.data.get("description") == "descr2"
		second.data.get("createdOn") == dateFormat.parse("2020-10-06")
		second.data.get("createdBy") == "jane.doe"

		def third = rows.get(2)
		third.id == "-1"
		third.data.get("milestoneId") == -1
		third.data.get("label") == "label1"
		third.data.get("status") == "IN_PROGRESS"
		third.data.get("endDate") == dateFormat.parse("2020-10-07")
		third.data.get("projectCount") == 2
		third.data.get("ownerFirstName") == "admin"
		third.data.get("ownerLastName") == "admin"
		third.data.get("ownerLogin") == "admin"
		third.data.get("ownerId") == -1
		third.data.get("description") == "descr1"
		third.data.get("createdOn") == dateFormat.parse("2020-10-06")
		third.data.get("createdBy") == "admin"
	}

	def "should restrict displayed milestones grid items for a project manager"() {
		given:
		UserAccountService userAccountService = Mock()
		UserDto user = Mock()
		user.isAdmin() >> false
		user.getUsername() >> "jane.doe"
		user.getUserId() >> -2
		this.milestoneDisplayService.userAccountService = userAccountService
		this.milestoneDisplayService.userAccountService.findCurrentUserDto() >> user

		def gridRequest = new GridRequest()
		gridRequest.size = 25

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

		when:
		def gridResponse = this.milestoneDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 3
		def rows = gridResponse.dataRows

		// The test data contains 4 project :
		// - (-1) has a GLOBAL range thus it is visible
		// - (-2) has a RESTRICTED range and is owned by the tested user. It is visible
		// - (-3) has a RESTRICTED range and is NOT owned by the tested user. It is NOT visible
		// - (-2) has a RESTRICTED range and is NOT owned by the tested user but is linked to a project
		// 		managed by the user. It is visible

		def first = rows.get(0)
		first.id == "-2"
		first.data.get("milestoneId") == -2
		first.data.get("label") == "label2"
		first.data.get("status") == "IN_PROGRESS"
		first.data.get("endDate") == dateFormat.parse("2020-10-08")
		first.data.get("projectCount") == 0
		first.data.get("ownerFirstName") == "jane"
		first.data.get("ownerLastName") == "doe"
		first.data.get("ownerLogin") == "jane.doe"
		first.data.get("ownerId") == -2
		first.data.get("description") == "descr2"
		first.data.get("createdOn") == dateFormat.parse("2020-10-06")
		first.data.get("createdBy") == "jane.doe"

		def second = rows.get(1)
		second.id == "-1"
		second.data.get("milestoneId") == -1
		second.data.get("label") == "label1"
		second.data.get("status") == "IN_PROGRESS"
		second.data.get("endDate") == dateFormat.parse("2020-10-07")
		second.data.get("projectCount") == 2
		second.data.get("ownerFirstName") == "admin"
		second.data.get("ownerLastName") == "admin"
		second.data.get("ownerLogin") == "admin"
		second.data.get("ownerId") == -1
		second.data.get("description") == "descr1"
		second.data.get("createdOn") == dateFormat.parse("2020-10-06")
		second.data.get("createdBy") == "admin"

		def third = rows.get(2)
		third.id == "-4"
		third.data.get("milestoneId") == -4
		third.data.get("ownerId") == -3
	}

	def "should forbid access to milestone view to unauthorized project managers"() {
		UserAccountService userAccountService = Mock()
		UserDto user = Mock()
		user.isAdmin() >> false
		user.getUsername() >> "jane.doe"
		user.getUserId() >> -2
		this.milestoneDisplayService.userAccountService = userAccountService
		this.milestoneDisplayService.userAccountService.findCurrentUserDto() >> user

		// cf visibility rules in 'should restrict displayed milestones grid items...'
		this.milestoneDisplayService.canReadMilestone(-1L) == true
		this.milestoneDisplayService.canReadMilestone(-2L) == true
		this.milestoneDisplayService.canReadMilestone(-3L) == false
		this.milestoneDisplayService.canReadMilestone(-4L) == true
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		given:
		UserAccountService userAccountService = Mock()
		UserDto admin = Mock()
		admin.isAdmin() >> true
		this.milestoneDisplayService.userAccountService = userAccountService
		this.milestoneDisplayService.userAccountService.findCurrentUserDto() >> admin

		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.milestoneDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["label",
				   "status",
				   "endDate",
				   "projectCount",
				   "ownerFirstName",
				   "ownerLastName",
				   "ownerLogin",
				   "ownerId",
				   "description",
				   "createdOn",
				   "createdBy"]
	}

	def "should fetch milestone view dto for a given milestone id"() {
		given:
		UserAccountService userAccountService = Mock()
		UserDto admin = Mock()
		admin.isAdmin() >> true
		this.milestoneDisplayService.userAccountService = userAccountService
		this.milestoneDisplayService.userAccountService.findCurrentUserDto() >> admin
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

		when:
		def milestoneViewDto = milestoneDisplayService.getMilestoneView(-1L)

		then:
		milestoneViewDto.label == "label1"
		milestoneViewDto.status == "IN_PROGRESS"
		milestoneViewDto.endDate ==  dateFormat.parse("2020-10-07")
		milestoneViewDto.range == "GLOBAL"
		milestoneViewDto.description == "descr1"
		milestoneViewDto.ownerFirstName == "admin"
		milestoneViewDto.ownerLastName == "admin"
		milestoneViewDto.ownerLogin == "admin"
		milestoneViewDto.createdOn == dateFormat.parse("2020-10-06")
		milestoneViewDto.createdBy == "admin"
	}
}
