/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testcase

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.bdd.Keyword.AND
import static org.squashtest.tm.domain.bdd.Keyword.BUT
import static org.squashtest.tm.domain.bdd.Keyword.GIVEN
import static org.squashtest.tm.domain.bdd.Keyword.THEN
import static org.squashtest.tm.domain.bdd.Keyword.WHEN

@DataSet
@Transactional
@UnitilsSupport
class TestStepModificationServiceImplIT extends DbunitServiceSpecification {

	@Inject
	TestStepModificationService testStepModificationService

	def "should update keyword of a keyword test step"() {
		when:
			testStepModificationService.updateKeywordTestStep(-3L, newKeyword)
		then:
			KeywordTestStep step = (KeywordTestStep) findEntity(KeywordTestStep.class, -3L)
			newKeyword == step.getKeyword()
		where:
			newKeyword << [GIVEN, WHEN, THEN, AND, BUT]
	}
}
