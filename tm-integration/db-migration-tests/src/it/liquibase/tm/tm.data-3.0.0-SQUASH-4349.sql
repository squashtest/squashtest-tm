INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES (-815), (-816), (-817), (-818);

INSERT INTO TEST_CASE_LIBRARY_NODE (TCLN_ID, NAME, CREATED_BY, CREATED_ON, PROJECT_ID, ATTACHMENT_LIST_ID) VALUES
(-222, 'SQUASH-4349-cdt', 'admin', '2021-03-18', 2, -815),
(-223, 'SQUASH-4349-called-test-1-non-delegate-parameters', 'admin', '2021-03-18', 2, -816),
(-224, 'SQUASH-4349-called-test-2-deleted-step', 'admin', '2021-03-18', 2, -817),
(-225, 'SQUASH-4349-called-test-3-delegate-parameters', 'admin', '2021-03-18', 2, -818);

INSERT INTO TEST_CASE (TCLN_ID, VERSION, TC_NATURE, TC_TYPE, PREREQUISITE, UUID) VALUES
(-222, 1, 12, 20, '', 'fc8c56d7-7540-4db1-892d-8b774db23ab5'),
(-223, 1, 12, 20, '', 'fc8c56d7-7540-4db1-892d-8b774db23ab6'),
(-224, 1, 12, 20, '', 'fc8c56d7-7540-4db1-892d-8b774db23ab7'),
(-225, 1, 12, 20, '', 'fc8c56d7-7540-4db1-892d-8b774db23ab8');

INSERT INTO TEST_STEP (TEST_STEP_ID) VALUES (-20), (-21);

INSERT INTO CALL_TEST_STEP (TEST_STEP_ID, CALLED_TEST_CASE_ID, CALLED_DATASET, DELEGATE_PARAMETER_VALUES) VALUES
(-20, -223, null, false),
(-21, -225, null, true);

INSERT INTO TEST_CASE_STEPS (TEST_CASE_ID, STEP_ID, STEP_ORDER) VALUES
(-222, -20, 0),
(-222, -21, 1);

INSERT INTO DATASET (DATASET_ID, NAME, TEST_CASE_ID) VALUES
(-3, 'SQUASH-4349-dataset-1', -222);

INSERT INTO PARAMETER (PARAM_ID, NAME, TEST_CASE_ID, DESCRIPTION) VALUES
(-1, 'param1', -222, 'param1 from test case -222'),
(-2, 'paramA', -223, 'paramA from test case -223'),
(-3, 'paramB', -223, 'paramB from test case -223'),
(-4, 'paramC', -224, 'paramC from test case -224'),
(-5, 'paramD', -225, 'paramD from test case -225');

INSERT INTO DATASET_PARAM_VALUE (DATASET_PARAM_VALUE_ID, DATASET_ID, PARAM_ID, PARAM_VALUE) VALUES
(-1, -3, -1, '1'),
(-2, -3, -2, 'a'),
(-3, -3, -3, 'b'),
(-4, -3, -4, 'c'),
(-5, -3, -5, 'd');

